ALTER TABLE `ims_zh_gjhdbm_user`
ADD COLUMN `is_delete`  tinyint(1) NOT NULL COMMENT '1删除' AFTER `police_num`;

ALTER TABLE `ims_zh_gjhdbm_case`
ADD COLUMN `end_pic`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `sub_status`,
ADD COLUMN `content`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `end_pic`;




ALTER TABLE `ims_zh_gjhdbm_user`
ADD COLUMN `is_police`  tinyint(1) NOT NULL COMMENT '1是交警 0不是' AFTER `user_name`;

ALTER TABLE `ims_zh_gjhdbm_case`
MODIFY COLUMN `status`  tinyint(1) NOT NULL COMMENT '0 待立案  6交警已接受案件  1调查处理中   2已结案 3拒绝立案 4技术鉴定中 5 下发认证书' AFTER `add_time`;

ALTER TABLE `ims_zh_gjhdbm_user`
DROP COLUMN `money`,
DROP COLUMN `item`,
DROP COLUMN `rz_type`,
DROP COLUMN `link_name`,
MODIFY COLUMN `link_tel`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '交警电话' AFTER `uniacid`,
ADD COLUMN `is_police`  tinyint(1) NOT NULL COMMENT '1是交警 0不是' AFTER `user_name`,
ADD COLUMN `police_num`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '警号' AFTER `is_police`;

ALTER TABLE `ims_zh_gjhdbm_message`
ADD COLUMN `belong_id`  int(11) NOT NULL AFTER `message`;

CREATE TABLE `ims_zh_gjhdbm_pic` (
`pic_id`  int(11) NOT NULL AUTO_INCREMENT ,
`type`  tinyint(1) NOT NULL COMMENT '1 侧前方 2侧后方 3碰撞部位' ,
`url`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`case_id`  int(11) NOT NULL ,
PRIMARY KEY (`pic_id`)
)
;





ALTER TABLE `ims_zh_gjhdbm_case`
MODIFY COLUMN `case_id`  int(10) NULL AUTO_INCREMENT FIRST ,
ADD COLUMN `is_high_speed`  tinyint(1) NOT NULL COMMENT '0 否 1是' AFTER `end_form_id`,
ADD COLUMN `is_secure`  tinyint(1) NOT NULL COMMENT '0 否 1是 交强险' AFTER `is_high_speed`;

ALTER TABLE `ims_zh_gjhdbm_user`
ADD COLUMN `idcard`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '身份证号' AFTER `link_tel`,
ADD COLUMN `user_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '姓名' AFTER `idcard`;


CREATE TABLE `ims_zh_gjhdbm_tel` (
`id`  int NOT NULL AUTO_INCREMENT ,
`tel`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`id`)
)
;

CREATE TABLE `ims_zh_gjhdbm_car` (
`id`  int NOT NULL AUTO_INCREMENT ,
`driving_num`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`car_num`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`secure_company`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `ims_zh_gjhdbm_tel`
ADD COLUMN `user_id`  int(11) NOT NULL AFTER `tel`;

ALTER TABLE `ims_zh_gjhdbm_car`
ADD COLUMN `user_id`  int(11) NOT NULL AFTER `secure_company`;

CREATE TABLE `ims_zh_gjhdbm_message` (
`id`  int NOT NULL AUTO_INCREMENT ,
`case_id`  int NOT NULL ,
`deal_id`  int NOT NULL ,
`message`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`id`)
)
;

ALTER TABLE `ims_zh_gjhdbm_case`
MODIFY COLUMN `status`  tinyint(1) NOT NULL COMMENT '0 待立案  1调查处理中   2已结案 3拒绝立案 4技术鉴定中 5 下发认证书' AFTER `add_time`;





CREATE TABLE `ims_zh_gjhdbm_case` (
`case_id`  int(10) NOT NULL AUTO_INCREMENT ,
`address`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '定位地址' ,
`a_contact`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'A方联系人' ,
`b_contact`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'B方联系人' ,
`a_tel`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'a方电话' ,
`b_tel`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'b方电话' ,
`add_time`  int(10) NOT NULL ,
`status`  tinyint(1) NOT NULL COMMENT '0 待立案  1处理中（已立案） 2已结案 3拒绝立案' ,
`check_time`  int(10) NOT NULL COMMENT '审核时间' ,
`deal_id`  int(10) NOT NULL COMMENT '案件处理者（交警）编号' ,
`is_delete`  int NOT NULL DEFAULT 0 COMMENT '0 未删除 1删除' ,
PRIMARY KEY (`case_id`)
);

ALTER TABLE `ims_zh_gjhdbm_case`
ADD COLUMN `apply_id`  int(10) NOT NULL COMMENT '案件申请者' AFTER `case_id`;

CREATE TABLE `ims_zh_gjhdbm_deal` (
`deal_id`  int NOT NULL AUTO_INCREMENT ,
`deal_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名字' ,
`deal_tel`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话' ,
`add_time`  int(10) NOT NULL ,
`is_delete`  int NOT NULL DEFAULT 0 COMMENT '0 未删除 1删除' ,
PRIMARY KEY (`deal_id`)
);

ALTER TABLE `ims_zh_gjhdbm_case`
ADD COLUMN `a_plate_num`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'a方拍照' AFTER `is_delete`,
ADD COLUMN `b_plate_num`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'b方拍照' AFTER `a_plate_num`,
ADD COLUMN `acc_pic`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '事故图片' AFTER `b_plate_num`;
ADD COLUMN `case_type`  tinyint(1) NOT NULL COMMENT '事故类型  1一般事故 2紧急事故' AFTER `acc_pic`;
ADD COLUMN `address_descript`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '位置描述' AFTER `case_type`;
MODIFY COLUMN `check_time`  int(10) NOT NULL COMMENT '立案时间' AFTER `status`,
ADD COLUMN `end_time`  int(10) NOT NULL COMMENT '结案时间' AFTER `address_descript`;

ALTER TABLE `ims_zh_gjhdbm_case`
ADD COLUMN `event_num`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '事故编号' AFTER `end_time`,
ADD COLUMN `cert_info`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `event_num`;


CREATE TABLE `ims_zh_gjhdbm_article` (
`article_id`  int NOT NULL AUTO_INCREMENT ,
`content`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`add_time`  int NOT NULL ,
`is_delete`  tinyint NOT NULL ,
PRIMARY KEY (`article_id`)
)
;

ALTER TABLE `ims_zh_gjhdbm_article`
ADD COLUMN `title`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `is_delete`;


CREATE TABLE `ims_zh_gjhdbm_contact` (
`contact_id`  int NOT NULL AUTO_INCREMENT ,
`case_id`  int NOT NULL ,
`contact_name`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`contact_tel`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`plate_num`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`contact_id`)
)
;
ALTER TABLE `ims_zh_gjhdbm_contact`
ADD COLUMN `type`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `plate_num`;

ALTER TABLE `ims_zh_gjhdbm_system`
ADD COLUMN `sign_name`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '签名' AFTER `city_open`;

ALTER TABLE `ims_zh_gjhdbm_system`
MODIFY COLUMN `is_dxyz`  int(4) NOT NULL DEFAULT 1 COMMENT '是否开启大鱼短信' AFTER `fb_notice`;

ALTER TABLE `ims_zh_gjhdbm_case`
ADD COLUMN `form_id`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `cert_info`,






