-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2018-06-30 10:50:53
-- 服务器版本： 5.5.54-log
-- PHP Version: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jjd_aodd_cn`
--

-- --------------------------------------------------------

--
-- 表的结构 `ims_account`
--

CREATE TABLE IF NOT EXISTS `ims_account` (
  `acid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `hash` varchar(8) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `isconnect` tinyint(4) NOT NULL,
  `isdeleted` tinyint(3) unsigned NOT NULL,
  `endtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_account`
--

INSERT INTO `ims_account` (`acid`, `uniacid`, `hash`, `type`, `isconnect`, `isdeleted`, `endtime`) VALUES
(1, 1, 'uRr8qvQV', 1, 0, 1, 0),
(2, 2, 'qT1dGt7j', 4, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ims_account_webapp`
--

CREATE TABLE IF NOT EXISTS `ims_account_webapp` (
  `acid` int(11) NOT NULL,
  `uniacid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_account_wechats`
--

CREATE TABLE IF NOT EXISTS `ims_account_wechats` (
  `acid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `token` varchar(32) NOT NULL,
  `encodingaeskey` varchar(255) NOT NULL,
  `level` tinyint(4) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `account` varchar(30) NOT NULL,
  `original` varchar(50) NOT NULL,
  `signature` varchar(100) NOT NULL,
  `country` varchar(10) NOT NULL,
  `province` varchar(3) NOT NULL,
  `city` varchar(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `key` varchar(50) NOT NULL,
  `secret` varchar(50) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `subscribeurl` varchar(120) NOT NULL,
  `auth_refresh_token` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_account_wechats`
--

INSERT INTO `ims_account_wechats` (`acid`, `uniacid`, `token`, `encodingaeskey`, `level`, `name`, `account`, `original`, `signature`, `country`, `province`, `city`, `username`, `password`, `lastupdate`, `key`, `secret`, `styleid`, `subscribeurl`, `auth_refresh_token`) VALUES
(1, 1, '2trvv4wd3g55ngczlkly4cvkvur3aybp', '', 4, '蓝狐网络', '', '', '', '', '', '', '', '', 0, '', '', 1, '', '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_account_wxapp`
--

CREATE TABLE IF NOT EXISTS `ims_account_wxapp` (
  `acid` int(10) unsigned NOT NULL,
  `uniacid` int(10) NOT NULL,
  `token` varchar(32) NOT NULL,
  `encodingaeskey` varchar(43) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `account` varchar(30) NOT NULL,
  `original` varchar(50) NOT NULL,
  `key` varchar(50) NOT NULL,
  `secret` varchar(50) NOT NULL,
  `name` varchar(30) NOT NULL,
  `appdomain` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_account_wxapp`
--

INSERT INTO `ims_account_wxapp` (`acid`, `uniacid`, `token`, `encodingaeskey`, `level`, `account`, `original`, `key`, `secret`, `name`, `appdomain`) VALUES
(2, 2, 'AzerCew1KceerlvKGR0k0RL1e0W2Jkw2', 'CmaZ2wa245aho4hhhrHzmawHrb6a0H2PB4P6hr4Pp26', 1, '', 'gh_c24751cd7e27', 'wxfb2115622142e037', '37d9773b5a5448361308c382ea57a550', '交警队', '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_article_category`
--

CREATE TABLE IF NOT EXISTS `ims_article_category` (
  `id` int(10) unsigned NOT NULL,
  `title` varchar(30) NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `type` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_article_news`
--

CREATE TABLE IF NOT EXISTS `ims_article_news` (
  `id` int(10) unsigned NOT NULL,
  `cateid` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `is_display` tinyint(3) unsigned NOT NULL,
  `is_show_home` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `click` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_article_notice`
--

CREATE TABLE IF NOT EXISTS `ims_article_notice` (
  `id` int(10) unsigned NOT NULL,
  `cateid` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `is_display` tinyint(3) unsigned NOT NULL,
  `is_show_home` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `click` int(10) unsigned NOT NULL,
  `style` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_article_unread_notice`
--

CREATE TABLE IF NOT EXISTS `ims_article_unread_notice` (
  `id` int(10) unsigned NOT NULL,
  `notice_id` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `is_new` tinyint(3) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_basic_reply`
--

CREATE TABLE IF NOT EXISTS `ims_basic_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `content` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_business`
--

CREATE TABLE IF NOT EXISTS `ims_business` (
  `id` int(10) unsigned NOT NULL,
  `weid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `province` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `dist` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `lng` varchar(10) NOT NULL,
  `lat` varchar(10) NOT NULL,
  `industry1` varchar(10) NOT NULL,
  `industry2` varchar(10) NOT NULL,
  `createtime` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_attachment`
--

CREATE TABLE IF NOT EXISTS `ims_core_attachment` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `module_upload_dir` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_core_attachment`
--

INSERT INTO `ims_core_attachment` (`id`, `uniacid`, `uid`, `filename`, `attachment`, `type`, `createtime`, `module_upload_dir`) VALUES
(1, 2, 1, 'add.png', 'images/2/2018/06/M4q0PQjPXYG10kg2Szp60ZjsZjY442.png', 1, 1528161116, ''),
(2, 2, 1, 'timg.jpg', 'images/2/2018/06/dkt1199eEN91q6D9eETDnT7049dK19.jpg', 1, 1528167731, ''),
(3, 2, 1, 'ChMkJlfJVX6IYRLdAAxCj6-OCggAAU94AOTAlQADEKn856.jpg', 'images/2/2018/06/mwlFh3luf5rFQR0t0z40F44ZOWU44q.jpg', 1, 1528356361, ''),
(4, 2, 1, 'ChMkJlfJWFmIKTtiAAJLKfn7R8wAAU-JQIng0YAAktB435.jpg', 'images/2/2018/06/xz43lp2727R3ct333YMMRy4YpzZ3Yp.jpg', 1, 1528356361, '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_cache`
--

CREATE TABLE IF NOT EXISTS `ims_core_cache` (
  `key` varchar(50) NOT NULL,
  `value` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_core_cache`
--

INSERT INTO `ims_core_cache` (`key`, `value`) VALUES
('setting', 'a:4:{s:8:"authmode";i:1;s:5:"close";a:2:{s:6:"status";s:1:"0";s:6:"reason";s:0:"";}s:8:"register";a:4:{s:4:"open";i:1;s:6:"verify";i:0;s:4:"code";i:1;s:7:"groupid";i:1;}s:9:"copyright";a:1:{s:6:"slides";a:3:{i:0;s:58:"https://img.alicdn.com/tps/TB1pfG4IFXXXXc6XXXXXXXXXXXX.jpg";i:1;s:58:"https://img.alicdn.com/tps/TB1sXGYIFXXXXc5XpXXXXXXXXXX.jpg";i:2;s:58:"https://img.alicdn.com/tps/TB1h9xxIFXXXXbKXXXXXXXXXXXX.jpg";}}}'),
('system_frame', 'a:8:{s:7:"account";a:7:{s:5:"title";s:9:"公众号";s:4:"icon";s:18:"wi wi-white-collar";s:3:"url";s:41:"./index.php?c=home&a=welcome&do=platform&";s:7:"section";a:5:{s:13:"platform_plus";a:2:{s:5:"title";s:12:"增强功能";s:4:"menu";a:6:{s:14:"platform_reply";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"自动回复";s:3:"url";s:31:"./index.php?c=platform&a=reply&";s:15:"permission_name";s:14:"platform_reply";s:4:"icon";s:11:"wi wi-reply";s:12:"displayorder";i:6;s:2:"id";N;s:14:"sub_permission";a:0:{}}s:13:"platform_menu";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"自定义菜单";s:3:"url";s:38:"./index.php?c=platform&a=menu&do=post&";s:15:"permission_name";s:13:"platform_menu";s:4:"icon";s:16:"wi wi-custommenu";s:12:"displayorder";i:5;s:2:"id";N;s:14:"sub_permission";N;}s:11:"platform_qr";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:22:"二维码/转化链接";s:3:"url";s:28:"./index.php?c=platform&a=qr&";s:15:"permission_name";s:11:"platform_qr";s:4:"icon";s:12:"wi wi-qrcode";s:12:"displayorder";i:4;s:2:"id";N;s:14:"sub_permission";a:0:{}}s:18:"platform_mass_task";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"定时群发";s:3:"url";s:30:"./index.php?c=platform&a=mass&";s:15:"permission_name";s:18:"platform_mass_task";s:4:"icon";s:13:"wi wi-crontab";s:12:"displayorder";i:3;s:2:"id";N;s:14:"sub_permission";N;}s:17:"platform_material";a:9:{s:9:"is_system";i:1;s:10:"is_display";s:1:"1";s:5:"title";s:16:"素材/编辑器";s:3:"url";s:34:"./index.php?c=platform&a=material&";s:15:"permission_name";s:17:"platform_material";s:4:"icon";s:12:"wi wi-redact";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";a:2:{i:0;a:3:{s:5:"title";s:13:"添加/编辑";s:3:"url";s:39:"./index.php?c=platform&a=material-post&";s:15:"permission_name";s:13:"material_post";}i:1;a:2:{s:5:"title";s:6:"删除";s:15:"permission_name";s:24:"platform_material_delete";}}}s:13:"platform_site";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:16:"微官网-文章";s:3:"url";s:38:"./index.php?c=site&a=multi&do=display&";s:15:"permission_name";s:13:"platform_site";s:4:"icon";s:10:"wi wi-home";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";a:0:{}}}}s:15:"platform_module";a:3:{s:5:"title";s:12:"应用模块";s:4:"menu";a:0:{}s:10:"is_display";b:1;}s:2:"mc";a:2:{s:5:"title";s:6:"粉丝";s:4:"menu";a:2:{s:7:"mc_fans";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"粉丝管理";s:3:"url";s:24:"./index.php?c=mc&a=fans&";s:15:"permission_name";s:7:"mc_fans";s:4:"icon";s:16:"wi wi-fansmanage";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";N;}s:9:"mc_member";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"会员管理";s:3:"url";s:26:"./index.php?c=mc&a=member&";s:15:"permission_name";s:9:"mc_member";s:4:"icon";s:10:"wi wi-fans";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:7:"profile";a:2:{s:5:"title";s:6:"配置";s:4:"menu";a:3:{s:7:"profile";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"参数配置";s:3:"url";s:33:"./index.php?c=profile&a=passport&";s:15:"permission_name";s:15:"profile_setting";s:4:"icon";s:23:"wi wi-parameter-setting";s:12:"displayorder";i:3;s:2:"id";N;s:14:"sub_permission";N;}s:7:"payment";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"支付参数";s:3:"url";s:32:"./index.php?c=profile&a=payment&";s:15:"permission_name";s:19:"profile_pay_setting";s:4:"icon";s:17:"wi wi-pay-setting";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";N;}s:11:"bind_domain";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"域名绑定";s:3:"url";s:36:"./index.php?c=profile&a=bind-domain&";s:15:"permission_name";s:19:"profile_bind_domain";s:4:"icon";s:23:"wi wi-parameter-setting";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:10:"statistics";a:2:{s:5:"title";s:6:"统计";s:4:"menu";a:1:{s:3:"app";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"访问统计";s:3:"url";s:31:"./index.php?c=statistics&a=app&";s:15:"permission_name";s:14:"statistics_app";s:4:"icon";s:9:"wi wi-api";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}}s:9:"is_system";b:1;s:10:"is_display";b:1;s:12:"displayorder";i:2;}s:5:"wxapp";a:7:{s:5:"title";s:9:"小程序";s:4:"icon";s:19:"wi wi-small-routine";s:3:"url";s:38:"./index.php?c=wxapp&a=display&do=home&";s:7:"section";a:3:{s:14:"wxapp_entrance";a:3:{s:5:"title";s:15:"小程序入口";s:4:"menu";a:1:{s:20:"module_entrance_link";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"入口页面";s:3:"url";s:36:"./index.php?c=wxapp&a=entrance-link&";s:15:"permission_name";s:19:"wxapp_entrance_link";s:4:"icon";s:18:"wi wi-data-synchro";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}s:10:"is_display";b:1;}s:12:"wxapp_module";a:3:{s:5:"title";s:6:"应用";s:4:"menu";a:0:{}s:10:"is_display";b:1;}s:20:"platform_manage_menu";a:2:{s:5:"title";s:6:"管理";s:4:"menu";a:4:{s:11:"module_link";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"数据同步";s:3:"url";s:42:"./index.php?c=wxapp&a=module-link-uniacid&";s:15:"permission_name";s:25:"wxapp_module_link_uniacid";s:4:"icon";s:18:"wi wi-data-synchro";s:12:"displayorder";i:4;s:2:"id";N;s:14:"sub_permission";N;}s:13:"wxapp_profile";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"支付参数";s:3:"url";s:30:"./index.php?c=wxapp&a=payment&";s:15:"permission_name";s:13:"wxapp_payment";s:4:"icon";s:16:"wi wi-appsetting";s:12:"displayorder";i:3;s:2:"id";N;s:14:"sub_permission";N;}s:14:"front_download";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:18:"上传微信审核";s:3:"url";s:37:"./index.php?c=wxapp&a=front-download&";s:15:"permission_name";s:20:"wxapp_front_download";s:4:"icon";s:13:"wi wi-examine";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";N;}s:23:"wxapp_platform_material";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:0;s:5:"title";s:12:"素材管理";s:3:"url";N;s:15:"permission_name";s:23:"wxapp_platform_material";s:4:"icon";N;s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";a:1:{i:0;a:2:{s:5:"title";s:6:"删除";s:15:"permission_name";s:30:"wxapp_platform_material_delete";}}}}}}s:9:"is_system";b:1;s:10:"is_display";b:1;s:12:"displayorder";i:3;}s:6:"webapp";a:7:{s:5:"title";s:2:"PC";s:4:"icon";s:18:"wi wi-white-collar";s:3:"url";s:39:"./index.php?c=webapp&a=home&do=display&";s:7:"section";a:2:{s:15:"platform_module";a:3:{s:5:"title";s:12:"应用模块";s:4:"menu";a:0:{}s:10:"is_display";b:1;}s:2:"mc";a:2:{s:5:"title";s:6:"粉丝";s:4:"menu";a:1:{s:9:"mc_member";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"会员管理";s:3:"url";s:26:"./index.php?c=mc&a=member&";s:15:"permission_name";s:9:"mc_member";s:4:"icon";s:10:"wi wi-fans";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}}s:9:"is_system";b:1;s:10:"is_display";b:1;s:12:"displayorder";i:4;}s:6:"module";a:7:{s:5:"title";s:6:"应用";s:4:"icon";s:11:"wi wi-apply";s:3:"url";s:31:"./index.php?c=module&a=display&";s:7:"section";a:0:{}s:9:"is_system";b:1;s:10:"is_display";b:1;s:12:"displayorder";i:5;}s:6:"system";a:7:{s:5:"title";s:6:"系统";s:4:"icon";s:13:"wi wi-setting";s:3:"url";s:39:"./index.php?c=home&a=welcome&do=system&";s:7:"section";a:10:{s:10:"wxplatform";a:2:{s:5:"title";s:9:"公众号";s:4:"menu";a:4:{s:14:"system_account";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:16:" 微信公众号";s:3:"url";s:45:"./index.php?c=account&a=manage&account_type=1";s:15:"permission_name";s:14:"system_account";s:4:"icon";s:12:"wi wi-wechat";s:12:"displayorder";i:4;s:2:"id";N;s:14:"sub_permission";a:6:{i:0;a:2:{s:5:"title";s:21:"公众号管理设置";s:15:"permission_name";s:21:"system_account_manage";}i:1;a:2:{s:5:"title";s:15:"添加公众号";s:15:"permission_name";s:19:"system_account_post";}i:2;a:2:{s:5:"title";s:15:"公众号停用";s:15:"permission_name";s:19:"system_account_stop";}i:3;a:2:{s:5:"title";s:18:"公众号回收站";s:15:"permission_name";s:22:"system_account_recycle";}i:4;a:2:{s:5:"title";s:15:"公众号删除";s:15:"permission_name";s:21:"system_account_delete";}i:5;a:2:{s:5:"title";s:15:"公众号恢复";s:15:"permission_name";s:22:"system_account_recover";}}}s:13:"system_module";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"公众号应用";s:3:"url";s:51:"./index.php?c=module&a=manage-system&account_type=1";s:15:"permission_name";s:13:"system_module";s:4:"icon";s:14:"wi wi-wx-apply";s:12:"displayorder";i:3;s:2:"id";N;s:14:"sub_permission";N;}s:15:"system_template";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"微官网模板";s:3:"url";s:32:"./index.php?c=system&a=template&";s:15:"permission_name";s:15:"system_template";s:4:"icon";s:17:"wi wi-wx-template";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";N;}s:15:"system_platform";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:19:" 微信开放平台";s:3:"url";s:32:"./index.php?c=system&a=platform&";s:15:"permission_name";s:15:"system_platform";s:4:"icon";s:20:"wi wi-exploitsetting";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:6:"module";a:2:{s:5:"title";s:9:"小程序";s:4:"menu";a:2:{s:12:"system_wxapp";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"微信小程序";s:3:"url";s:45:"./index.php?c=account&a=manage&account_type=4";s:15:"permission_name";s:12:"system_wxapp";s:4:"icon";s:11:"wi wi-wxapp";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";a:6:{i:0;a:2:{s:5:"title";s:21:"小程序管理设置";s:15:"permission_name";s:19:"system_wxapp_manage";}i:1;a:2:{s:5:"title";s:15:"添加小程序";s:15:"permission_name";s:17:"system_wxapp_post";}i:2;a:2:{s:5:"title";s:15:"小程序停用";s:15:"permission_name";s:17:"system_wxapp_stop";}i:3;a:2:{s:5:"title";s:18:"小程序回收站";s:15:"permission_name";s:20:"system_wxapp_recycle";}i:4;a:2:{s:5:"title";s:15:"小程序删除";s:15:"permission_name";s:19:"system_wxapp_delete";}i:5;a:2:{s:5:"title";s:15:"小程序恢复";s:15:"permission_name";s:20:"system_wxapp_recover";}}}s:19:"system_module_wxapp";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"小程序应用";s:3:"url";s:51:"./index.php?c=module&a=manage-system&account_type=4";s:15:"permission_name";s:19:"system_module_wxapp";s:4:"icon";s:17:"wi wi-wxapp-apply";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:7:"welcome";a:3:{s:5:"title";s:12:"系统首页";s:4:"menu";a:1:{s:14:"system_welcome";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:18:"系统首页应用";s:3:"url";s:53:"./index.php?c=module&a=manage-system&system_welcome=1";s:15:"permission_name";s:14:"system_welcome";s:4:"icon";s:11:"wi wi-wxapp";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}s:7:"founder";b:1;}s:6:"webapp";a:2:{s:5:"title";s:2:"PC";s:4:"menu";a:2:{s:12:"system_wxapp";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:2:"PC";s:3:"url";s:45:"./index.php?c=account&a=manage&account_type=5";s:15:"permission_name";s:13:"system_webapp";s:4:"icon";s:11:"wi wi-wxapp";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";a:0:{}}s:19:"system_module_wxapp";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:8:"PC应用";s:3:"url";s:51:"./index.php?c=module&a=manage-system&account_type=5";s:15:"permission_name";s:19:"system_module_wxapp";s:4:"icon";s:17:"wi wi-wxapp-apply";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:4:"user";a:2:{s:5:"title";s:13:"帐户/用户";s:4:"menu";a:3:{s:9:"system_my";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"我的帐户";s:3:"url";s:29:"./index.php?c=user&a=profile&";s:15:"permission_name";s:9:"system_my";s:4:"icon";s:10:"wi wi-user";s:12:"displayorder";i:3;s:2:"id";N;s:14:"sub_permission";N;}s:11:"system_user";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"用户管理";s:3:"url";s:29:"./index.php?c=user&a=display&";s:15:"permission_name";s:11:"system_user";s:4:"icon";s:16:"wi wi-user-group";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";a:7:{i:0;a:2:{s:5:"title";s:12:"编辑用户";s:15:"permission_name";s:16:"system_user_post";}i:1;a:2:{s:5:"title";s:12:"审核用户";s:15:"permission_name";s:17:"system_user_check";}i:2;a:2:{s:5:"title";s:12:"店员管理";s:15:"permission_name";s:17:"system_user_clerk";}i:3;a:2:{s:5:"title";s:15:"用户回收站";s:15:"permission_name";s:19:"system_user_recycle";}i:4;a:2:{s:5:"title";s:18:"用户属性设置";s:15:"permission_name";s:18:"system_user_fields";}i:5;a:2:{s:5:"title";s:31:"用户属性设置-编辑字段";s:15:"permission_name";s:23:"system_user_fields_post";}i:6;a:2:{s:5:"title";s:18:"用户注册设置";s:15:"permission_name";s:23:"system_user_registerset";}}}s:25:"system_user_founder_group";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"副创始人组";s:3:"url";s:32:"./index.php?c=founder&a=display&";s:15:"permission_name";s:21:"system_founder_manage";s:4:"icon";s:16:"wi wi-co-founder";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";a:6:{i:0;a:2:{s:5:"title";s:18:"添加创始人组";s:15:"permission_name";s:24:"system_founder_group_add";}i:1;a:2:{s:5:"title";s:18:"编辑创始人组";s:15:"permission_name";s:25:"system_founder_group_post";}i:2;a:2:{s:5:"title";s:18:"删除创始人组";s:15:"permission_name";s:24:"system_founder_group_del";}i:3;a:2:{s:5:"title";s:15:"添加创始人";s:15:"permission_name";s:23:"system_founder_user_add";}i:4;a:2:{s:5:"title";s:15:"编辑创始人";s:15:"permission_name";s:24:"system_founder_user_post";}i:5;a:2:{s:5:"title";s:15:"删除创始人";s:15:"permission_name";s:23:"system_founder_user_del";}}}}}s:10:"permission";a:2:{s:5:"title";s:12:"权限管理";s:4:"menu";a:2:{s:19:"system_module_group";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"应用权限组";s:3:"url";s:29:"./index.php?c=module&a=group&";s:15:"permission_name";s:19:"system_module_group";s:4:"icon";s:21:"wi wi-appjurisdiction";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";a:3:{i:0;a:2:{s:5:"title";s:21:"添加应用权限组";s:15:"permission_name";s:23:"system_module_group_add";}i:1;a:2:{s:5:"title";s:21:"编辑应用权限组";s:15:"permission_name";s:24:"system_module_group_post";}i:2;a:2:{s:5:"title";s:21:"删除应用权限组";s:15:"permission_name";s:23:"system_module_group_del";}}}s:17:"system_user_group";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"用户权限组";s:3:"url";s:27:"./index.php?c=user&a=group&";s:15:"permission_name";s:17:"system_user_group";s:4:"icon";s:22:"wi wi-userjurisdiction";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";a:3:{i:0;a:2:{s:5:"title";s:15:"添加用户组";s:15:"permission_name";s:21:"system_user_group_add";}i:1;a:2:{s:5:"title";s:15:"编辑用户组";s:15:"permission_name";s:22:"system_user_group_post";}i:2;a:2:{s:5:"title";s:15:"删除用户组";s:15:"permission_name";s:21:"system_user_group_del";}}}}}s:7:"article";a:2:{s:5:"title";s:13:"文章/公告";s:4:"menu";a:2:{s:14:"system_article";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"文章管理";s:3:"url";s:29:"./index.php?c=article&a=news&";s:15:"permission_name";s:19:"system_article_news";s:4:"icon";s:13:"wi wi-article";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";N;}s:21:"system_article_notice";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"公告管理";s:3:"url";s:31:"./index.php?c=article&a=notice&";s:15:"permission_name";s:21:"system_article_notice";s:4:"icon";s:12:"wi wi-notice";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:7:"message";a:2:{s:5:"title";s:12:"消息提醒";s:4:"menu";a:1:{s:21:"system_message_notice";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"消息提醒";s:3:"url";s:31:"./index.php?c=message&a=notice&";s:15:"permission_name";s:21:"system_message_notice";s:4:"icon";s:13:"wi wi-article";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:17:"system_statistics";a:2:{s:5:"title";s:6:"统计";s:4:"menu";a:1:{s:23:"system_account_analysis";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"访问统计";s:3:"url";s:35:"./index.php?c=statistics&a=account&";s:15:"permission_name";s:23:"system_account_analysis";s:4:"icon";s:13:"wi wi-article";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:5:"cache";a:2:{s:5:"title";s:6:"缓存";s:4:"menu";a:1:{s:26:"system_setting_updatecache";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"更新缓存";s:3:"url";s:35:"./index.php?c=system&a=updatecache&";s:15:"permission_name";s:26:"system_setting_updatecache";s:4:"icon";s:12:"wi wi-update";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}}s:9:"is_system";b:1;s:10:"is_display";b:1;s:12:"displayorder";i:6;}s:4:"site";a:8:{s:5:"title";s:6:"站点";s:4:"icon";s:17:"wi wi-system-site";s:3:"url";s:28:"./index.php?c=system&a=site&";s:7:"section";a:3:{s:5:"cloud";a:2:{s:5:"title";s:9:"云服务";s:4:"menu";a:3:{s:14:"system_profile";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"系统升级";s:3:"url";s:30:"./index.php?c=cloud&a=upgrade&";s:15:"permission_name";s:20:"system_cloud_upgrade";s:4:"icon";s:11:"wi wi-cache";s:12:"displayorder";i:3;s:2:"id";N;s:14:"sub_permission";N;}s:21:"system_cloud_register";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"注册站点";s:3:"url";s:30:"./index.php?c=cloud&a=profile&";s:15:"permission_name";s:21:"system_cloud_register";s:4:"icon";s:18:"wi wi-registersite";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";N;}s:21:"system_cloud_diagnose";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"云服务诊断";s:3:"url";s:31:"./index.php?c=cloud&a=diagnose&";s:15:"permission_name";s:21:"system_cloud_diagnose";s:4:"icon";s:14:"wi wi-diagnose";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:7:"setting";a:2:{s:5:"title";s:6:"设置";s:4:"menu";a:9:{s:19:"system_setting_site";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"站点设置";s:3:"url";s:28:"./index.php?c=system&a=site&";s:15:"permission_name";s:19:"system_setting_site";s:4:"icon";s:18:"wi wi-site-setting";s:12:"displayorder";i:9;s:2:"id";N;s:14:"sub_permission";N;}s:19:"system_setting_menu";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"菜单设置";s:3:"url";s:28:"./index.php?c=system&a=menu&";s:15:"permission_name";s:19:"system_setting_menu";s:4:"icon";s:18:"wi wi-menu-setting";s:12:"displayorder";i:8;s:2:"id";N;s:14:"sub_permission";N;}s:25:"system_setting_attachment";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"附件设置";s:3:"url";s:34:"./index.php?c=system&a=attachment&";s:15:"permission_name";s:25:"system_setting_attachment";s:4:"icon";s:16:"wi wi-attachment";s:12:"displayorder";i:7;s:2:"id";N;s:14:"sub_permission";N;}s:25:"system_setting_systeminfo";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"系统信息";s:3:"url";s:34:"./index.php?c=system&a=systeminfo&";s:15:"permission_name";s:25:"system_setting_systeminfo";s:4:"icon";s:17:"wi wi-system-info";s:12:"displayorder";i:6;s:2:"id";N;s:14:"sub_permission";N;}s:19:"system_setting_logs";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"查看日志";s:3:"url";s:28:"./index.php?c=system&a=logs&";s:15:"permission_name";s:19:"system_setting_logs";s:4:"icon";s:9:"wi wi-log";s:12:"displayorder";i:5;s:2:"id";N;s:14:"sub_permission";N;}s:26:"system_setting_ipwhitelist";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:11:"IP白名单";s:3:"url";s:35:"./index.php?c=system&a=ipwhitelist&";s:15:"permission_name";s:26:"system_setting_ipwhitelist";s:4:"icon";s:8:"wi wi-ip";s:12:"displayorder";i:4;s:2:"id";N;s:14:"sub_permission";N;}s:28:"system_setting_sensitiveword";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"过滤敏感词";s:3:"url";s:37:"./index.php?c=system&a=sensitiveword&";s:15:"permission_name";s:28:"system_setting_sensitiveword";s:4:"icon";s:15:"wi wi-sensitive";s:12:"displayorder";i:3;s:2:"id";N;s:14:"sub_permission";N;}s:25:"system_setting_thirdlogin";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:21:"第三方登录配置";s:3:"url";s:34:"./index.php?c=system&a=thirdlogin&";s:15:"permission_name";s:25:"system_setting_thirdlogin";s:4:"icon";s:15:"wi wi-sensitive";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";N;}s:20:"system_setting_oauth";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:17:"oauth全局设置";s:3:"url";s:29:"./index.php?c=system&a=oauth&";s:15:"permission_name";s:20:"system_setting_oauth";s:4:"icon";s:15:"wi wi-sensitive";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}s:7:"utility";a:2:{s:5:"title";s:12:"常用工具";s:4:"menu";a:5:{s:24:"system_utility_filecheck";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:18:"系统文件校验";s:3:"url";s:33:"./index.php?c=system&a=filecheck&";s:15:"permission_name";s:24:"system_utility_filecheck";s:4:"icon";s:10:"wi wi-file";s:12:"displayorder";i:5;s:2:"id";N;s:14:"sub_permission";N;}s:23:"system_utility_optimize";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"性能优化";s:3:"url";s:32:"./index.php?c=system&a=optimize&";s:15:"permission_name";s:23:"system_utility_optimize";s:4:"icon";s:14:"wi wi-optimize";s:12:"displayorder";i:4;s:2:"id";N;s:14:"sub_permission";N;}s:23:"system_utility_database";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:9:"数据库";s:3:"url";s:32:"./index.php?c=system&a=database&";s:15:"permission_name";s:23:"system_utility_database";s:4:"icon";s:9:"wi wi-sql";s:12:"displayorder";i:3;s:2:"id";N;s:14:"sub_permission";N;}s:19:"system_utility_scan";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:12:"木马查杀";s:3:"url";s:28:"./index.php?c=system&a=scan&";s:15:"permission_name";s:19:"system_utility_scan";s:4:"icon";s:12:"wi wi-safety";s:12:"displayorder";i:2;s:2:"id";N;s:14:"sub_permission";N;}s:18:"system_utility_bom";a:9:{s:9:"is_system";i:1;s:10:"is_display";i:1;s:5:"title";s:15:"检测文件BOM";s:3:"url";s:27:"./index.php?c=system&a=bom&";s:15:"permission_name";s:18:"system_utility_bom";s:4:"icon";s:9:"wi wi-bom";s:12:"displayorder";i:1;s:2:"id";N;s:14:"sub_permission";N;}}}}s:7:"founder";b:1;s:9:"is_system";b:1;s:10:"is_display";b:1;s:12:"displayorder";i:7;}s:4:"help";a:8:{s:5:"title";s:12:"系统帮助";s:4:"icon";s:12:"wi wi-market";s:3:"url";s:29:"./index.php?c=help&a=display&";s:7:"section";a:0:{}s:5:"blank";b:0;s:9:"is_system";b:1;s:10:"is_display";b:1;s:12:"displayorder";i:8;}s:5:"store";a:7:{s:5:"title";s:6:"商城";s:4:"icon";s:11:"wi wi-store";s:3:"url";s:43:"./index.php?c=home&a=welcome&do=ext&m=store";s:7:"section";a:0:{}s:9:"is_system";b:1;s:10:"is_display";b:1;s:12:"displayorder";i:9;}}'),
('userbasefields', 'a:45:{s:7:"uniacid";s:17:"同一公众号id";s:7:"groupid";s:8:"分组id";s:7:"credit1";s:6:"积分";s:7:"credit2";s:6:"余额";s:7:"credit3";s:19:"预留积分类型3";s:7:"credit4";s:19:"预留积分类型4";s:7:"credit5";s:19:"预留积分类型5";s:7:"credit6";s:19:"预留积分类型6";s:10:"createtime";s:12:"加入时间";s:6:"mobile";s:12:"手机号码";s:5:"email";s:12:"电子邮箱";s:8:"realname";s:12:"真实姓名";s:8:"nickname";s:6:"昵称";s:6:"avatar";s:6:"头像";s:2:"qq";s:5:"QQ号";s:6:"gender";s:6:"性别";s:5:"birth";s:6:"生日";s:13:"constellation";s:6:"星座";s:6:"zodiac";s:6:"生肖";s:9:"telephone";s:12:"固定电话";s:6:"idcard";s:12:"证件号码";s:9:"studentid";s:6:"学号";s:5:"grade";s:6:"班级";s:7:"address";s:6:"地址";s:7:"zipcode";s:6:"邮编";s:11:"nationality";s:6:"国籍";s:6:"reside";s:9:"居住地";s:14:"graduateschool";s:12:"毕业学校";s:7:"company";s:6:"公司";s:9:"education";s:6:"学历";s:10:"occupation";s:6:"职业";s:8:"position";s:6:"职位";s:7:"revenue";s:9:"年收入";s:15:"affectivestatus";s:12:"情感状态";s:10:"lookingfor";s:13:" 交友目的";s:9:"bloodtype";s:6:"血型";s:6:"height";s:6:"身高";s:6:"weight";s:6:"体重";s:6:"alipay";s:15:"支付宝帐号";s:3:"msn";s:3:"MSN";s:6:"taobao";s:12:"阿里旺旺";s:4:"site";s:6:"主页";s:3:"bio";s:12:"自我介绍";s:8:"interest";s:12:"兴趣爱好";s:8:"password";s:6:"密码";}'),
('usersfields', 'a:46:{s:8:"realname";s:12:"真实姓名";s:8:"nickname";s:6:"昵称";s:6:"avatar";s:6:"头像";s:2:"qq";s:5:"QQ号";s:6:"mobile";s:12:"手机号码";s:3:"vip";s:9:"VIP级别";s:6:"gender";s:6:"性别";s:9:"birthyear";s:12:"出生生日";s:13:"constellation";s:6:"星座";s:6:"zodiac";s:6:"生肖";s:9:"telephone";s:12:"固定电话";s:6:"idcard";s:12:"证件号码";s:9:"studentid";s:6:"学号";s:5:"grade";s:6:"班级";s:7:"address";s:12:"邮寄地址";s:7:"zipcode";s:6:"邮编";s:11:"nationality";s:6:"国籍";s:14:"resideprovince";s:12:"居住地址";s:14:"graduateschool";s:12:"毕业学校";s:7:"company";s:6:"公司";s:9:"education";s:6:"学历";s:10:"occupation";s:6:"职业";s:8:"position";s:6:"职位";s:7:"revenue";s:9:"年收入";s:15:"affectivestatus";s:12:"情感状态";s:10:"lookingfor";s:13:" 交友目的";s:9:"bloodtype";s:6:"血型";s:6:"height";s:6:"身高";s:6:"weight";s:6:"体重";s:6:"alipay";s:15:"支付宝帐号";s:3:"msn";s:3:"MSN";s:5:"email";s:12:"电子邮箱";s:6:"taobao";s:12:"阿里旺旺";s:4:"site";s:6:"主页";s:3:"bio";s:12:"自我介绍";s:8:"interest";s:12:"兴趣爱好";s:7:"uniacid";s:17:"同一公众号id";s:7:"groupid";s:8:"分组id";s:7:"credit1";s:6:"积分";s:7:"credit2";s:6:"余额";s:7:"credit3";s:19:"预留积分类型3";s:7:"credit4";s:19:"预留积分类型4";s:7:"credit5";s:19:"预留积分类型5";s:7:"credit6";s:19:"预留积分类型6";s:10:"createtime";s:12:"加入时间";s:8:"password";s:12:"用户密码";}'),
('module_receive_enable', 'a:0:{}'),
('we7::site_store_buy_1', 'a:0:{}'),
('we7:module_info:zh_gjhdbm', 'a:30:{s:3:"mid";s:2:"12";s:4:"name";s:9:"zh_gjhdbm";s:4:"type";s:8:"activity";s:5:"title";s:21:"活动报名高级版";s:7:"version";s:3:"3.4";s:7:"ability";s:212:"找好活动、办好活动,用活动行!活动行提供创业、互联网、科技、金融、教育、亲子、生活、聚会交友等50多种活动,是深受主办方信赖和城市白领喜爱的活动平台";s:11:"description";s:212:"找好活动、办好活动,用活动行!活动行提供创业、互联网、科技、金融、教育、亲子、生活、聚会交友等50多种活动,是深受主办方信赖和城市白领喜爱的活动平台";s:6:"author";s:12:"微猫源码";s:3:"url";s:25:"http://www.weixin2015.cn/";s:8:"settings";s:1:"0";s:10:"subscribes";a:0:{}s:7:"handles";a:0:{}s:12:"isrulefields";s:1:"0";s:8:"issystem";s:1:"0";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:2:"N;";s:13:"title_initial";s:1:"H";s:13:"wxapp_support";s:1:"2";s:11:"app_support";s:1:"1";s:15:"welcome_support";s:1:"1";s:10:"oauth_type";s:1:"1";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:57:"http://jjd.aodd.cn/addons/zh_gjhdbm/icon.jpg?v=1528101878";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:basic', 'a:30:{s:3:"mid";s:1:"1";s:4:"name";s:5:"basic";s:4:"type";s:6:"system";s:5:"title";s:18:"基本文字回复";s:7:"version";s:3:"1.0";s:7:"ability";s:24:"和您进行简单对话";s:11:"description";s:201:"一问一答得简单对话. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的回复内容.";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:53:"http://jjd.aodd.cn/addons/basic/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:news', 'a:30:{s:3:"mid";s:1:"2";s:4:"name";s:4:"news";s:4:"type";s:6:"system";s:5:"title";s:24:"基本混合图文回复";s:7:"version";s:3:"1.0";s:7:"ability";s:33:"为你提供生动的图文资讯";s:11:"description";s:272:"一问一答得简单对话, 但是回复内容包括图片文字等更生动的媒体内容. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的图文回复内容.";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:52:"http://jjd.aodd.cn/addons/news/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:music', 'a:30:{s:3:"mid";s:1:"3";s:4:"name";s:5:"music";s:4:"type";s:6:"system";s:5:"title";s:18:"基本音乐回复";s:7:"version";s:3:"1.0";s:7:"ability";s:39:"提供语音、音乐等音频类回复";s:11:"description";s:183:"在回复规则中可选择具有语音、音乐等音频类的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝，实现一问一答得简单对话。";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:53:"http://jjd.aodd.cn/addons/music/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:userapi', 'a:30:{s:3:"mid";s:1:"4";s:4:"name";s:7:"userapi";s:4:"type";s:6:"system";s:5:"title";s:21:"自定义接口回复";s:7:"version";s:3:"1.1";s:7:"ability";s:33:"更方便的第三方接口设置";s:11:"description";s:141:"自定义接口又称第三方接口，可以让开发者更方便的接入微擎系统，高效的与微信公众平台进行对接整合。";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:55:"http://jjd.aodd.cn/addons/userapi/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:recharge', 'a:30:{s:3:"mid";s:1:"5";s:4:"name";s:8:"recharge";s:4:"type";s:6:"system";s:5:"title";s:24:"会员中心充值模块";s:7:"version";s:3:"1.0";s:7:"ability";s:24:"提供会员充值功能";s:11:"description";s:0:"";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"0";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:56:"http://jjd.aodd.cn/addons/recharge/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:custom', 'a:30:{s:3:"mid";s:1:"6";s:4:"name";s:6:"custom";s:4:"type";s:6:"system";s:5:"title";s:15:"多客服转接";s:7:"version";s:5:"1.0.0";s:7:"ability";s:36:"用来接入腾讯的多客服系统";s:11:"description";s:0:"";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:17:"http://bbs.we7.cc";s:8:"settings";s:1:"0";s:10:"subscribes";a:0:{}s:7:"handles";a:6:{i:0;s:5:"image";i:1;s:5:"voice";i:2;s:5:"video";i:3;s:8:"location";i:4;s:4:"link";i:5;s:4:"text";}s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:54:"http://jjd.aodd.cn/addons/custom/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:images', 'a:30:{s:3:"mid";s:1:"7";s:4:"name";s:6:"images";s:4:"type";s:6:"system";s:5:"title";s:18:"基本图片回复";s:7:"version";s:3:"1.0";s:7:"ability";s:18:"提供图片回复";s:11:"description";s:132:"在回复规则中可选择具有图片的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝图片。";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:54:"http://jjd.aodd.cn/addons/images/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:video', 'a:30:{s:3:"mid";s:1:"8";s:4:"name";s:5:"video";s:4:"type";s:6:"system";s:5:"title";s:18:"基本视频回复";s:7:"version";s:3:"1.0";s:7:"ability";s:18:"提供图片回复";s:11:"description";s:132:"在回复规则中可选择具有视频的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝视频。";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:53:"http://jjd.aodd.cn/addons/video/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:voice', 'a:30:{s:3:"mid";s:1:"9";s:4:"name";s:5:"voice";s:4:"type";s:6:"system";s:5:"title";s:18:"基本语音回复";s:7:"version";s:3:"1.0";s:7:"ability";s:18:"提供语音回复";s:11:"description";s:132:"在回复规则中可选择具有语音的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝语音。";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:53:"http://jjd.aodd.cn/addons/voice/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:chats', 'a:30:{s:3:"mid";s:2:"10";s:4:"name";s:5:"chats";s:4:"type";s:6:"system";s:5:"title";s:18:"发送客服消息";s:7:"version";s:3:"1.0";s:7:"ability";s:77:"公众号可以在粉丝最后发送消息的48小时内无限制发送消息";s:11:"description";s:0:"";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"0";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:53:"http://jjd.aodd.cn/addons/chats/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:module_info:wxcard', 'a:30:{s:3:"mid";s:2:"11";s:4:"name";s:6:"wxcard";s:4:"type";s:6:"system";s:5:"title";s:18:"微信卡券回复";s:7:"version";s:3:"1.0";s:7:"ability";s:18:"微信卡券回复";s:11:"description";s:18:"微信卡券回复";s:6:"author";s:13:"WeEngine Team";s:3:"url";s:18:"http://www.we7.cc/";s:8:"settings";s:1:"0";s:10:"subscribes";s:0:"";s:7:"handles";s:0:"";s:12:"isrulefields";s:1:"1";s:8:"issystem";s:1:"1";s:6:"target";s:1:"0";s:6:"iscard";s:1:"0";s:11:"permissions";s:0:"";s:13:"title_initial";s:0:"";s:13:"wxapp_support";s:1:"1";s:11:"app_support";s:1:"2";s:15:"welcome_support";s:1:"0";s:10:"oauth_type";s:1:"0";s:14:"webapp_support";s:1:"1";s:2:"id";N;s:10:"modulename";N;s:9:"isdisplay";i:1;s:4:"logo";s:54:"http://jjd.aodd.cn/addons/wxcard/icon.jpg?v=1528101838";s:11:"main_module";b:0;s:11:"plugin_list";a:0:{}s:11:"is_relation";b:0;}'),
('we7:lastaccount:VfA8V', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('unisetting:2', 'a:0:{}'),
('uniaccount:2', 'a:26:{s:4:"acid";s:1:"2";s:7:"uniacid";s:1:"2";s:5:"token";s:32:"AzerCew1KceerlvKGR0k0RL1e0W2Jkw2";s:14:"encodingaeskey";s:43:"CmaZ2wa245aho4hhhrHzmawHrb6a0H2PB4P6hr4Pp26";s:5:"level";s:1:"1";s:7:"account";s:0:"";s:8:"original";s:15:"gh_c24751cd7e27";s:3:"key";s:18:"wxfb2115622142e037";s:6:"secret";s:32:"37d9773b5a5448361308c382ea57a550";s:4:"name";s:9:"交警队";s:9:"appdomain";s:0:"";s:11:"encrypt_key";s:18:"wxfb2115622142e037";s:4:"type";s:1:"4";s:9:"isconnect";s:1:"0";s:9:"isdeleted";s:1:"0";s:7:"endtime";s:1:"0";s:3:"uid";s:1:"1";s:9:"starttime";s:1:"0";s:6:"groups";a:0:{}s:7:"setting";a:0:{}s:10:"grouplevel";N;s:4:"logo";s:59:"http://jjd.aodd.cn/attachment/headimg_2.jpg?time=1528708761";s:6:"qrcode";s:58:"http://jjd.aodd.cn/attachment/qrcode_2.jpg?time=1528708761";s:9:"switchurl";s:51:"./index.php?c=account&a=display&do=switch&uniacid=2";s:3:"sms";i:0;s:7:"setmeal";a:5:{s:3:"uid";i:-1;s:8:"username";s:9:"创始人";s:9:"timelimit";s:9:"未设置";s:7:"groupid";s:2:"-1";s:9:"groupname";s:12:"所有服务";}}'),
('we7:2:site_store_buy_4', 'a:0:{}'),
('we7:unimodules:2:', 'a:12:{s:5:"basic";a:1:{s:4:"name";s:5:"basic";}s:4:"news";a:1:{s:4:"name";s:4:"news";}s:5:"music";a:1:{s:4:"name";s:5:"music";}s:7:"userapi";a:1:{s:4:"name";s:7:"userapi";}s:8:"recharge";a:1:{s:4:"name";s:8:"recharge";}s:6:"custom";a:1:{s:4:"name";s:6:"custom";}s:6:"images";a:1:{s:4:"name";s:6:"images";}s:5:"video";a:1:{s:4:"name";s:5:"video";}s:5:"voice";a:1:{s:4:"name";s:5:"voice";}s:5:"chats";a:1:{s:4:"name";s:5:"chats";}s:6:"wxcard";a:1:{s:4:"name";s:6:"wxcard";}s:9:"zh_gjhdbm";a:1:{s:4:"name";s:9:"zh_gjhdbm";}}'),
('accesstoken:wxfb2115622142e037', 'a:2:{s:5:"token";N;s:6:"expire";i:1530325562;}'),
('we7:lastaccount:OTq99', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:DhGVd', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:unimodules:2:1', 'a:12:{s:5:"basic";a:1:{s:4:"name";s:5:"basic";}s:4:"news";a:1:{s:4:"name";s:4:"news";}s:5:"music";a:1:{s:4:"name";s:5:"music";}s:7:"userapi";a:1:{s:4:"name";s:7:"userapi";}s:8:"recharge";a:1:{s:4:"name";s:8:"recharge";}s:6:"custom";a:1:{s:4:"name";s:6:"custom";}s:6:"images";a:1:{s:4:"name";s:6:"images";}s:5:"video";a:1:{s:4:"name";s:5:"video";}s:5:"voice";a:1:{s:4:"name";s:5:"voice";}s:5:"chats";a:1:{s:4:"name";s:5:"chats";}s:6:"wxcard";a:1:{s:4:"name";s:6:"wxcard";}s:9:"zh_gjhdbm";a:1:{s:4:"name";s:9:"zh_gjhdbm";}}'),
('we7:lastaccount:d77zb', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:2:site_store_buy_6', 'i:0;'),
('we7:lastaccount:Gl2Ll', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:aq0g7', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:2:site_store_buy_1', 'a:0:{}'),
('we7:2:site_store_buy_5', 'a:0:{}'),
('we7:lastaccount:eM779', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:gP7Qp', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:zUwgq', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:dD661', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:ytTnh', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:N66pm', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:X89wg', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:d4FFE', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:lastaccount:MJkLH', 'a:2:{i:0;s:0:"";s:5:"wxapp";s:1:"2";}'),
('we7:all_cloud_upgrade_module:', 'a:2:{s:6:"expire";i:1528103638;s:4:"data";a:0:{}}'),
('we7:module:all_uninstall', 'a:2:{s:6:"expire";i:1530329357;s:4:"data";a:4:{s:13:"cloud_m_count";N;s:7:"modules";a:2:{s:7:"recycle";a:0:{}s:11:"uninstalled";a:0:{}}s:9:"app_count";i:0;s:11:"wxapp_count";i:0;}}'),
('we7:unimodules::', 'a:12:{s:5:"basic";a:1:{s:4:"name";s:5:"basic";}s:4:"news";a:1:{s:4:"name";s:4:"news";}s:5:"music";a:1:{s:4:"name";s:5:"music";}s:7:"userapi";a:1:{s:4:"name";s:7:"userapi";}s:8:"recharge";a:1:{s:4:"name";s:8:"recharge";}s:6:"custom";a:1:{s:4:"name";s:6:"custom";}s:6:"images";a:1:{s:4:"name";s:6:"images";}s:5:"video";a:1:{s:4:"name";s:5:"video";}s:5:"voice";a:1:{s:4:"name";s:5:"voice";}s:5:"chats";a:1:{s:4:"name";s:5:"chats";}s:6:"wxcard";a:1:{s:4:"name";s:6:"wxcard";}s:9:"zh_gjhdbm";a:1:{s:4:"name";s:9:"zh_gjhdbm";}}'),
('we7:uni_group', 'a:1:{i:1;a:7:{s:2:"id";s:1:"1";s:9:"owner_uid";s:1:"0";s:4:"name";s:18:"体验套餐服务";s:7:"modules";s:2:"N;";s:9:"templates";s:2:"N;";s:7:"uniacid";s:1:"0";s:5:"wxapp";a:0:{}}}'),
('we7:user_modules:1', 'a:12:{i:0;s:9:"zh_gjhdbm";i:1;s:6:"wxcard";i:2;s:5:"chats";i:3;s:5:"voice";i:4;s:5:"video";i:5;s:6:"images";i:6;s:6:"custom";i:7;s:8:"recharge";i:8;s:7:"userapi";i:9;s:5:"music";i:10;s:4:"news";i:11;s:5:"basic";}');

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_cron`
--

CREATE TABLE IF NOT EXISTS `ims_core_cron` (
  `id` int(10) unsigned NOT NULL,
  `cloudid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `filename` varchar(50) NOT NULL,
  `lastruntime` int(10) unsigned NOT NULL,
  `nextruntime` int(10) unsigned NOT NULL,
  `weekday` tinyint(3) NOT NULL,
  `day` tinyint(3) NOT NULL,
  `hour` tinyint(3) NOT NULL,
  `minute` varchar(255) NOT NULL,
  `extra` varchar(5000) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_cron_record`
--

CREATE TABLE IF NOT EXISTS `ims_core_cron_record` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `tid` int(10) unsigned NOT NULL,
  `note` varchar(500) NOT NULL,
  `tag` varchar(5000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_menu`
--

CREATE TABLE IF NOT EXISTS `ims_core_menu` (
  `id` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `title` varchar(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  `append_title` varchar(30) NOT NULL,
  `append_url` varchar(255) NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `type` varchar(15) NOT NULL,
  `is_display` tinyint(3) unsigned NOT NULL,
  `is_system` tinyint(3) unsigned NOT NULL,
  `permission_name` varchar(50) NOT NULL,
  `group_name` varchar(30) NOT NULL,
  `icon` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_core_menu`
--

INSERT INTO `ims_core_menu` (`id`, `pid`, `title`, `name`, `url`, `append_title`, `append_url`, `displayorder`, `type`, `is_display`, `is_system`, `permission_name`, `group_name`, `icon`) VALUES
(1, 0, '', '', '', '', '', 0, '', 1, 0, 'platform_material', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_paylog`
--

CREATE TABLE IF NOT EXISTS `ims_core_paylog` (
  `plid` bigint(11) unsigned NOT NULL,
  `type` varchar(20) NOT NULL,
  `uniacid` int(11) NOT NULL,
  `acid` int(10) NOT NULL,
  `openid` varchar(40) NOT NULL,
  `uniontid` varchar(64) NOT NULL,
  `tid` varchar(128) NOT NULL,
  `fee` decimal(10,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `module` varchar(50) NOT NULL,
  `tag` varchar(2000) NOT NULL,
  `is_usecard` tinyint(3) unsigned NOT NULL,
  `card_type` tinyint(3) unsigned NOT NULL,
  `card_id` varchar(50) NOT NULL,
  `card_fee` decimal(10,2) unsigned NOT NULL,
  `encrypt_code` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_performance`
--

CREATE TABLE IF NOT EXISTS `ims_core_performance` (
  `id` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL,
  `runtime` varchar(10) NOT NULL,
  `runurl` varchar(512) NOT NULL,
  `runsql` varchar(512) NOT NULL,
  `createtime` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_queue`
--

CREATE TABLE IF NOT EXISTS `ims_core_queue` (
  `qid` bigint(20) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `message` varchar(2000) NOT NULL,
  `params` varchar(1000) NOT NULL,
  `keyword` varchar(1000) NOT NULL,
  `response` varchar(2000) NOT NULL,
  `module` varchar(50) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `dateline` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_refundlog`
--

CREATE TABLE IF NOT EXISTS `ims_core_refundlog` (
  `id` int(11) NOT NULL,
  `uniacid` int(11) NOT NULL,
  `refund_uniontid` varchar(64) NOT NULL,
  `reason` varchar(80) NOT NULL,
  `uniontid` varchar(64) NOT NULL,
  `fee` decimal(10,2) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_resource`
--

CREATE TABLE IF NOT EXISTS `ims_core_resource` (
  `mid` int(11) NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `media_id` varchar(100) NOT NULL,
  `trunk` int(10) unsigned NOT NULL,
  `type` varchar(10) NOT NULL,
  `dateline` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_sendsms_log`
--

CREATE TABLE IF NOT EXISTS `ims_core_sendsms_log` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `result` varchar(255) NOT NULL,
  `createtime` int(11) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_sessions`
--

CREATE TABLE IF NOT EXISTS `ims_core_sessions` (
  `sid` char(32) NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `data` varchar(5000) NOT NULL,
  `expiretime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_core_sessions`
--

INSERT INTO `ims_core_sessions` (`sid`, `uniacid`, `openid`, `data`, `expiretime`) VALUES
('fe313f3a8fcb546755c6eabc977d9476', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"z04k";i:1530326939;}', 1530330539),
('3c7586d3befb0c22872b6e9cfeacec75', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QYhL";i:1530326939;}', 1530330539),
('e16d63d014e788baa1a9ee2ad911161d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Gq0q";i:1530326938;}', 1530330538),
('bc693580129d63f72487124c5a197c22', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"d727";i:1530326925;}', 1530330525),
('7ac19b9dbebc2893d6d7b639b9fabd24', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K6v4";i:1530326937;}', 1530330537),
('aae63cbf79635cc772b6ae2c2d2e856c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YzHO";i:1530326925;}', 1530330525),
('ad089d974a8665fa57726fe0c3b111df', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"b4mf";i:1530326925;}', 1530330525),
('6460866bcc7ae79c7610f66f97254d84', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aDc8";i:1530326924;}', 1530330524),
('5ba3936232924dc5527f7542dab1d1de', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CSu8";i:1530326924;}', 1530330524),
('fe2f612c84e3355a58c3fc774902c1ef', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"xNO7";i:1530326924;}', 1530330524),
('7871ff3c6ed167a8d50fb353e4954197', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"t3J3";i:1530326924;}', 1530330524),
('33b9f1add2ff46c7dcdcfec2a82c944c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ne6i";i:1530326924;}', 1530330524),
('ffe3ed4e671aa83c18cbbff6f8f38653', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UXC6";i:1530326924;}', 1530330524),
('6b4c0ea13cb58101eab5c32ac32678cd', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"o1Rj";i:1530326924;}', 1530330524),
('67794aa964b12439b4fd5a6647b9d9f5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"I7ko";i:1530326888;}', 1530330488),
('5d9ca5b9f2cf44a4336cbabbcb8ab62c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HqP3";i:1530326888;}', 1530330488),
('d2e0001435fec05f0a7fc631198c4df1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vKyA";i:1530326887;}', 1530330487),
('f6c838756fa00721f1bd2128aded6b04', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CW6Z";i:1530326887;}', 1530330487),
('f617c274aa2f6b2bb9f2d250e107b0f1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wt6G";i:1530326886;}', 1530330486),
('fa4e94f7bfbebce19e62330c2b13ff3d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YXAa";i:1530326886;}', 1530330486),
('aa973b85dfd4e2e01a889b829a86c1c5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K20w";i:1530326886;}', 1530330486),
('8ca4bd3a0cec825ca1568a50c1ad335a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XCxz";i:1530326886;}', 1530330486),
('4c9526b42a0af6543fa4d738e77d52eb', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"s2Pe";i:1530326886;}', 1530330486),
('d8eefae4b3a263b8546a0c076705f3d7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C2i5";i:1530326886;}', 1530330486),
('c99b4db2761c694f7f7c355c87f2a966', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Yyi7";i:1530326797;}', 1530330397),
('287aea579c69ec8710cdf3cabcfabb21', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oiV8";i:1530326797;}', 1530330397),
('28bd7f50e5bd1f00261a54f8bdee5320', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IG40";i:1530326797;}', 1530330397),
('ae7b3f16b8ec12d61e60b3a0b0ae9990', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WzN4";i:1530326795;}', 1530330395),
('103394ee73852f4875888aade6941cf3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RoAV";i:1530326793;}', 1530330393),
('92e78549a92595cfa240ec86f2adc38f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"M5rz";i:1530326792;}', 1530330392),
('617fd451eba031326f5b80466ad2b05b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qPdd";i:1530326792;}', 1530330392),
('876a316ba3fa1c8afd62b54e35a71ef8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Wllz";i:1530326791;}', 1530330391),
('615b0a6634ac9cb4fcac4b38d5a99522', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WdsX";i:1530326791;}', 1530330391),
('fe7da5647e9d5db1910e56129f50e5e3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"FUCB";i:1530326791;}', 1530330391),
('63fe35ae0e3f8e8d9b58407dd9251d4c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Q6Am";i:1530326791;}', 1530330391),
('db2036213e5fdeaff06f252503980aef', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eFOP";i:1530326791;}', 1530330391),
('7c69a7389c3873def304bce92a93fc49', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"f1l5";i:1530326791;}', 1530330391),
('accfe3778c18f6c103d074b396d33a7e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"V1yg";i:1530326791;}', 1530330391),
('1b1d9f03c0a7d7894517b84ad72327fb', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qthL";i:1530326658;}', 1530330258),
('3c2810f1951ac8b9818e95c05dfcd663', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yA88";i:1530326658;}', 1530330258),
('7fa57ca750b1c516359c951bcf3d88c1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"owLI";i:1530326657;}', 1530330257),
('f5b0a9daa381248adc235bdbc94ed73f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QHhU";i:1530326656;}', 1530330256),
('8e2dc29a78c69d73f79676aac297b43d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"TnR3";i:1530326653;}', 1530330253),
('a12c5d81fa06fb22886bc497dbcf809b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"py6g";i:1530326653;}', 1530330253),
('0f8304506efc63b8419f153386eca96b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"TCmV";i:1530326652;}', 1530330252),
('1e88738cc1ccc6c6907e3e5b126c5b92', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UMEE";i:1530326652;}', 1530330252),
('ee0fdafb455e42bc001daef22fb23ffc', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nueK";i:1530326651;}', 1530330251),
('fae89273ab3d4c5b709acb3ca2d2a2b4', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Kg1K";i:1530326651;}', 1530330251),
('a0a503cb2309101a736c5fab5e14549a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZAU4";i:1530326651;}', 1530330251),
('c0433c1606b7429ec1e08ad64f0aa067', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fcb8";i:1530326651;}', 1530330251),
('d34c1eb90abbe97f99ad57a4af53740e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"GxmG";i:1530326651;}', 1530330251),
('8cffb18fe8514229ef93b267e4bff542', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dSc3";i:1530326651;}', 1530330251),
('7b4632b0e9d0809f7fd5ecf0b6df0eb7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ulw9";i:1530326591;}', 1530330191),
('ed9c5f3e26be1c87ced9039fddb64c15', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"I5EY";i:1530326591;}', 1530330191),
('c63cee1ec451a2e9888436260d7da015', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Tdd3";i:1530326591;}', 1530330191),
('947b4517d597d732793e5f3860a3c469', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uF7f";i:1530326586;}', 1530330186),
('16bd5ec2248d934b3086aebeb34d8967', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Z393";i:1530326584;}', 1530330184),
('ec8f39148872d790a26bd461709f21ee', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"s1rP";i:1530326583;}', 1530330183),
('52f0564d54de2966a990fbd739ba34d5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VXPp";i:1530326583;}', 1530330183),
('f932eb7be230a571b0cfaa61aa62db2b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZSv5";i:1530326582;}', 1530330182),
('58fcea89e65968486bfc9da7a2bf9249', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ARQ9";i:1530326582;}', 1530330182),
('88dd4ca41c764ca338a64b415fbcf951', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"W2NL";i:1530326582;}', 1530330182),
('0b9286c8e47c4d379419f04ebe6f3006', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"bX31";i:1530326582;}', 1530330182),
('40fff89fb36f4e6bef62fe9c1535431c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"La69";i:1530326582;}', 1530330182),
('6a4e150c298768d6f17e516b831662de', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dc6z";i:1530326582;}', 1530330182),
('0fb5b7d7d011d97efcfa88064181401a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NG4M";i:1530326582;}', 1530330182),
('119c2e3f1e61a96dee93e1109a5fea40', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fL88";i:1530326338;}', 1530329938),
('ec453078769ce68bd71ae65a6c806513', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Q77e";i:1530326338;}', 1530329938),
('c2455e0761c85b73fa8885be385a45a8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"FIAP";i:1530326338;}', 1530329938),
('0387fce715a766f7a60ac5cdf0ff798c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hp41";i:1530326336;}', 1530329936),
('76610807d3358363e1a9fb6670c9c179', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Eos8";i:1530326304;}', 1530329904),
('c8a38a24d88b4f09284845bf5283770d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wPWf";i:1530326304;}', 1530329904),
('cb41433f1a897d9f24edc63548c6aa8e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eKA0";i:1530326303;}', 1530329903),
('cf09b29124545f4072c1f3c638b85023', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"q6F0";i:1530326302;}', 1530329902),
('7934ebe92c54cfa269b739b020f5d583', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Q33j";i:1530326302;}', 1530329902),
('2815494438bddf597e57e06992633bc4', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fGlK";i:1530326302;}', 1530329902),
('1864d208ed54ad74935062aa6a1424fe', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kf6I";i:1530326302;}', 1530329902),
('0bd7f195aacb56cd83992b762523c8ab', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"q6u4";i:1530326301;}', 1530329901),
('10c37bfdd882773de506da827528b43f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C4dI";i:1530326301;}', 1530329901),
('c4ec7162c7e1e1db6e2124d74e84fd55', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"j177";i:1530326301;}', 1530329901),
('35cd32f8148c9bff7c05417dc80ea569', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"n55Y";i:1530326221;}', 1530329821),
('2d74dba7557e67283d48853449c6971d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"N2gS";i:1530326221;}', 1530329821),
('7701a84d479dc7695c3b343f32c93d00', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Vrn1";i:1530326220;}', 1530329820),
('6c40cdd9ed175ace233542ee2351f35f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"u6X4";i:1530326219;}', 1530329819),
('b7e8e26e1abdf0ba0115d4612e4e1013', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C7Fh";i:1530326219;}', 1530329819),
('23cf9a01a18bb9aefd955a34893d657e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K2yl";i:1530326219;}', 1530329819),
('bf9e438a579c545cce1e8befe02b4a2d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"s33a";i:1530326219;}', 1530329819),
('7c3254e1b24807c92e08cf95b28589de', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Mvdj";i:1530326218;}', 1530329818),
('dab1ec2059a12e57649a68418744f807', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ErRW";i:1530326218;}', 1530329818),
('277df359a972548efbb5974a2a66ef10', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RtF8";i:1530326218;}', 1530329818),
('8a61f536a9a38a2f0043c16ecb9af4d6', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"z2D1";i:1530325873;}', 1530329473),
('55d0ab64c4bc261352360c2bd19410a1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"n46L";i:1530325873;}', 1530329473),
('58b1586b7c91612878560434ceb2680a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"G2AW";i:1530325873;}', 1530329473),
('1f11e977fd46621c22203498dcacc1cb', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AMKT";i:1530325871;}', 1530329471),
('1d56a6cd112d3d877ff478190ad61c27', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wXZC";i:1530325869;}', 1530329469),
('918705975d44a36e44d61f9259edd6a7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"veSe";i:1530325869;}', 1530329469),
('a8ee8c13d2e50b5de3cb2996f4a9fd9b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"paA9";i:1530325868;}', 1530329468),
('7fe587ef6a6bf8b86615dca6ebfedde0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jKLl";i:1530325867;}', 1530329467),
('376806d5e1804fca9ae633fedec363f0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Mk0h";i:1530325867;}', 1530329467),
('539fa1ce6725c63fbcb63250d8f04b10', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"G0Ll";i:1530325867;}', 1530329467),
('003b82e60f330b1fef9e7a4981b14f4b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tH06";i:1530325867;}', 1530329467),
('ccba4db970ed861e55b3708c575a8232', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zc98";i:1530325867;}', 1530329467),
('99f80815690cf3291dbab26a541ad1dd', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i1qt";i:1530325867;}', 1530329467),
('b59deaa9e9d31e0580b6da086ab962f7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"B5C5";i:1530325867;}', 1530329467),
('fff31dc5a7f60eb819bb5d9c1137e2dd', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"o0Ik";i:1530325850;}', 1530329450),
('c3062c4fe3318618a93b221e84f6f6fa', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ueMZ";i:1530325850;}', 1530329450),
('9d7979955c40c20947b6e870e86251e8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fMzX";i:1530325850;}', 1530329450),
('040ef4744b67cae9b188e9794bf70231', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"d9W4";i:1530325710;}', 1530329310),
('b8f893a571dab5ada6ec753fa72909a5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i298";i:1530325708;}', 1530329308),
('007f2503fa35c01d9a34e2481a5548f9', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wlW6";i:1530325708;}', 1530329308),
('a4e43e2d38c0a984d629e926f5ba4dd7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tQS9";i:1530325707;}', 1530329307),
('483e4f22f7debd6a1fda460bbe57ce86', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EPJj";i:1530325707;}', 1530329307),
('3f3f8c053f64a2f1cf51dbcd8aff649e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"gCjn";i:1530325707;}', 1530329307),
('b0b1c0c99ba1afd1103d29665d353caa', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ru86";i:1530325707;}', 1530329307),
('e4b7984ce51e184ca7f5f18f9178ccd7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"P6MZ";i:1530325707;}', 1530329307),
('ef124e21866d52d3478e7bc425c092af', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"z0w7";i:1530325707;}', 1530329307),
('350d8a5be9cd9766d7186cea7ca9244f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kF5f";i:1530325707;}', 1530329307),
('3611105f3689b3b7e02de222b3e9de47', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tMhE";i:1530325707;}', 1530329307),
('03be824cc3a7021ef1f75b42b2010d4e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"bfQf";i:1530325671;}', 1530329271),
('56c7ac50349730f1d9e987fd96d02e99', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"obC3";i:1530325655;}', 1530329255),
('2762395aa50fc27e6b8d4b0e85d98d27', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tqQe";i:1530325655;}', 1530329255),
('69d0079e4f25b9b72fe9e82383afca85', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QE99";i:1530325654;}', 1530329254),
('2c6eaa656d1f6d03a43e56a207d455c1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Gv26";i:1530325654;}', 1530329254),
('a4eff762877c412e661ed5ea61c39b1e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"bHa9";i:1530325653;}', 1530329253),
('6d2a8f12cc0ca14bf735339b850a8c12', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nz0G";i:1530325653;}', 1530329253),
('c5bef2c53537ecd035bf299a43294f96', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"l7xu";i:1530325653;}', 1530329253),
('6922b0a88f264ea3ac1cc0eaf38cf612', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sEW2";i:1530325653;}', 1530329253),
('7c66b155d4667ff3a8fbcbd0a7159935', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RpO7";i:1530325653;}', 1530329253),
('4f4b9b45b26e041c46025c445420e35a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"cwW4";i:1530325653;}', 1530329253),
('b73bb64e7281951d3546e0b0db38ee9d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"SvfS";i:1530325651;}', 1530329251),
('7a0f4a396be5bf3ea3657d0eeb5205a7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JKJ7";i:1530325650;}', 1530329250),
('69c72d3059fd19c788d7b9a1f2972bfa', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"I1ZV";i:1530325650;}', 1530329250),
('41c88cb96994a9dab559605334952233', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lB62";i:1530325651;}', 1530329251),
('41e08eea4b8fe15463f44936bcdc3b4c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JLrb";i:1530325650;}', 1530329250),
('24e2786da3bd42da01c7bf7ceb3f989f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HmpL";i:1530325650;}', 1530329250),
('2b2f1cebcdca2aac597993ed356bcaf1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"G8Cx";i:1530325650;}', 1530329250),
('43b69dece5a72d67a28a24db91a73d71', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Hmhg";i:1530325650;}', 1530329250),
('caaa32b3bc49d8cd782e8cfe531a7317', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"PWw1";i:1530325649;}', 1530329249),
('d4db9934f65035d245907182276d6a21', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yGa5";i:1530325649;}', 1530329249),
('60bdb1e53927b70878833fd72f60177c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K8F1";i:1530325646;}', 1530329246),
('87bc8daec108c9700d11c73e85f7e3da', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IG53";i:1530325646;}', 1530329246),
('2ffd27811bc2b24ab8c1e71169a5bf53', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yqB9";i:1530325645;}', 1530329245),
('491222f8fe995993ebdb01a705fb63ce', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RDz5";i:1530325645;}', 1530329245),
('a43f1766eb277eeccc2de083e74e2d9f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Pq8B";i:1530325644;}', 1530329244),
('2d57443b5f50f9edf281b2280ca1950a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"n65N";i:1530325644;}', 1530329244),
('b2d62f4b1957c0e956078e7137693465', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"bc1d";i:1530325644;}', 1530329244),
('debabdc85818b957b4188e2661f76ec8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Fw6d";i:1530325644;}', 1530329244),
('a7726efad73a482e41d4a7e55b3b6a66', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Q8JT";i:1530325644;}', 1530329244),
('5e4abf632979963ee351bfb6459c71e6', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sHS1";i:1530325644;}', 1530329244),
('17719dd45137936446e7c3137ffb8579', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"a45B";i:1530325630;}', 1530329230),
('5fd15cf12f6b1207e221a364f451abaf', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aPhj";i:1530325625;}', 1530329225),
('edd92bcd9736d26223c2d90cac250c20', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nMLY";i:1530325625;}', 1530329225),
('6b25985d47ba5ada55dce507c59a8404', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zMt2";i:1530325624;}', 1530329224),
('6538b22505b2beff34d77aaf63c4026a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Q5yc";i:1530325624;}', 1530329224),
('ca7d91affc03d39058d2656b85a8d720', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IJa4";i:1530325623;}', 1530329223),
('59f1dbf8c3ed7bbe1a2b7ad68674e548', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"e8cz";i:1530325623;}', 1530329223),
('38c6b0949d5882aeed931ad3c65f05cc', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AC0V";i:1530325623;}', 1530329223),
('e208da4f11e38b1f087113db57bd9bee', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HlhU";i:1530325623;}', 1530329223),
('008f68f3bc2774a8ca2a298f13dbc395', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"H3gF";i:1530325623;}', 1530329223),
('21b8519ec694ed87182c8572b50b722e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QTJZ";i:1530325623;}', 1530329223),
('39397bc4da09920a1808637ecdc83c8f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F5ZU";i:1530325617;}', 1530329217),
('ba06f16ceb6b91cc9dd50d490190415f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"MIjZ";i:1530325608;}', 1530329208),
('0761563023e2ab4e5ac2a99655f800a2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CL6Q";i:1530325605;}', 1530329205),
('1c5274eb0b9f0934305fa5147352d125', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yWgc";i:1530325605;}', 1530329205),
('0af5a46849d7e4830fad1e23dd481c58', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"cVvb";i:1530325604;}', 1530329204),
('677b731cb26313b31c4a70c228aadbff', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"PXqk";i:1530325603;}', 1530329203),
('35435a3253cb3044126849cb6ea40c27', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"l7UA";i:1530325603;}', 1530329203),
('10242fe4c2b1c1da29f09578460f1dbc', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ewh4";i:1530325603;}', 1530329203),
('52803d282a6078a40843eaa3d84516b1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OT2G";i:1530325603;}', 1530329203),
('340f968f7e3b58bbdeb1e0682030e84e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jeWa";i:1530325602;}', 1530329202),
('fdd29da219fc28792f340c762b31f6e7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"GsSP";i:1530325602;}', 1530329202),
('6e5d0e43f74f6a8066e42fddcfbbf719', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JUOO";i:1530325602;}', 1530329202),
('d9ce6d783757a093fd854acbcffb5b54', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"l37t";i:1530325582;}', 1530329182),
('c1a73911beb6f834d327f516a4f6d41b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"PJ3r";i:1530325111;}', 1530328711),
('952a35d88e75fcacddd1eb3463fd39bc', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"H06H";i:1530325111;}', 1530328711),
('a21ed5236044951fb08c8b542872d6b0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"FGQ5";i:1530325111;}', 1530328711),
('903088815f4208888e7fa737aa29fee6', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lJxG";i:1530325106;}', 1530328706),
('09a9a7d114cbff689457b274590b8a08', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IJFK";i:1530325105;}', 1530328705),
('3a787657880154daf2882746c3c847ed', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"S0Dy";i:1530325105;}', 1530328705),
('1bfa64b7dac04656ea1b40d7a2af14cc', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WYEq";i:1530325104;}', 1530328704),
('bc9fec6b8866b8745495467732e3b011', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"gXlQ";i:1530325104;}', 1530328704),
('f6f0009614b97f0ddc4dfd33bdee4d3d', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YnFa";i:1530325104;}', 1530328704),
('1127224c79a646be955f19375551ba9f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"DR02";i:1530325104;}', 1530328704),
('86186e59689d2e55dbbdfd998d5c1d41', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"T1Ii";i:1530325103;}', 1530328703),
('0915bf8ca592098bd701a31f7128f72c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NX72";i:1530325103;}', 1530328703),
('2917b3145743e4b0eb0a378172e20140', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wJRt";i:1530325103;}', 1530328703),
('04190c570fed4064f259f82a0c5879d8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"T4Tj";i:1530325103;}', 1530328703),
('36ba6387fca28ffe3ad66b3e48598887', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"id3F";i:1530325068;}', 1530328668),
('ba858434c2fc8985f27f62979a4e2a33', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oe2x";i:1530325068;}', 1530328668),
('f35699d29134a3a5092eb60a78085c7b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zEt7";i:1530325068;}', 1530328668),
('835a89348069356bd193f4922f50d39f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ELEF";i:1530325016;}', 1530328616),
('8260589e6a9031c24886fcfefc5fb6b3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zYY2";i:1530325016;}', 1530328616),
('d1a6c83277af98ab18cd69ce663e96b0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ONqI";i:1530325016;}', 1530328616),
('bee71c24d0cd5621c23d51271be31cea', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OtiD";i:1530325015;}', 1530328615),
('18264c9b8f318ac15f61f0234485053d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"X997";i:1530325013;}', 1530328613),
('2a7a0f57e5643a6c1aeaac04860f6373', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NxEi";i:1530325013;}', 1530328613),
('770aabbeecae6adfbb1c697e3186244f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"cf3J";i:1530325012;}', 1530328612),
('0d2d20ecb9bd81efd0869e212fea0aec', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JU9a";i:1530325012;}', 1530328612),
('8715d00acae0eb7cf325347b195311f6', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"SUw6";i:1530325012;}', 1530328612),
('c967d91a23b8eb31e49f738fd1bfc520', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ouk1";i:1530325011;}', 1530328611),
('976678c1b197f66121824ab7d96423fc', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qg8t";i:1530325011;}', 1530328611),
('e9a24ee998f9479e8b81a98d062f5025', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"l9fH";i:1530325011;}', 1530328611),
('61f7e54632fa2310cd0bb69b410f42bf', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"k2Ov";i:1530325011;}', 1530328611),
('2537cf05ad531f6df7bd2f74818ad40e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Cn4n";i:1530325011;}', 1530328611),
('542594cbf394ce7247623ae901fd0df0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"SbKF";i:1530324876;}', 1530328476),
('9db73bc516ab353e063318480fc975d3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YPfU";i:1530324876;}', 1530328476),
('62a1092fe5ea2b6f4af57d9fa9100e35', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rjx6";i:1530324876;}', 1530328476),
('9cf53594ec1576d7f0d871e6ad03263a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HPhb";i:1530324875;}', 1530328475),
('baae421f11fa95603f7bc6f2409bff85', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Xgjx";i:1530324875;}', 1530328475),
('32cbd305294446ff178ebe9cdc698651', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C8t6";i:1530324875;}', 1530328475),
('2e3d1bfb1956777bb298dd0c134eeeda', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vD54";i:1530324875;}', 1530328475),
('e6fecddfb178a0f215a67fc8d77e595a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i886";i:1530324875;}', 1530328475),
('29f09eab56a37e60ae086e27a7c01786', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"DHb7";i:1530324874;}', 1530328474),
('fe35a45c1e96435df185dd11fdaa6394', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"y4t7";i:1530324874;}', 1530328474),
('a1d895d84ad7454fd2382f839b583dbf', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZoeX";i:1530324292;}', 1530327892),
('fce562e856ed0b270e2bbc112ed322fd', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"MXIS";i:1530324292;}', 1530327892),
('a984442a46540a7df77ee4647b6b2719', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"u6ho";i:1530324291;}', 1530327891),
('38590f7508e87002f5e4f8ba47d07739', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vMZI";i:1530324157;}', 1530327757),
('e00917418df029eae9a02a0f3617d007', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"z7p4";i:1530324157;}', 1530327757),
('989f4ae4a4521684903333ac5d3e18f5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K90e";i:1530324157;}', 1530327757),
('37c8cb0f422b6d9df42a09b79305e9fe', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"H86V";i:1530324089;}', 1530327689),
('e2b8d8a353b2d16070622f775b5e6e51', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"v3Nn";i:1530324089;}', 1530327689),
('7439b6d1a3ac6f036898ad6782c83c72', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"xD8p";i:1530324089;}', 1530327689),
('af3852d581f8136cc378010975049635', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"iyL7";i:1530324084;}', 1530327684),
('eec5ced1e8d8db058148c73a7b3a659e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"a1Mv";i:1530324084;}', 1530327684),
('b520edacfd8516f0ecb970935161989a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aokk";i:1530324085;}', 1530327685),
('3dfa4549e19ea6cf8b1f7dc8554f1abb', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"gp1k";i:1530324083;}', 1530327683),
('1fe6db63e55396dd4a626fea0d4d3c0f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OPjk";i:1530324083;}', 1530327683),
('5cb1a42fcab79c8359e1b3910b1fcc52', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"a8ZE";i:1530324082;}', 1530327682),
('d859bacc7e42dff5865dd84b639cf982', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dc22";i:1530324082;}', 1530327682),
('306a1ca30aabc2093fc77934f4300243', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aqmq";i:1530324082;}', 1530327682),
('4c254e4745543fe0081b63d3b48b1132', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IDaa";i:1530324082;}', 1530327682),
('068a5c2236515dc66fa74c5d4e938362', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CL3Z";i:1530324064;}', 1530327664),
('d4512f3da95b0a88c378f2d558dc1d44', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zap3";i:1530324082;}', 1530327682),
('462fa377cc0096a9eb32a385934d9f19', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tXf2";i:1530324082;}', 1530327682),
('6e221e627686548202ae06311cf738f1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"o7Ij";i:1530324064;}', 1530327664),
('09eb71fa477c21f86d5c500d7eb501cb', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"h0M0";i:1530324062;}', 1530327662),
('f16cb4120d6f5fa8e215b282c33cf916', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nCjJ";i:1530324063;}', 1530327663),
('7b4cf866cebe6def9e863fdc5ec4a3fe', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"D1Ef";i:1530324062;}', 1530327662),
('815e8af6e307a245bdd2036453a81362', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"n18N";i:1530324062;}', 1530327662),
('cba4083db8fa3909c18085fdb5570d99', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zaQQ";i:1530324062;}', 1530327662),
('d17517a36e5da62986090f2c845c6fa0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lZNg";i:1530324062;}', 1530327662),
('c7268c6c43b0c9b3d56fccd6ddf94238', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NoDm";i:1530324062;}', 1530327662),
('e0faa69512b2d6256b15db36f4be920f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dXrd";i:1530324062;}', 1530327662),
('02e87ca43f34b95da21a62e4ee6a45f7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"V5pp";i:1530323924;}', 1530327524),
('712575b7791712825613295701d00725', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Dwfq";i:1530323925;}', 1530327525),
('13085353ec5e14061d8fb68cada3efd9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JEUJ";i:1530323925;}', 1530327525),
('83cf3d9158422c76bc10208a962e1a63', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OANV";i:1530323923;}', 1530327523),
('2a7661cd55f39ca5ee310338fbf98673', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nkkK";i:1530323923;}', 1530327523),
('ee07f246fc0d8543517a34a10156b2e5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"iQOO";i:1530323922;}', 1530327522),
('1d085fb88f6a42896b9ccb875909087f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Wp6h";i:1530323922;}', 1530327522),
('b2b2a73c6f83c38414da94a3e6b6dc4e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"M85e";i:1530323922;}', 1530327522),
('1a2edbb08be81e71127b05eab8ec7a53', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vm05";i:1530323922;}', 1530327522),
('61ed45cb687983f27eb8675f4b5d82b5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"xg63";i:1530323859;}', 1530327459),
('7e368e86a41606de614383535240afe7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"thN4";i:1530323922;}', 1530327522),
('b981242b7e2ba79fb52fdac94691e09e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WLIL";i:1530323859;}', 1530327459),
('e6f3bc854a31c59c5ab57c0b03c9c6ce', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uNjA";i:1530323856;}', 1530327456),
('cd9b8c4bfe919053b86db11af7980295', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"xfJN";i:1530323853;}', 1530327453),
('05dfe3a0ef8117a8e34076b3bfa2ecad', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"g37S";i:1530323852;}', 1530327452),
('12de64cd19b24f8e8f9f9685f05496d9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ymt5";i:1530323853;}', 1530327453),
('2b6ce256575ab16b6b2c4a7a595d507b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ISI0";i:1530323851;}', 1530327451),
('43b6bf36ca4afb9f1cd9a930e389cbe0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"e5p4";i:1530323851;}', 1530327451),
('bff909c09c37bee5b5eb9800b287ace7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"G1Kq";i:1530323851;}', 1530327451),
('225f36a4e95d70e112f656490587c92a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Vf46";i:1530323851;}', 1530327451),
('b8ed6b5bfe428c7a1b6d850789cce5b1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"FCQ8";i:1530323851;}', 1530327451),
('1e9016f8859b4c7a2bac8c88378af0c7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"h262";i:1530323851;}', 1530327451),
('a7562aa752d5373fc7d3fca08b4f2623', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"BhST";i:1530323851;}', 1530327451),
('b2c1f5913f3ba5dd60fb6b3e57075af2', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UJfT";i:1530323762;}', 1530327362),
('418b38cca1df1e6a80474a240f70f343', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IAk2";i:1530323762;}', 1530327362),
('13ebd24a6a1c79cf9439dc1a34e3681a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"h3si";i:1530323760;}', 1530327360),
('e020f5f1dd37d41ac2548b77ea1f8a1b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"USm8";i:1530323748;}', 1530327348),
('70cf1a67d14cfe407b837ed2bf63bd5c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Vg5m";i:1530323746;}', 1530327346),
('f86a1216177010260c39ced11a96fb70', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sgYh";i:1530323747;}', 1530327347),
('05a193366bded93d87a8a79cbff0827a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QQiS";i:1530323747;}', 1530327347),
('67e84849313f7951aca9f0f9a061e5a5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yg6M";i:1530323745;}', 1530327345),
('4d38846ff6070b673c740b2b457af6af', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F69g";i:1530323745;}', 1530327345),
('9858151edd6a4721fc795e589365ac63', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Qqr0";i:1530323745;}', 1530327345),
('6a7bc3a860b4f9857bf577065eedb720', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qxuO";i:1530323745;}', 1530327345),
('fedaa25eec529afb178b872e4d49c49e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qBz0";i:1530323745;}', 1530327345),
('9522a3f0a99fcf550c1eaf53bb8fa15e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"H44e";i:1530323745;}', 1530327345),
('f089e6a50d4405903f6a2e76c8550bc0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tZLu";i:1530323738;}', 1530327338),
('89fbd2b4ebb249ed2f67b273d9e906e2', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mXlf";i:1530323738;}', 1530327338),
('634249bacde02694c6af29e481567b9e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"b3g7";i:1530323737;}', 1530327337),
('275fffe582a81cdb9edb360b7338197b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zfbS";i:1530323736;}', 1530327336),
('5ad09039f4e7fe35a5edf686bfcb77c8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Hv0O";i:1530323736;}', 1530327336),
('e5fa189d6105200a8ba76ba3cce7b656', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oJKV";i:1530323736;}', 1530327336),
('cc93e545a4dee03fc6262a4b4dece749', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hCkM";i:1530323736;}', 1530327336),
('661165fb441cc0589c34fd7e56248eae', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Pnp5";i:1530323734;}', 1530327334),
('3a0c902391cf44e5683babf29e0b2c8e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"pg18";i:1530323735;}', 1530327335),
('2d3f20d038c59077d882724f33d6dd62', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"a1l4";i:1530323735;}', 1530327335),
('eda1cce3573da68f35888f617dd98373', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rqZq";i:1530323736;}', 1530327336),
('d55dae7c2bb9b4b8c146d5341d372e02', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mXMG";i:1530323734;}', 1530327334),
('ddf25351d0b78baa571b14873545eebe', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AT6e";i:1530323734;}', 1530327334),
('d80e7a50cad820bfe2fa9741abf896ac', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IK2E";i:1530323734;}', 1530327334),
('03607ded0f07efb7faaea1ec6f0b59ff', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"t5Jf";i:1530323734;}', 1530327334),
('0180e1cb2c17e85c4456e53843dd659a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sCNn";i:1530323734;}', 1530327334),
('a33413ad5d000dd79e3db968b92c6599', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K3WN";i:1530323726;}', 1530327326),
('c3b50c8d3de6176d440226ea58228913', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ACMR";i:1530323712;}', 1530327312),
('08f54d58e56829ba229b8c43b972646e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rHee";i:1530323712;}', 1530327312),
('13d06b8b7a7ff78b868beacc34064427', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZAxa";i:1530323710;}', 1530327310),
('056b5e1782b1a2efa825e81b32011949', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"I8hK";i:1530323710;}', 1530327310),
('eb05bddcd3f4f86bc77a73a92ceb8f1d', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Plir";i:1530323711;}', 1530327311),
('5bcc290dfeb691b233182ee7332055b0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"R2J2";i:1530323710;}', 1530327310),
('ea2b503a6f6a38821bbaa3c3e1444fc9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EFSO";i:1530323710;}', 1530327310),
('4432f712e16c1da2f939a800e1ffae29', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"iiGM";i:1530323710;}', 1530327310),
('bfa86240474e3b7cb874b1924115b4aa', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Q1P3";i:1530323710;}', 1530327310),
('582f612cbb98ea19b1fb3e097aa57c3e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Wq95";i:1530323710;}', 1530327310),
('7387c217418c7542699fac5d52b20822', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"FbsH";i:1530323693;}', 1530327293),
('264e07cc6eb6ad7b301c57d230e58c6c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"W3jn";i:1530323693;}', 1530327293),
('68cdcdaf949dcca0328f798838dd9d82', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AZLj";i:1530323689;}', 1530327289),
('10788eda5ec8f92236ec7a14f6c85893', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EGCg";i:1530323691;}', 1530327291),
('94e54da8ce4986bbe22b71e40dcb1963', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Gki1";i:1530323689;}', 1530327289),
('0c0c708c224e1445ae0925fd749f0aa7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"y598";i:1530323688;}', 1530327288),
('484dc937ad4432de30d39452dcf2ba27', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vxx7";i:1530323687;}', 1530327287),
('f54eb7934ac706b0f3d3a060e847807c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kYq0";i:1530323688;}', 1530327288),
('33dc1374b8cde41134aca6410be79aa9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Jfm8";i:1530323687;}', 1530327287),
('f07fb8b6b26a77288d181329623e82b4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C6PK";i:1530323687;}', 1530327287),
('098228ca48f0eb521f3ccc7dd6b6383e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"BwDG";i:1530323687;}', 1530327287),
('186d5d1c321466f9b729e74bddc4fcd4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JZRx";i:1530323687;}', 1530327287),
('74c7e5462abc207cc1517ff2d9191e7f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"o3jP";i:1530323687;}', 1530327287),
('4f7a3f950beb537686edd616720d8fe1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"j5S2";i:1530323633;}', 1530327233),
('e9f7f2ae6f270e171c799147fff5fece', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LKHZ";i:1530323622;}', 1530327222),
('e2e187c5c8ae0062efe0daced4fda364', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mZLs";i:1530323632;}', 1530327232),
('496f92bffa847157274a6a2df1c11e38', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Fr9p";i:1530323620;}', 1530327220),
('628442af843f7a7c240772aaaf648fba', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wurQ";i:1530323620;}', 1530327220),
('a8ce4dbbbe41979b11fd053eb880f949', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qmg6";i:1530323619;}', 1530327219),
('f017300b61439e167331a99818b351dc', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i7O9";i:1530323619;}', 1530327219),
('18387da23c7fc905e250863f2ac3e538', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hyJZ";i:1530323618;}', 1530327218),
('cc4c4290cfa7fcf65c2219e0cc4a554f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EE8D";i:1530323618;}', 1530327218),
('f42c4dd786f285873c5bddc02db50c0d', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"amBZ";i:1530323618;}', 1530327218),
('86fa1120e918294bb8a7769fa3a933c0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"PzA9";i:1530323618;}', 1530327218),
('b7c5e33a931974ba0cbe290c30e60844', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RHN2";i:1530323618;}', 1530327218),
('c48f4d40f72e9300b4cd54d4cb194162', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JyIA";i:1530323618;}', 1530327218),
('668f2ff42117515f6ba4ebd08893bdbf', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Zr49";i:1530323471;}', 1530327071),
('49ad0d03ffe2071a5cbfb94baee46cc0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"znb5";i:1530323470;}', 1530327070),
('a07a7bff94c333e4d8a7417101cd6e2f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IV1g";i:1530323458;}', 1530327058),
('4dad7e2be7a00f591a8fa4208de37628', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F8N3";i:1530323456;}', 1530327056),
('9987de5052b797dada7f4e61c234ed3b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"FGir";i:1530323456;}', 1530327056),
('722c9328bd8a9a487315062c4201f05a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tv2J";i:1530323455;}', 1530327055),
('4c982573635fd43da13332603b12b9c1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nTt3";i:1530323455;}', 1530327055),
('ec974d0b6900e0b00a9efc621e4f6d59', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wN3M";i:1530323454;}', 1530327054),
('daad6ea483f0b4e1d6c7124dd180fd35', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eDjq";i:1530323454;}', 1530327054),
('df08ee365db7e4834f71e904c0a95e6d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"pPeq";i:1530323454;}', 1530327054),
('02ad2f266d63dd4d4cc63f5f15bd5aef', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"p1qa";i:1530323454;}', 1530327054),
('e6186c8db371c197528a58ffac315eb0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"P1YH";i:1530323454;}', 1530327054),
('e890704ac3df8c4787c5108760011b7a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aLZK";i:1530323406;}', 1530327006),
('ab29d0ce69c6c4234de3614f60fcd00d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qFGI";i:1530323454;}', 1530327054),
('cbc6257215d4e833224526b5317587b9', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XldI";i:1530323406;}', 1530327006),
('7c33aa962935d170d370a61662caa169', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wF5q";i:1530323404;}', 1530327004),
('605bc8c80b2ce42be6b97b9a9d7cd87b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OH4Z";i:1530323402;}', 1530327002),
('d3babbaf3f1302912f01edef22c6a584', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lSUQ";i:1530323402;}', 1530327002),
('062433cbd2d30398cae92c53c768585e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vka9";i:1530323401;}', 1530327001),
('b55c2538886ae5437c5fb65d036a5973', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UiI8";i:1530323401;}', 1530327001),
('42adb9ac662b727b2fc3d591ebdbc26e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Frnz";i:1530323401;}', 1530327001),
('5ad6aa87fd36c45d1a48cb5bf216570f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QPkx";i:1530323401;}', 1530327001),
('69a432e5684c07f1905384a12a2c3870', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nhK8";i:1530323401;}', 1530327001),
('6723aea48e62798f1664c0a5ee6c1691', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KZff";i:1530323401;}', 1530327001),
('d964204f3b71252e11481fd5425f930d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"c2gt";i:1530323400;}', 1530327000),
('9cc641198ebad8a5c54b3e1489e6e461', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lDdl";i:1530323400;}', 1530327000),
('a39dd8c77cb30cbfb535925f7f4575cb', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"P4cR";i:1530323398;}', 1530326998),
('5e3db5f265ed6a41746fcdb4550a2983', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Qt0P";i:1530323398;}', 1530326998),
('acac19393ab3fb53104cd98410868ac5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XDI2";i:1530323398;}', 1530326998),
('a1ec756fa442246c33d3bd01ee44875c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uFKF";i:1530323398;}', 1530326998),
('d704bf229de91b67d6b6c20d81471650', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rnwJ";i:1530323398;}', 1530326998),
('6b4688deab6e0fc223413109b4ab1375', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"h9RS";i:1530323398;}', 1530326998),
('0b6a0034eb8999621d5a44345ffc49a4', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kPbq";i:1530323398;}', 1530326998),
('2fd589f81b65929dace4abcf00903fa4', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"k8Xi";i:1530323398;}', 1530326998),
('f0ac4abc11f8b3514c0722e0f0f2aec6', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jGG1";i:1530323383;}', 1530326983),
('ae1744660cbb4f432cefb809f92a214b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uMi0";i:1530323382;}', 1530326982),
('768490eac29971935696daf41aeefb46', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AS7b";i:1530323382;}', 1530326982),
('e77ebb4be065551661611c2163830386', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"auSo";i:1530323381;}', 1530326981),
('0aa31a68290168a0e6fae12a460d7167', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HID9";i:1530323381;}', 1530326981),
('b9eaeb7523a8e183a9e9aa2f7236059a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"n9I3";i:1530323381;}', 1530326981),
('65f77417fdffda0141060097386866da', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RCQ9";i:1530323381;}', 1530326981),
('5a5d1eea724b666b3644cc52ec018a64', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VoC9";i:1530323381;}', 1530326981),
('7025465ac47447aed2c0ad1e91b1bd5b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YbbD";i:1530323380;}', 1530326980),
('95172c2553eecf30dd186a87c3ba6419', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nTBK";i:1530323351;}', 1530326951),
('0975a4e7f3f278f6752fac1f4c77586d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eK97";i:1530323380;}', 1530326980),
('459c6d02a85a70168930dbef42a6c591', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EUxu";i:1530323380;}', 1530326980);
INSERT INTO `ims_core_sessions` (`sid`, `uniacid`, `openid`, `data`, `expiretime`) VALUES
('bc5b38f0a1f4109628ee4ddfc50b61ab', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Sxw6";i:1530323349;}', 1530326949),
('bbe3b08da93847a71e3de93779996e17', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UNlt";i:1530323349;}', 1530326949),
('cd5c10e346ae2c68e39cf67b4c8f663c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"p47V";i:1530323349;}', 1530326949),
('0fa3de26c6ad97cccc45084a7b06e0f6', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fJ7e";i:1530323348;}', 1530326948),
('285fbbd2134ca05084e4d7ef2a341aac', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jxA9";i:1530323348;}', 1530326948),
('118c5af63de3b16ab8c3cfe8c7d4b5b0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Y1mr";i:1530323348;}', 1530326948),
('37d08352e483e2a6b3ec43e3a0331969', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jQuZ";i:1530323348;}', 1530326948),
('cb0b7a542f79925041fa04da193fd5ba', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"T0TV";i:1530323348;}', 1530326948),
('25706958b174d1e46a0d5d68763fd926', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tJle";i:1530323348;}', 1530326948),
('c6853b66a2f8dc4b75d6532512dbd994', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RyGZ";i:1530323348;}', 1530326948),
('3fb60e5406d3f31aa9209b0e59b7bbb2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F1R5";i:1530323303;}', 1530326903),
('19c70703201316e8e28bf2bbb357dcbd', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Be2i";i:1530323303;}', 1530326903),
('b5c3588c21b3d5ad0e7791deec0e1c45', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C7Ck";i:1530323302;}', 1530326902),
('5a791cc14857b43e33b3c21a3197eebf', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"h04L";i:1530323302;}', 1530326902),
('0df712fae02fd0c489678ef5ad79ab0c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YXNo";i:1530323301;}', 1530326901),
('724ee9a5629c9591d980c0360bddd558', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"c3j3";i:1530323301;}', 1530326901),
('d37bc8cab951f064cf1d32a804038ef6', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"c3Oo";i:1530323301;}', 1530326901),
('9835fff705c7919c47efc58acfac893a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ooa1";i:1530323301;}', 1530326901),
('9823c92ff253a77159e9fd7fab1f6275', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"M36L";i:1530323301;}', 1530326901),
('1eff66968e85fd43854eee41e587b2cb', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"xJlJ";i:1530323301;}', 1530326901),
('1e6e9cc6b62cb3950892578f9efa150d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"TJPN";i:1530323284;}', 1530326884),
('539029ff66aec1c733c3225fe72e1f36', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KBr6";i:1530323284;}', 1530326884),
('4e2a0492a88a98ebac17b252bd2e7fe3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"B4fI";i:1530323283;}', 1530326883),
('29b0a6b69d656ba8ff3b6885498cd403', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Lw4V";i:1530323282;}', 1530326882),
('437a7557223190fc6db894ded866c883', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"GuCk";i:1530323282;}', 1530326882),
('2b5b855ee1a1b4adbfa82a3a7b9fe181', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ErO7";i:1530323282;}', 1530326882),
('bfd5e6c1451dbc92214e7e337d088e80', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lcpB";i:1530323282;}', 1530326882),
('ef2bdda7eda0acfffe2469a7bd4d7f07', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"bzHM";i:1530323281;}', 1530326881),
('ad5ba6bbe83570d3dcb3f2dbdc38304e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NqNJ";i:1530323281;}', 1530326881),
('a668d99b0e4f2e0715d2c7e4715e7448', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hVBm";i:1530323281;}', 1530326881),
('7eab9b20b184feb1d53d742b93a3585c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"btGB";i:1530323255;}', 1530326855),
('ba72cff3e2c30d88af993d05a901596a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fPxt";i:1530323222;}', 1530326822),
('76328ba94efabe3e921e9baca945354e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XmHV";i:1530323222;}', 1530326822),
('977ea5951d447d4029c3f67de8925fb2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"f0Sn";i:1530323221;}', 1530326821),
('3c398f4b95a56430e3849f103c199c45', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EWVw";i:1530323218;}', 1530326818),
('14d33828827f158fa968fcb6ebf25485', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mC28";i:1530323218;}', 1530326818),
('59232e4182f96323451a77f2e0b8fbb0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IL0L";i:1530323218;}', 1530326818),
('1c4d9167e22cdf90b4acf62733e9ac86', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JY10";i:1530323217;}', 1530326817),
('55603bd685b4b300ee2060f6187a2f81', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"h5GQ";i:1530323217;}', 1530326817),
('7cba76fbba0a171f37a9d906447b91e0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nkoC";i:1530323217;}', 1530326817),
('34ec7dce12ba1ed153dff728ab49978f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"q7ih";i:1530323217;}', 1530326817),
('3eac5e4b5e073cb99d01f23baa0c0cb8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CgMT";i:1530323193;}', 1530326793),
('9b5bff33e3c94901ea543da198500b25', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i25X";i:1530323192;}', 1530326792),
('be0effd40be476124658772f7b5b4a44', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Pt47";i:1530323191;}', 1530326791),
('684af97c64ce3450b6bc6afa1055b8b7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"TVUm";i:1530323187;}', 1530326787),
('870f40975cc9ea11682fdf981d898ff9', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Urrb";i:1530323186;}', 1530326786),
('aa1c3798f3a0ea2b5733db219ca0c111', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"z5kT";i:1530323186;}', 1530326786),
('400b3d766d684d9dab255ffb6299750c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Z3xM";i:1530323186;}', 1530326786),
('361318c7c60e1ddcc5c2eb466ef47f29', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Dl9G";i:1530323186;}', 1530326786),
('1f9e5a3260c80f3f187a627baeaa61ea', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"l3p3";i:1530323186;}', 1530326786),
('022efdff11473ef69b3013cbb7ad79f0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"p8br";i:1530323186;}', 1530326786),
('e4469b6d1aa73107378bddfdc9d9a600', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Q5Jc";i:1530323123;}', 1530326723),
('63158728b545c3c9beb3979649ab3d52', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nRMJ";i:1530323122;}', 1530326722),
('804cbae65134d7a4b45f4d75acf56f0b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"B6gq";i:1530323122;}', 1530326722),
('438cd973f246a6b0d670ba919ec22577', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"x001";i:1530323121;}', 1530326721),
('09c3a612d33c81683904f754ebeb9b42', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"t1Qn";i:1530323121;}', 1530326721),
('2f89092b53a9da133bbc3bc8078b4092', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"p26m";i:1530323120;}', 1530326720),
('440fa8693c623b63a6613a6982b39f3d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ycz2";i:1530323120;}', 1530326720),
('daab573f32b10833d030daf323c57f09', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lm97";i:1530323120;}', 1530326720),
('bad4bee4b3d44642a06127781a5a7528', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"exqC";i:1530323120;}', 1530326720),
('51fd8d955cda1877f5293ba4e67bfee1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ed11";i:1530323120;}', 1530326720),
('0b6a15baceaf80046f6ddeba1c5cdaf8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RcIO";i:1530323120;}', 1530326720),
('b3222602dfa78768c6949ca78f07057e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ka07";i:1530323090;}', 1530326690),
('bfa92ab5f8b61bf1dadd88a33235062b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"scUu";i:1530323086;}', 1530326686),
('bb7b7e4077d15dc6d50edb6f5c283073', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"R8YY";i:1530323083;}', 1530326683),
('09bfa87d42348c2165d920c142161935', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"m37Z";i:1530323080;}', 1530326680),
('ba68797c2905d8869fc14f23202c37b5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UPPE";i:1530323080;}', 1530326680),
('d6be004bd1e318b6b3e532a3f61106ad', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Lkv8";i:1530323037;}', 1530326637),
('00e6e6105a120616258441c86d73a761', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nT14";i:1530323036;}', 1530326636),
('f1857ae77f86272e027335dcdcc22da3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kgXi";i:1530323025;}', 1530326625),
('1789dcdadf00d42aff3e4bea9b2d91e5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ygZl";i:1530323023;}', 1530326623),
('84c296a54f0e43c6f6a47df027562b7f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"iDfm";i:1530323023;}', 1530326623),
('c71a4297c52b28e472d5d1bce3a57494', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"y99n";i:1530323022;}', 1530326622),
('944fd9fa6450346656a5199ec4e7664b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yJNb";i:1530323022;}', 1530326622),
('22df45f0a761a89a708899a569ee0281', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VrNo";i:1530323021;}', 1530326621),
('553753b4b97a826438893b9fee3da31e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KzhI";i:1530323021;}', 1530326621),
('fc16aac5ec44586b0c980255dd12fbfc', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vvpK";i:1530323021;}', 1530326621),
('bc91c4c27df3a3677f0d39f46494bc37', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lSz8";i:1530323021;}', 1530326621),
('4ea0163d19a8fb13c6f8e8c4f68ff859', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Bv1N";i:1530323021;}', 1530326621),
('98f9c19144fadbd8f148a03e5fc2a513', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tzsk";i:1530323021;}', 1530326621),
('7d4bdc194f413b654f70601d1d23b5bf', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KPUB";i:1530323001;}', 1530326601),
('e960bb8289fbc6093a9e3f673a59d374', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"J49i";i:1530323001;}', 1530326601),
('383fee969c307a8b1a186871547ba95e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WexE";i:1530323000;}', 1530326600),
('0e2fb69962c21f538139ca32e41add16', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"P3zD";i:1530323000;}', 1530326600),
('c19f4b2a22d2318751f1a57214662d61', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"S4UA";i:1530323000;}', 1530326600),
('e8a968e3785c4c4468e8eb22c6dac9bc', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OK8k";i:1530323000;}', 1530326600),
('810bdf5aec5e17cd2c90e42559cdaa7c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K1e5";i:1530323000;}', 1530326600),
('d7a0a26dee927eebd44ba05942a29d47', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nfm7";i:1530323000;}', 1530326600),
('7d5ffa49ebbc3aa9c0c2fcbcf49e7a4d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ibdg";i:1530322999;}', 1530326599),
('b145cf7e0ffddc5d1dcbe81423325457', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"t5f5";i:1530322999;}', 1530326599),
('1b1a03f8686ba3397d013f8ebbacc73f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lg26";i:1530322934;}', 1530326534),
('161caab7d237d2d845310444fe3e4ec6', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aBj1";i:1530322932;}', 1530326532),
('6dfb09bcaae992bfd6b054920a78f37d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"X7Ji";i:1530322932;}', 1530326532),
('5d1e6238bc69ac8c917cc08b41040d94', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"y3IQ";i:1530322932;}', 1530326532),
('afd257b81d59eaf7b680a66dd4c113e7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LDd9";i:1530322932;}', 1530326532),
('73ad280334b4b4d13b2e1af7395bd849', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WgLr";i:1530322931;}', 1530326531),
('c53b0984a3ede5eed694dc24aa430950', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"d7zX";i:1530322931;}', 1530326531),
('40bdc768cb620244342e80ddf49121ea', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fFQp";i:1530322931;}', 1530326531),
('ed07aa25817c5e944502ae7378996068', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"MXwb";i:1530322931;}', 1530326531),
('0fcb5526d5ae95bb931e7fa9efd5cae4', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oWq8";i:1530322931;}', 1530326531),
('0b37c0e0cdc33958853921f912013c05', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JZI2";i:1530322931;}', 1530326531),
('7f846e68fc792f50d13468ec3835bd54', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ycs9";i:1530322853;}', 1530326453),
('8eb4e1699a85866c7e9df2138f1376bd', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"BY5i";i:1530322852;}', 1530326452),
('0b765eb533926d70aba23842134416e6', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Cbc1";i:1530322846;}', 1530326446),
('81175bcd25d6f3cd05556679367423a7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Vzk0";i:1530322844;}', 1530326444),
('b314c6b03ca7b423578676a4a324e4df', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"f3Nu";i:1530322844;}', 1530326444),
('b1c64ac41465bc75a1b160ac197d15c4', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"iKtM";i:1530322843;}', 1530326443),
('178e0cfe94e82c2bcc06dfe41354e66a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wwPq";i:1530322843;}', 1530326443),
('0e5b07b52faa45823b1d66df02eb5023', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"O233";i:1530322843;}', 1530326443),
('85897cd2520f8499fb4b8a52a5d2f030', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"MbPm";i:1530322843;}', 1530326443),
('1b5a751facc2a5177e2829fe09e47baa', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"P0Bk";i:1530322843;}', 1530326443),
('22d931ed2e08354e4c76d1851aed489f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"H9iW";i:1530322843;}', 1530326443),
('cf297037f9172ab16b13df8fea9aea10', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"E1w9";i:1530322843;}', 1530326443),
('875ebc5708bf48c414079ff871b8b2f2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"a8zW";i:1530322843;}', 1530326443),
('e49a466328e60a290ead0c45d42b3a39', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CdDL";i:1530322634;}', 1530326234),
('671bc4ead62e3cb3ac30c2dbda929b1c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rv6I";i:1530322626;}', 1530326226),
('ffa591755a711e6612dee049cf5b65ad', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wtZT";i:1530322580;}', 1530326180),
('472bd25b6defc63f27e55a0788966e9d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UC08";i:1530322579;}', 1530326179),
('488f0ced338f3ab165b25907a3d48a35', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F6Vl";i:1530322577;}', 1530326177),
('83bf7ae41eccfcc98c732da5b07fbf7f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"p1Fb";i:1530322573;}', 1530326173),
('35e34d04ab5e6e3f2f88b43a0880a07d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"xaoo";i:1530322573;}', 1530326173),
('1b904473cfeccd872f969ab3f49cbac5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"DT96";i:1530322572;}', 1530326172),
('3292d02fccadae2a87b8996c38246714', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tkm9";i:1530322571;}', 1530326171),
('8035bfd0cd1c9aa3c4e4f39869d8303b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NXly";i:1530322571;}', 1530326171),
('e5b7313e278fb18168e18d5b846c42f7', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Y9V4";i:1530322571;}', 1530326171),
('9ced91d0dd140dfd9ed185cf86847893', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C1wS";i:1530322571;}', 1530326171),
('1c23ec8f612483cfe2f5916321ceb019', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LSXT";i:1530322571;}', 1530326171),
('40b73658cc5ca8fc2190e06f11d66d61', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"GVQw";i:1530322571;}', 1530326171),
('d6430dea1aee0182e60046961bbc0838', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"N9t0";i:1530322570;}', 1530326170),
('aaaa6950829f6cd552b483da6cdfe5b2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Bo8o";i:1530322569;}', 1530326169),
('b1ffb5f586978de58b9a208143746ca3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LH39";i:1530322569;}', 1530326169),
('b99c04c743e1a52797d545a8ae031e30', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"x4MM";i:1530322367;}', 1530325967),
('22ce421b88f5cd8c214eded5869ffbc1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F3kk";i:1530322365;}', 1530325965),
('466a0e5a73c4d7f28973a004a2e0f917', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wdo7";i:1530322357;}', 1530325957),
('025c5087f2af78d2f285202713105944', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vpzI";i:1530322322;}', 1530325922),
('76bd3a3d2442147be980f407a63d8647', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Qj9B";i:1530322229;}', 1530325829),
('ba5dc4a01087979a0a8d40d39f16f35f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"beHn";i:1530322222;}', 1530325822),
('e32ec7e14b69ecdcbf4ae68512f961f2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"gPp2";i:1530322220;}', 1530325820),
('ae3330801252b247ef7378bd9bbe7854', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yKLf";i:1530322217;}', 1530325817),
('e3d57a50d3bc6111f82e211e97a97ba4', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uzcc";i:1530322124;}', 1530325724),
('eddabc48456328fa21a6cfb8f7e5074a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vcVe";i:1530322121;}', 1530325721),
('f90eed1a1576d5c60d02f289a7243938', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"e79N";i:1530322120;}', 1530325720),
('26133e4ee524fb5608409f1274e6b1bd', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XuuU";i:1530322120;}', 1530325720),
('1dc82a75033d9df7dc02abc638e2f3ba', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"W3L3";i:1530322120;}', 1530325720),
('e425e67fe74f452e009fb40e40500978', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K3t7";i:1530322119;}', 1530325719),
('37bae1711d3175eb9fcb37d5fc24109d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mVcO";i:1530322119;}', 1530325719),
('1b5be056b4d15b4eb2a4c04c942f0731', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kFj8";i:1530322119;}', 1530325719),
('4fa89756ece3d6974a6be96b3b0c82cb', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"SHGb";i:1530322119;}', 1530325719),
('eff4115a5459d633faae780c48659097', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"B1Fm";i:1530322119;}', 1530325719),
('b0f9a2d469db5ffb1f462d719ca13e69', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dN3K";i:1530322119;}', 1530325719),
('4bd80a1e40379ed2cb1f27b1372af50e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mgzs";i:1530321917;}', 1530325517),
('7fd4d7736bc010fd14a9f7447617340f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"J4A4";i:1530321914;}', 1530325514),
('b3fe6c32d3a8c314d126394a22a30513', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aY39";i:1530321886;}', 1530325486),
('73259d567df3a9f671b3eecc2c3d27f2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LoWj";i:1530321882;}', 1530325482),
('cb16df5221dce4b2acd6314309f18111', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"a3EZ";i:1530321881;}', 1530325481),
('bf125bfcad68893d11823b122f44284d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jJUE";i:1530321881;}', 1530325481),
('ecf5c005eb45d3db9885c8a9dc1e80d8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HY7Q";i:1530321881;}', 1530325481),
('94813a62c0539f3890623c10ce0e25ca', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Cm8A";i:1530321880;}', 1530325480),
('e3e9e5b4c4f18a76a674946645e93d2a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"T338";i:1530321880;}', 1530325480),
('5bf4c7f3424bcfaa746dbb2e66da8fd5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"v0N7";i:1530321880;}', 1530325480),
('4c4be73c9c1b3e42464f0954ce945406', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zPCn";i:1530321880;}', 1530325480),
('2367645f154cf2895997af67a09c2837', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JRrz";i:1530321880;}', 1530325480),
('1db8e8615b367a32975be2d22a175329', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CYMF";i:1530321880;}', 1530325480),
('6ff134550831152525d5f3edeb75eaac', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dYLZ";i:1530321683;}', 1530325283),
('3019507819f6d2056e7484468746b88f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"y34S";i:1530321681;}', 1530325281),
('c09f5f5497156d44980225a7d7a4f4c1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"owl5";i:1530321614;}', 1530325214),
('977a1d392211808096d6081bd0d60978', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fVi9";i:1530321611;}', 1530325211),
('f34288c76e4b1f7863929c5b8e7d0e0c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i61A";i:1530321610;}', 1530325210),
('40d2e7d17cea888dd215b1e37d093bc2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"xS20";i:1530321610;}', 1530325210),
('017607bbf012a1e0e54b062939f6c022', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YEnF";i:1530321609;}', 1530325209),
('aeb70655b570b376d9fea007dfa7150d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RPp0";i:1530321609;}', 1530325209),
('42a4efbaebb3c762c4854cc9f40aca8f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"u7h7";i:1530321609;}', 1530325209),
('f9291ef62b74397c9bb831c017882c29', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"owBm";i:1530321609;}', 1530325209),
('12ca30c28659e3dd3c3feb4c66070164', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"R6ND";i:1530321609;}', 1530325209),
('a082658f6ce585da48b3b3339ba18359', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"G553";i:1530321609;}', 1530325209),
('a6426bb2f277c08bdb4f06bd76a166f8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"L1b1";i:1530321609;}', 1530325209),
('cbf8295fa00f844a148571f5323fe100', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lH79";i:1530321606;}', 1530325206),
('dc9c37443006f4afc8ee2a619d88eb88', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C3Zo";i:1530321605;}', 1530325205),
('601c81381ce37b0655f9c1b14b087104', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qGG8";i:1530321604;}', 1530325204),
('11c6959079f805b71a7199757dec4126', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hu6U";i:1530321604;}', 1530325204),
('5fa62386344966bcb5aabac9f5f2ad53', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lmp7";i:1530321603;}', 1530325203),
('e4d419ecdf4daabc8deb949651d158e2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"FR4X";i:1530321603;}', 1530325203),
('1771e3210a367364c710e4dc44e82156', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"D95k";i:1530321603;}', 1530325203),
('695f58f64d7052b5fad3260957e7db1f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NTDX";i:1530321603;}', 1530325203),
('fe8894d65394fdb5da9a2cf39ea750ef', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EIvB";i:1530321603;}', 1530325203),
('16082504f5e0dd91958877c68a4f8a48', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eX4V";i:1530321603;}', 1530325203),
('3330d15024e90aa835a01ac44d7c2d0b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"e307";i:1530321603;}', 1530325203),
('f0faefde9649d5abf151c40b110b5204', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Vftg";i:1530321586;}', 1530325186),
('c82b38312f9afb52490a28a8104d31f8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hd8R";i:1530321585;}', 1530325185),
('107e3e0728c84eb122da0830e41d3ff3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fg60";i:1530321585;}', 1530325185),
('e5945b3ad03f86d19b0386325eaa423b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZdoM";i:1530321584;}', 1530325184),
('0d8232a33c60f90c8512b3a7fb18afe3', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oxsf";i:1530321584;}', 1530325184),
('2d7a7f095bd1211a3cd8362620fba302', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dEhg";i:1530321584;}', 1530325184),
('7b852079c666d7992ac1408a318e5f2e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"p8Oy";i:1530321584;}', 1530325184),
('0bc8bbaa827a56c1c9299b6025fe2282', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F5mG";i:1530321584;}', 1530325184),
('6e8cff21d95a66a2be1eb10160d8c1a1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"q4Eu";i:1530321584;}', 1530325184),
('c90ae355578b312d566c2af9b931df51', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"w964";i:1530321584;}', 1530325184),
('246630e3e0cea0d365c374741cf976da', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"bXhx";i:1530321549;}', 1530325149),
('b99b668e28ca1f3ef745b51987478c84', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tbU0";i:1530321549;}', 1530325149),
('242953d65b1137132e58ea2162b075fe', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZKOC";i:1530321549;}', 1530325149),
('4a680c721f8fe9da0b1733c955034219', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i82u";i:1530321548;}', 1530325148),
('5f78636432457cc9a5b48f881bfe0619', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"DxGk";i:1530321548;}', 1530325148),
('7ba18283d92ba80c03d91a1a9bdd5817', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"trzz";i:1530321548;}', 1530325148),
('602599e1ee41bd8920d0421836a4cfc2', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vOJp";i:1530321548;}', 1530325148),
('f0c72a3bc46140a5832e637de12d984b', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VTj6";i:1530321548;}', 1530325148),
('7657475ddd6f14632a07444026c1aede', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sgrp";i:1530321548;}', 1530325148),
('b6c9fe48798b1ebf1c220896fce35a7d', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Kr6O";i:1530321548;}', 1530325148),
('636fec64bad07d4d30eee317a5271130', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"W7kI";i:1530321506;}', 1530325106),
('787b5ea38f9136a4a4b60b58392e67db', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yUwW";i:1530321498;}', 1530325098),
('30002d82e0a4d197f1bd1418d36ce1ea', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"PhbB";i:1530321494;}', 1530325094),
('69aeb5269c4a1c402e1b4559629c801e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Cv98";i:1530321485;}', 1530325085),
('bbd6b66dbccac678b74dad6fd15f0948', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"o7mZ";i:1530321477;}', 1530325077),
('009c393ac12bd74f3d23d8a2f0cdc6b5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"cpdN";i:1530321477;}', 1530325077),
('058bc0f426b8b46dac1701ded1110695', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"I34l";i:1530321476;}', 1530325076),
('4f17079c901bed8703fd42c16563ef1e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fT6X";i:1530321475;}', 1530325075),
('dd0c5ffaaa3acf0012247d9389256f9a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NU5p";i:1530321474;}', 1530325074),
('5676788f5971012e6d487c64e91d13a7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kYcL";i:1530321474;}', 1530325074),
('e3e204b3bbf48055be6df961d736a98c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"j6aF";i:1530321474;}', 1530325074),
('9d4b2f7c70e38e0ffc1dc0440891600b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Lxvp";i:1530321474;}', 1530325074),
('5826b221a2fa95dc33719b0016912cd0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oR0M";i:1530321474;}', 1530325074),
('9db85dd3ece39764139a71063bb8f452', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"u8FX";i:1530321474;}', 1530325074),
('4dea4d9e177a3c97011f4ab1e1518195', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i030";i:1530321460;}', 1530325060),
('91834ab08af99f2efab9f15c147cc646', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eoo8";i:1530321460;}', 1530325060),
('024cb572a4aaa27bb3d15dea7f4daf1a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QgXH";i:1530321459;}', 1530325059),
('15af699644b5364449126ad61602cf10', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"v4uc";i:1530321459;}', 1530325059),
('4ebe4107b8dfe68a3adc1ea4e724cdfc', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lZI9";i:1530321458;}', 1530325058),
('a7d841029e63224e4460c29f98d013a8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hI8f";i:1530321458;}', 1530325058),
('e3776e23edea6e4869fb4e6d551892ae', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Gmnt";i:1530321458;}', 1530325058),
('af3ebdff9442e5c6b8ca87e20ac9c1e8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"m2Q8";i:1530321458;}', 1530325058),
('b8c5db61e9d1ebc4f47512cdcdcedb5b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"d83I";i:1530321458;}', 1530325058),
('f064c5678bb06a69974bff9c02489eb9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IUf1";i:1530321458;}', 1530325058),
('451ea068bb50fdd5c739af149ca70673', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tKxJ";i:1530321425;}', 1530325025),
('44724f040ed9b91d5c12e570f4f8d87c', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nM74";i:1530321420;}', 1530325020),
('4c5d95b82e514ac60bb8f1134b10d5ad', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"n6WX";i:1530321420;}', 1530325020),
('7062eb6386a014102a3ef3fdf9a3948e', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"BL9g";i:1530321419;}', 1530325019),
('b1c660088cfc0eaaa19e31177b5e595f', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XU7C";i:1530321419;}', 1530325019),
('6337dffcd606bc48a2b7e5dc80bc21c6', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AT63";i:1530321419;}', 1530325019),
('b67e4dc51027ce4af4a1101f71739f22', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uJLl";i:1530321419;}', 1530325019),
('750b7981b8e85b41b6da66f78e1780b3', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K671";i:1530321419;}', 1530325019),
('0b7426472c6707c5cce7c6eaedcf1bd6', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uIfu";i:1530321419;}', 1530325019),
('0142925bfb1b63430bcd1eeca8d3b12b', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RzJu";i:1530321419;}', 1530325019),
('1e20051d0e9b162eb20ca46dc3213f33', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UHg3";i:1530321418;}', 1530325018),
('39f8f5565437e0d7b137caf9b0894e84', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VO6R";i:1530321387;}', 1530324987),
('2189937203689bdfa69d1c5eab8f498c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uGrd";i:1530321386;}', 1530324986),
('847f4d4d5d28cb74089d7746e4336caa', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Dzpa";i:1530321386;}', 1530324986),
('774896757331d0e0b3123a565c24f7f0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VHRO";i:1530321387;}', 1530324987),
('6569a859b01c33cac02c97562929b6f5', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QhZU";i:1530321385;}', 1530324985),
('dc03d9ef9b3cb2d5d3450e8fbfea95b1', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VBWY";i:1530321385;}', 1530324985),
('2cea2a9cc38296e0816188cdaad1b85c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Inf7";i:1530321385;}', 1530324985),
('3d77f6b5dd8ca136744ebd057bffe3fb', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"pX72";i:1530321385;}', 1530324985),
('270dd5e12cd51622d9de8d119147562a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qO99";i:1530321385;}', 1530324985),
('a7aebd8bac924d5dabb9f754f5c747fe', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UtrV";i:1530321385;}', 1530324985),
('da56497ff296df45a9d7e17d0cc182b4', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZyMb";i:1530321293;}', 1530324893),
('b51e687682e0704bbbfbc106f3c79065', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dXJA";i:1530321293;}', 1530324893),
('5bb42d79beac6f782de0db3f644fed37', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rcC1";i:1530321292;}', 1530324892),
('f75839969b7e34ecc22828fd8ad60be6', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lhRE";i:1530321292;}', 1530324892),
('438644e782c751987514390b127936da', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oxZ4";i:1530321292;}', 1530324892),
('b3de203bd9634c57ecb11081f1e73b6d', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"TZxN";i:1530321292;}', 1530324892),
('872d979420bd4c96ef3a458123b437c5', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"TcaC";i:1530321292;}', 1530324892),
('278cd4f94098eeb11f9e71c667089dd5', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sRuZ";i:1530321292;}', 1530324892),
('715618691d149941097a18667bfb2a53', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C1Oh";i:1530321292;}', 1530324892),
('c447b69368e479612b192f51999fb036', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZFfI";i:1530321291;}', 1530324891),
('d9713c5012eef809f623910fadaca63d', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i190";i:1530321187;}', 1530324787),
('2419b5471f457ee9e59c70d35113f995', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Lgjh";i:1530321187;}', 1530324787),
('a5499574fd361ab090ade38094b9edaa', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"s9IO";i:1530321186;}', 1530324786),
('b6fdc2c42f53724f005ff2856f95abfb', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"cUMG";i:1530321184;}', 1530324784),
('3354dcbda3b1d0e4f398b7156ba47400', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QVIB";i:1530321184;}', 1530324784),
('dea33fb790923a35f8999431f63e8c03', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UmhL";i:1530321185;}', 1530324785),
('597f7cd773d8ab655c8b1d3eca2c43e0', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VU83";i:1530321184;}', 1530324784),
('66ea7de723dd0438fda6331d34fb7b71', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jp6c";i:1530321184;}', 1530324784),
('4c5b03fa5b60d02a883c36f61ef31050', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KX2n";i:1530321184;}', 1530324784),
('3236087baa60c923fe9f6e636379b857', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"K77e";i:1530321184;}', 1530324784),
('e110b5e99c94ffe4b7b203fdffa1b2ad', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"nBob";i:1530321066;}', 1530324666),
('2edd20eec11e5d1a27008d501b8be924', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ysli";i:1530321026;}', 1530324626),
('8018aa865623d4420dce4be42763934f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HpRE";i:1530320990;}', 1530324590),
('b3d51fce4047d7413caf7483ef7f2d94', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"r7Do";i:1530320988;}', 1530324588),
('98ffc9d639289b7f608ce9a80425571c', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RpMs";i:1530320983;}', 1530324583),
('32293cc185cf9ed4b3ed592354d1288f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NSB8";i:1530320982;}', 1530324582),
('f2e553ad8f303800c67069a1bd279037', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"DBA7";i:1530320981;}', 1530324581),
('6beecc60c85cdb7b96a7550571c151e0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hz67";i:1530320982;}', 1530324582),
('77cc4e38c00f4a2d8128665c9415ef9a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"A1m0";i:1530320982;}', 1530324582),
('b6509b8d2dba9d50ad7f2612d0d1806e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"N0L4";i:1530320981;}', 1530324581),
('ce97d5fd014b934174217c367b7c4f7e', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WyjS";i:1530320981;}', 1530324581),
('7a605c45be661caee38f3a2f9c6537ec', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Dt65";i:1530320981;}', 1530324581),
('2aace0880c7728f21d493a7926991606', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IxTT";i:1530320981;}', 1530324581),
('beb8150feaf7d4cdb8f9c24087df3592', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KJ0e";i:1530320716;}', 1530324316),
('bdaee807fc13ea4cfe4b164b3fcdc37a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RPcs";i:1530320981;}', 1530324581),
('eba99b701ad8adb44506ae4fab4ab894', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wizF";i:1530320713;}', 1530324313),
('c925f82134ee9970d1582e8ae93e5de8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Zu91";i:1530320714;}', 1530324314),
('d24e512d4196a23a7197c24213edf52f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Bh6o";i:1530320714;}', 1530324314),
('3998dbb180a6c6f625a6fc06af800756', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JHaE";i:1530320712;}', 1530324312),
('26bd9996f9f686d82571d65522d44dd0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"m8bo";i:1530320712;}', 1530324312),
('c552586b03eb5f527bcfc0716a1c7971', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"PD2v";i:1530320711;}', 1530324311),
('02c636731723fa85b199d954e29ff96d', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"n27V";i:1530320711;}', 1530324311),
('77c3e182ea9d6d03a75bb186fad4f0c9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JUNS";i:1530320712;}', 1530324312),
('e12de889587f3f944bd7fc1c95f66df2', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ej6c";i:1530320712;}', 1530324312),
('eee5541902d05837febd0e663d71c9d0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qmTO";i:1530320684;}', 1530324284),
('f62b019b26bd6522cd2dc30cf270ad28', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lDTh";i:1530320711;}', 1530324311),
('08a5098d5e89142045809ebd9e44735e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"j1Gh";i:1530320679;}', 1530324279),
('65838ac72e28cc206970e786dbaf256e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"cxj7";i:1530320679;}', 1530324279),
('6b08870b8622f31f06909e10bf327c35', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"a3J2";i:1530320679;}', 1530324279),
('65d5dffb5758fa4686f8ba5a1808ada4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zmOU";i:1530320679;}', 1530324279),
('535cdb71775265c79f683e26a2a44c67', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KO72";i:1530320679;}', 1530324279),
('cb10241e381441b02d76127a57e27191', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"o88c";i:1530320680;}', 1530324280),
('e1da5ad99237c43821992d8763bb55c9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zuzU";i:1530320680;}', 1530324280),
('fa2ae786cbc0e1c699befeb2750332fe', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oM12";i:1530320681;}', 1530324281),
('3c250127647925e27a8e9a5c8fac5421', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"fFU2";i:1530320681;}', 1530324281),
('f1c2375974d2049460237a7995bdb7ca', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ic0g";i:1530320682;}', 1530324282),
('ae7d01ba3d2e69b1487e3b13b23ac427', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CXb6";i:1530320658;}', 1530324258),
('cdefaf99673578d6430ffa8029565875', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RMk5";i:1530320666;}', 1530324266),
('d8d3e08de184e01c023478f9d89cad21', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"xwy5";i:1530320679;}', 1530324279),
('3dcf354c99f4fe57df27da2a9400a776', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UfiF";i:1530320656;}', 1530324256),
('d04394f20ebd9a22d028b43447c4e50f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Mt7q";i:1530320653;}', 1530324253),
('dbec404bedbb1893221cf1561f84fdcc', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AJ5q";i:1530320654;}', 1530324254),
('f0f8ceb30641d93b754fe4f92b92a8cc', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eSql";i:1530320654;}', 1530324254),
('31270a099db48e685720e9f8d4cff717', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"U4ss";i:1530320653;}', 1530324253),
('7aff800a91155320091b761e52a93e2b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XRzb";i:1530320652;}', 1530324252),
('a6b60ab824df920a11702a2712f774e4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KF5N";i:1530320652;}', 1530324252),
('8d29d6a57532f6222a07409f1b67bb6d', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"GMPp";i:1530320652;}', 1530324252),
('948083142f648038bb4229fd9ffff763', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ok0K";i:1530320652;}', 1530324252),
('08281073387c9f416e6aa29ec67d2b66', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UOkk";i:1530320652;}', 1530324252),
('119de49ce86583cf4e0578f4f56bb4de', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ygzk";i:1530320652;}', 1530324252),
('74201e451cf0ffa602c44933c47cfca4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZACK";i:1530320618;}', 1530324218),
('06499326728782d337248c6655ebed19', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"iKy7";i:1530320576;}', 1530324176),
('f07b4a9660c5efd6fd59505124915cec', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jqth";i:1530320569;}', 1530324169),
('e9842758f83eb572980841579b3f1771', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mud1";i:1530320564;}', 1530324164),
('009fd4fefe9b2cf2c9bcbd526accdc00', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"r9H6";i:1530320557;}', 1530324157),
('7b37575f0c0f52029a51680118eb7d3f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"e9u2";i:1530320553;}', 1530324153),
('595b2b5d5cd2f7e0877058516a0e8ea8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"T6A4";i:1530320549;}', 1530324149),
('312c212b0f8ab157363c049aa84e6f03', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"I4nV";i:1530320549;}', 1530324149),
('7c3b2670f04e5264bdb2bf2f7ca952c8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Bopw";i:1530320549;}', 1530324149),
('c672a57b5ac691ca7b319d21a2af15cf', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"W9fD";i:1530320548;}', 1530324148),
('ba521d7da6f3a6b45e5fd214f813a624', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IR6r";i:1530320548;}', 1530324148),
('02ec06ea28f448ac513add450324ac07', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sbq9";i:1530320548;}', 1530324148),
('db0fe929da461b7fb85770b558072fc2', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"y3j2";i:1530320548;}', 1530324148),
('52b13799568cf4a44522f46ff9964206', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"pB00";i:1530320548;}', 1530324148),
('515b106e16347c39585e9546547bd2a4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OnTL";i:1530320548;}', 1530324148),
('aeeaa577422d57b01b5df1958ca0e7a0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"pp3V";i:1530320548;}', 1530324148),
('bcff3a352efea11a5194d36ef2754621', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lSll";i:1530320469;}', 1530324069),
('6749d3289cd5add34a37a89933f8244c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"BrZz";i:1530320414;}', 1530324014),
('06e0cf0b2a1c793e9cd1e112e40210e9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"l4S0";i:1530320406;}', 1530324006),
('9004d663fa0c2c16a7eecd34b1d6b61a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wA2n";i:1530320404;}', 1530324004),
('68e56c4af96783dc986b4decd2da6112', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"G2oH";i:1530320402;}', 1530324002),
('a90ff76e64cfe27af7521d7bce220a1c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VJJf";i:1530320401;}', 1530324001),
('eb20e1e187d3d728f8eb582e4ceac2f3', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"c8Iz";i:1530320401;}', 1530324001),
('5e38bd859fb658a4bcb5e8bb39304ec3', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oKKU";i:1530320400;}', 1530324000),
('00e74d7d173d0952a20936e9cef3b23f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"QbPQ";i:1530320399;}', 1530323999),
('fe0066df9493b246724e6b295854d523', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CGNR";i:1530320399;}', 1530323999),
('0b87f273f1b3707931cd2880d0a7a100', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jP78";i:1530320399;}', 1530323999),
('5597fe8fbdb47153d32d4d8b9f283532', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"L5pl";i:1530320399;}', 1530323999),
('ce294b52aca6e29ab5e1aca538aa6b50', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"cALC";i:1530320399;}', 1530323999),
('571934709936d06dcb3c7a8b03d92fec', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OjNO";i:1530320399;}', 1530323999),
('c839c5d39ab9791a599aab19edcbda9e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rTzR";i:1530320382;}', 1530323982),
('4b1b098f0df0ad4099ff739ce7272d1e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"IxHw";i:1530320366;}', 1530323966),
('d6847406ee679e16adb0c2c785a44189', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"pWav";i:1530320364;}', 1530323964),
('18093b45225c3175226f8512bd08d58c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VTRF";i:1530320359;}', 1530323959),
('e4742c11bb40595dbe3a630d8424c45b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RPAd";i:1530320357;}', 1530323957),
('a0a2df8cfb4e84dd8e55850fb268a5f5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hmn6";i:1530320355;}', 1530323955),
('76a197249072490e71c214822c307f51', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"pR34";i:1530320352;}', 1530323952),
('565a46e0331cde1ffc64bc46d8b322a9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jK87";i:1530320342;}', 1530323942),
('b1352640ea3d77fc5ddcae6830dd842d', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CGgt";i:1530320340;}', 1530323940),
('618535546a97bcebc5dad6b13b94ea7a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YigZ";i:1530320339;}', 1530323939),
('d136e3b92109e1435dd3d0b94faba85e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JzNA";i:1530320339;}', 1530323939),
('819b2b24f4f581101dba527b858de783', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EWRC";i:1530320339;}', 1530323939),
('c71769ac794497b1679ee191073bfbb4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WeF7";i:1530320338;}', 1530323938),
('a77ed25ccea5cbee51dee2b77922b28d', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mV3A";i:1530320338;}', 1530323938),
('ac9dad2da3785593656665e46ea4d9d8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JI2p";i:1530320338;}', 1530323938),
('83800a42370b4b0c004671c073e03407', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"C2AR";i:1530320338;}', 1530323938),
('1d6bd89fe0ad5d9ffa68c4a226fa0ee1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OE5v";i:1530320338;}', 1530323938),
('7c37bacd181850aea6d6f28b00b456e1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"H9DD";i:1530320338;}', 1530323938),
('b5a20a3bafba4753de25baa855dc718a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sjR7";i:1530320338;}', 1530323938),
('f2bcb4e0b26482b40c947b86c23bbbc4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yQY4";i:1530320332;}', 1530323932),
('4bb87dd0a369843d6d4f8aead2a00885', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"TfJG";i:1530320332;}', 1530323932),
('804034dcd8661f5feffe7caf950eb10c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mlyl";i:1530320332;}', 1530323932),
('e2122ec725fb6f83722144390af7498b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"PH3X";i:1530320331;}', 1530323931),
('1a24a95879367c27eeee636565f73f59', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"tG3T";i:1530320331;}', 1530323931),
('94d1680e355c0da73676d30b4d4aecb3', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"s7dd";i:1530320331;}', 1530323931),
('42f3979b3c4ca8da2d735b46b224a02c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ylf3";i:1530320331;}', 1530323931),
('ab19fe582f85ad7a84ea20bacd8cb126', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Dxp9";i:1530320331;}', 1530323931),
('eba7c0e799572009b1f79436f27be234', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zrgy";i:1530320331;}', 1530323931);
INSERT INTO `ims_core_sessions` (`sid`, `uniacid`, `openid`, `data`, `expiretime`) VALUES
('a906a0506bf9b2a0b3300e4e25c11ed7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KQL2";i:1530320331;}', 1530323931),
('2c68dd2d81991c82f4a8f3ab8e73f583', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"v5EZ";i:1530320264;}', 1530323864),
('6179fc1f25564f2dcd28fe33c128f4bb', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LoOb";i:1530320261;}', 1530323861),
('9547636fc612fc9c0bee32a79690a961', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XxIp";i:1530320258;}', 1530323858),
('4cfc2f609361ebf5308e5244f4fbd0b4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i1ro";i:1530320257;}', 1530323857),
('1a7cc59a819ad38b187c58c4ba1984a5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WWvi";i:1530320256;}', 1530323856),
('2e35d61596d9a218561d00b3e8768047', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RsWr";i:1530320256;}', 1530323856),
('57be9ffaa083b894a96477acfcfee8ee', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"lz55";i:1530320255;}', 1530323855),
('d907379bcb51d56e5c50b2319ef5063b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yBCb";i:1530320255;}', 1530323855),
('d8eae7641b8220f7e8f7e0c33ade8e77', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"uKRY";i:1530320255;}', 1530323855),
('5a550f3cb3e28127ba4bd69d36738046', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i3LS";i:1530320255;}', 1530323855),
('05c8e2496cb4c3c4ace39e82ddb7cd0c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"wKgA";i:1530320255;}', 1530323855),
('5d714347a1d70a8464b8b106769e22c5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kLLb";i:1530320255;}', 1530323855),
('4a4e62a6c6df53ff4a75712b135b1bb3', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"v72B";i:1530320066;}', 1530323666),
('2dd5f76c20d19285797e3be9ae2ea439', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"iLpL";i:1530320063;}', 1530323663),
('e0b30cc7a1f0728dd6e443f707dcfd2a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WJO4";i:1530320057;}', 1530323657),
('a0f11fa7d2e248b33c07dd0cba12be35', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"qUsA";i:1530320057;}', 1530323657),
('b8b54c5ff01f0ef9112f32b42be7a0f0', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HXQ2";i:1530320056;}', 1530323656),
('5f7edeb7b26e18d9a8700a40884b6e05', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"r9ZQ";i:1530320055;}', 1530323655),
('f5abbde9327e47f8bf1beb117a2d9b1b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"X1I1";i:1530320054;}', 1530323654),
('f0d10f94949e1ab5a6fd735ae4c4b58b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XLlg";i:1530320054;}', 1530323654),
('ee0d90a6b3eee95c2fe91f6584479041', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yeRW";i:1530320054;}', 1530323654),
('c5219d070e8eefba87569d7b62d52c77', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KvTx";i:1530320054;}', 1530323654),
('996b60362a21908f7016d3094869af29', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"r206";i:1530320054;}', 1530323654),
('cbb2b64032621d90067fe7e91b7e3dd6', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"J5Zy";i:1530320054;}', 1530323654),
('6c9b98e61520761c1d9ea7bdf6870b51', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"DaNy";i:1530319940;}', 1530323540),
('f9571c1db4c1789bb84a0db1c228f9de', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Zs58";i:1530319936;}', 1530323536),
('802674eb06c1361593b72bc3bb8fc3a1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"D98o";i:1530319933;}', 1530323533),
('66da87a55a0c9988831cf13ff03fb487', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sj8A";i:1530319933;}', 1530323533),
('a80ded50f7efad2afcb5828bad858f4c', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Yso4";i:1530319932;}', 1530323532),
('68a154becaca0aa893294cbe8e67a4fb', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F4Q7";i:1530319932;}', 1530323532),
('9aee903e0a21d184383548347e1f6909', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"txmJ";i:1530319931;}', 1530323531),
('8b91e8040172e257eb9a402890cf2c83', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CkQj";i:1530319931;}', 1530323531),
('ee91330d3bc74ab95ac614d92e9dc4f9', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Nx0B";i:1530319931;}', 1530323531),
('f9bc64558da9a7688b30a836b33d0217', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"X5XA";i:1530319931;}', 1530323531),
('dffbbc188f317c10f86b4ff25871b7ab', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"D3X3";i:1530319931;}', 1530323531),
('ba860a30058d67dc98e9ee8145c51481', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"q478";i:1530319931;}', 1530323531),
('0b8e9a54707f67dc0a996fd3dc8b3aea', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"DRGZ";i:1530319923;}', 1530323523),
('08e07f7f15428b90d05ff164b16a6be3', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"D9TH";i:1530319918;}', 1530323518),
('6bf8e3d5c34ac6c2ae4e3431ceb6f555', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"y482";i:1530319917;}', 1530323517),
('57aa3b29a3844bb5f009f6bec77b5cf8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"G70E";i:1530319916;}', 1530323516),
('77ad26acbb14ac05471a217873beefe8', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"gTxD";i:1530319915;}', 1530323515),
('d11f9e1709b773e85ad177d8acb85dad', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XMp5";i:1530319915;}', 1530323515),
('7eb04dfc32dc9bdad113300bc33e9d64', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"B742";i:1530319915;}', 1530323515),
('55e54d873602d021f3f701b7f52d3e85', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"q2Oi";i:1530319915;}', 1530323515),
('929332442e051c07d36d59a83a290667', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LIkP";i:1530319915;}', 1530323515),
('bf8de566a75b9ff00c81df85cc985a3a', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LCRy";i:1530319915;}', 1530323515),
('81b7de67222ecb6f546845eb4368c9da', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VzdS";i:1530319915;}', 1530323515),
('8654989b5f59a27317ab5a813a4a1d1e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"O02U";i:1530319802;}', 1530323402),
('6b859e01d17f9f317e5c47f8437d021f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"j4wE";i:1530319797;}', 1530323397),
('169fa066d6b0d9e73cd4ac17e520663f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"sL4h";i:1530319791;}', 1530323391),
('089a5aa36483f080d1d3c41dbdc98cd1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aiwC";i:1530319791;}', 1530323391),
('7195e824d62c894cd0ed640dfe527cf1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"q0Zz";i:1530319790;}', 1530323390),
('83d75147512a4fa708eedffc12e7f7f3', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"iywI";i:1530319790;}', 1530323390),
('bcc5dae4b88e81b65fb736a4ed7fc7c7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"B0TS";i:1530319790;}', 1530323390),
('916395fa16efcdc2f98f462461725ae5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AHGr";i:1530319789;}', 1530323389),
('f8bebec61687c07dbaa031f46829f412', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KIH4";i:1530319789;}', 1530323389),
('538f30783949ba266b0e69a6454debb4', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oMbD";i:1530319789;}', 1530323389),
('26dfeca1ef378b7d9a8d9ad491fc411e', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"H83W";i:1530319789;}', 1530323389),
('e796c4d00145ae6b14106958dd757ac7', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LHav";i:1530319789;}', 1530323389),
('4fa1f636f67ca082ca28e3bbb826322f', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Y97Z";i:1530319766;}', 1530323366),
('f8449c49f760fc5413b54d0b1999cf37', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NfSO";i:1530319765;}', 1530323365),
('a5cd61e2ccdbb690b5449c37d2b52308', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"prHF";i:1530319765;}', 1530323365),
('efc0b813f8ee9982e2641a16cf161c43', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VT6y";i:1530319764;}', 1530323364),
('131a108fbe64665d5becae6991895f33', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Kq2q";i:1530319764;}', 1530323364),
('93a9fa1ff03da23cf16ac46cf3258e69', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"YR7f";i:1530319764;}', 1530323364),
('52344a9a4eb41e641edff47bc0f2175b', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"SYZK";i:1530319764;}', 1530323364),
('79e8d44affdf53d12c7e2d1994e96cce', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"A0mM";i:1530319764;}', 1530323364),
('9b8632ac2f12c0897db3559614f3d8b1', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oGj4";i:1530319764;}', 1530323364),
('203af87b84d8aa677b730b327471ec80', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"n2vH";i:1530319764;}', 1530323364),
('e581ad296abd5a956b40f062ce25e0bd', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HF83";i:1530319735;}', 1530323335),
('556c4e9f0a4548495e3a102abadea131', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"W5tB";i:1530319735;}', 1530323335),
('f551b94745e473a68c0c2393761fdb98', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kTnc";i:1530319734;}', 1530323334),
('4809aaa6412f02b8af6a1ef42ba8ad81', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RKjQ";i:1530319734;}', 1530323334),
('aadeb518a7c22691c36a275e55104581', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KdDh";i:1530319734;}', 1530323334),
('f802294a2383de158919f413987da932', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WUU0";i:1530319734;}', 1530323334),
('fe76f523da739c0b02d4363106658783', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"DiaW";i:1530319734;}', 1530323334),
('be9556e28f9a016180ddae0e57a5ae64', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aNff";i:1530319734;}', 1530323334),
('5734dc9f6a77e008c25fa6c418c294bd', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"HKuk";i:1530319734;}', 1530323334),
('2c24481f4d6e3fac806912a3414f2189', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Huzn";i:1530319734;}', 1530323334),
('0238919b501c85ed2a413431e72ba375', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ywbu";i:1530319720;}', 1530323320),
('4ddd279d7c854620ffecbbf80f006487', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"VMrA";i:1530319720;}', 1530323320),
('571d18406807504d307acbaf32d51002', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Wpdo";i:1530319719;}', 1530323319),
('68095e3c06107b2aaf1374c956010248', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UZY5";i:1530319719;}', 1530323319),
('b950e24c2d99abb2c8035409201d5d35', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"KG5k";i:1530319719;}', 1530323319),
('4fe0b9b69e70c02839c91c5541b0dfe5', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rZPX";i:1530319719;}', 1530323319),
('31929a6f621214a5fee0de2c10e5a961', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"dye4";i:1530319718;}', 1530323318),
('8ea72fbd05b45b4e3c831c16f7ad8e10', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"p64K";i:1530319718;}', 1530323318),
('c697795761e3fa5c2b588555681560be', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i3w8";i:1530319718;}', 1530323318),
('ffd3d9d8d19f26d893cbf4e52b076ff6', 2, '122.5.45.150', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"V01z";i:1530319718;}', 1530323318),
('779f25a2b6bb40295314a677e31997e9', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"feI8";i:1530319297;}', 1530322897),
('49205d9c5d54ab2107a01970161f78ea', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"c4I4";i:1530319294;}', 1530322894),
('7929012c60d9f6b64642e419ec301e60', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jr4K";i:1530319284;}', 1530322884),
('6655e603b0a118a5b6c78354b48b43c0', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"s2kc";i:1530319284;}', 1530322884),
('e36f3db22837fada1bdfdfc93c20c1c4', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"i72y";i:1530319283;}', 1530322883),
('942c834f03fff1c721e4d7f652e11d0b', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AKHD";i:1530319283;}', 1530322883),
('a55748435ce24bf41bad4b4ca811f92e', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"WOny";i:1530319283;}', 1530322883),
('816e116ac3ea89300a773728849093c2', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Uzqr";i:1530319283;}', 1530322883),
('68d787bcca2843436c148c342a91d241', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"UvZ5";i:1530319283;}', 1530322883),
('6bf12dbb0e6de211046be39b40110404', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"S3FI";i:1530319283;}', 1530322883),
('f3cb04c0125a2e784af2cb2227d59352', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"U9jB";i:1530319283;}', 1530322883),
('2fec505172abf0ecc6c9e76a5a5eeb25', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ppFM";i:1530319283;}', 1530322883),
('200b230668d223d8ff1e06dcd86a9c41', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mZA6";i:1530319281;}', 1530322881),
('484f46e06e53a4342aec8ef6459a4b2d', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eiO5";i:1530319281;}', 1530322881),
('633c32310c404614d9d6a021e5b52ef4', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mmOE";i:1530319280;}', 1530322880),
('ac8998b2fc6a56f9f91c7d55ce34e9bb', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XmPo";i:1530319280;}', 1530322880),
('42750352f7ee74c933591ef784beba82', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rzs7";i:1530319280;}', 1530322880),
('a9fdcbf62693869044d706318f0718a3', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kDaM";i:1530319280;}', 1530322880),
('c99f0fe4b1fd14fcb71e8b554f3eebbe', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ZKzh";i:1530319280;}', 1530322880),
('0dc9f237b2510a40f6cd980b16075f03', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"hccW";i:1530319280;}', 1530322880),
('d907f27607f0adedf357251c71626f6c', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"P35c";i:1530319280;}', 1530322880),
('344e27ad446cd083d6ff75423d74f86a', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"mfaQ";i:1530319280;}', 1530322880),
('7d28e92e76589162c7b5d91b7349b36e', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"vK89";i:1530319277;}', 1530322877),
('ec190b56b9c9213cbc015583c8c38341', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"j2DO";i:1530319273;}', 1530322873),
('6d39bc32af681ba664373687fe4f2061', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"kmQF";i:1530319273;}', 1530322873),
('c08aa4941921deaba7aad78d7f10ef57', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"SM9t";i:1530319272;}', 1530322872),
('f587ac7fb2d9f1618fdd0ba6221c4efb', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"phcv";i:1530319272;}', 1530322872),
('b1a72e8a3bea3f26f83fe3580df2626e', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eAHA";i:1530319272;}', 1530322872),
('8edd4e7df6207522ce2ae0405e16fe47', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"XJqK";i:1530319272;}', 1530322872),
('ae56c42deef52d964eaca82a2fddabae', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"gSu7";i:1530319272;}', 1530322872),
('5be3fa118cb6a43c5a5c6f75b3b631ee', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"p7iN";i:1530319272;}', 1530322872),
('5ea8c83996cdf36e3f2d0f166a1d2678', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"W9CJ";i:1530319272;}', 1530322872),
('9ab6c69eeaca218fc5485ee6d33cf8f3', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AGKd";i:1530319271;}', 1530322871),
('bafc83bcf077b311075a2523fc684d9a', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"NU6d";i:1530319229;}', 1530322829),
('787803c2624d003b1a1ea9a45a0db505', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"u7Uc";i:1530319228;}', 1530322828),
('1dc114abf9487858a7c617f9f4e2ba6d', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"y8c0";i:1530319228;}', 1530322828),
('a0fb8d18d83a69de2f1db41b43bddb23', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"F033";i:1530319228;}', 1530322828),
('b94334bf752790d47cd1d63b655d0b0b', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Ny7L";i:1530319227;}', 1530322827),
('3af6c0a85241be145b9740b5f731c8cd', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"v4Xz";i:1530319227;}', 1530322827),
('174b5df2f586d489581d193978be7dc9', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Uw6W";i:1530319227;}', 1530322827),
('e7e40ebc5293d768f2836602a3a147e8', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aBQ5";i:1530319227;}', 1530322827),
('1703f82fb364cba771a293b3aaf8e19a', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"q4E5";i:1530319227;}', 1530322827),
('1d47c1af29f989ddd5b3fe18a16de5b1', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CcLa";i:1530319227;}', 1530322827),
('521e9b6683e8e1b5c8080c1ceabbb537', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"aoKz";i:1530319221;}', 1530322821),
('e071e068b82f0d6c9f0ae527d47cdb2c', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"OJzk";i:1530319221;}', 1530322821),
('e8b0c85d5ca3c4ca1f70d99581c9b478', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"TuzM";i:1530319220;}', 1530322820),
('38e45fb6e0c44011ff11fc02b07ff477', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"AxXt";i:1530319221;}', 1530322821),
('03a9c0c6e7618166d1f81600c77ec026', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"oLfz";i:1530319220;}', 1530322820),
('7de772a1e9c24b60b0b398827895ebba', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zx9v";i:1530319220;}', 1530322820),
('5c3666ac915db5a53e4f111ac7bea880', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"eDV9";i:1530319220;}', 1530322820),
('96c5a09b7026ec0f81c5059709645401', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"PvFF";i:1530319220;}', 1530322820),
('183d4e022acb4716bf7188da03b8e1b9', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"yl2b";i:1530319219;}', 1530322819),
('be2568603b1b72681bc6473817ce1fb1', 2, '122.5.18.10', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Wub6";i:1530319219;}', 1530322819),
('a49974387ab92ff2bc3b1fa989877325', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"EKvr";i:1530319199;}', 1530322799),
('eeeb52bfcf6b4885f57b386c52149add', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"ByM5";i:1530319198;}', 1530322798),
('e504cda795a136ac22a3e20cc43fdfce', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"e7Mt";i:1530319198;}', 1530322798),
('724070a1962c0ce922aacac6ad3a3408', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"CgOp";i:1530319197;}', 1530322797),
('4618b5bab638f77d1eb101cdb371b428', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"JT63";i:1530319197;}', 1530322797),
('c8e959aa8b7c53f67fab21033eb0f282', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"RMNV";i:1530319196;}', 1530322796),
('061fe3fb7e85f45e4962912007172b5a', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"zOzO";i:1530319196;}', 1530322796),
('3f62c2d2f0a4a316f5733cb90f5cd3cb', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LA72";i:1530319196;}', 1530322796),
('dd186a45c3922c1b41d7b6db844db470', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"rtRm";i:1530319196;}', 1530322796),
('5342bdf50a1247a80bc820f96545fbab', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"Jg5x";i:1530319196;}', 1530322796),
('55e0a44aa15fdd0e8853804ef7637a2f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"b9Hk";i:1530319192;}', 1530322792),
('04df0e12d6690d307ec729808978eca0', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"jEyV";i:1530319192;}', 1530322792),
('4cf9c47e9c4b4508460332a28babc586', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"naSn";i:1530319192;}', 1530322792),
('4af2b0d4af304e06b1a6512cba91a4d8', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"LA6c";i:1530319192;}', 1530322792),
('41d5316cc0d3dcb7fa9d605d65c480da', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"U9LL";i:1530319192;}', 1530322792),
('fb305d2e64d7a4ee27f330088d722f9f', 2, '221.0.90.28', 'acid|s:1:"2";uniacid|i:2;token|a:1:{s:4:"d3LL";i:1530319192;}', 1530322792);

-- --------------------------------------------------------

--
-- 表的结构 `ims_core_settings`
--

CREATE TABLE IF NOT EXISTS `ims_core_settings` (
  `key` varchar(200) NOT NULL,
  `value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_core_settings`
--

INSERT INTO `ims_core_settings` (`key`, `value`) VALUES
('authmode', 'i:1;'),
('close', 'a:2:{s:6:"status";s:1:"0";s:6:"reason";s:0:"";}'),
('register', 'a:4:{s:4:"open";i:1;s:6:"verify";i:0;s:4:"code";i:1;s:7:"groupid";i:1;}'),
('copyright', 'a:1:{s:6:"slides";a:3:{i:0;s:58:"https://img.alicdn.com/tps/TB1pfG4IFXXXXc6XXXXXXXXXXXX.jpg";i:1;s:58:"https://img.alicdn.com/tps/TB1sXGYIFXXXXc5XpXXXXXXXXXX.jpg";i:2;s:58:"https://img.alicdn.com/tps/TB1h9xxIFXXXXbKXXXXXXXXXXXX.jpg";}}');

-- --------------------------------------------------------

--
-- 表的结构 `ims_coupon_location`
--

CREATE TABLE IF NOT EXISTS `ims_coupon_location` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `sid` int(10) unsigned NOT NULL,
  `location_id` int(10) unsigned NOT NULL,
  `business_name` varchar(50) NOT NULL,
  `branch_name` varchar(50) NOT NULL,
  `category` varchar(255) NOT NULL,
  `province` varchar(15) NOT NULL,
  `city` varchar(15) NOT NULL,
  `district` varchar(15) NOT NULL,
  `address` varchar(50) NOT NULL,
  `longitude` varchar(15) NOT NULL,
  `latitude` varchar(15) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `photo_list` varchar(10000) NOT NULL,
  `avg_price` int(10) unsigned NOT NULL,
  `open_time` varchar(50) NOT NULL,
  `recommend` varchar(255) NOT NULL,
  `special` varchar(255) NOT NULL,
  `introduction` varchar(255) NOT NULL,
  `offset_type` tinyint(3) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_cover_reply`
--

CREATE TABLE IF NOT EXISTS `ims_cover_reply` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `module` varchar(30) NOT NULL,
  `do` varchar(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_cover_reply`
--

INSERT INTO `ims_cover_reply` (`id`, `uniacid`, `multiid`, `rid`, `module`, `do`, `title`, `description`, `thumb`, `url`) VALUES
(1, 1, 0, 7, 'mc', '', '进入个人中心', '', '', './index.php?c=mc&a=home&i=1'),
(2, 1, 1, 8, 'site', '', '进入首页', '', '', './index.php?c=home&i=1&t=1');

-- --------------------------------------------------------

--
-- 表的结构 `ims_custom_reply`
--

CREATE TABLE IF NOT EXISTS `ims_custom_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `start1` int(10) NOT NULL,
  `end1` int(10) NOT NULL,
  `start2` int(10) NOT NULL,
  `end2` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_images_reply`
--

CREATE TABLE IF NOT EXISTS `ims_images_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_cash_record`
--

CREATE TABLE IF NOT EXISTS `ims_mc_cash_record` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `clerk_id` int(10) unsigned NOT NULL,
  `store_id` int(10) unsigned NOT NULL,
  `clerk_type` tinyint(3) unsigned NOT NULL,
  `fee` decimal(10,2) unsigned NOT NULL,
  `final_fee` decimal(10,2) unsigned NOT NULL,
  `credit1` int(10) unsigned NOT NULL,
  `credit1_fee` decimal(10,2) unsigned NOT NULL,
  `credit2` decimal(10,2) unsigned NOT NULL,
  `cash` decimal(10,2) unsigned NOT NULL,
  `return_cash` decimal(10,2) unsigned NOT NULL,
  `final_cash` decimal(10,2) unsigned NOT NULL,
  `remark` varchar(255) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `trade_type` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_chats_record`
--

CREATE TABLE IF NOT EXISTS `ims_mc_chats_record` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `flag` tinyint(3) unsigned NOT NULL,
  `openid` varchar(32) NOT NULL,
  `msgtype` varchar(15) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_credits_recharge`
--

CREATE TABLE IF NOT EXISTS `ims_mc_credits_recharge` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `tid` varchar(64) NOT NULL,
  `transid` varchar(30) NOT NULL,
  `fee` varchar(10) NOT NULL,
  `type` varchar(15) NOT NULL,
  `tag` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `backtype` tinyint(3) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_credits_record`
--

CREATE TABLE IF NOT EXISTS `ims_mc_credits_record` (
  `id` int(11) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `uniacid` int(11) NOT NULL,
  `credittype` varchar(10) NOT NULL,
  `num` decimal(10,2) NOT NULL,
  `operator` int(10) unsigned NOT NULL,
  `module` varchar(30) NOT NULL,
  `clerk_id` int(10) unsigned NOT NULL,
  `store_id` int(10) unsigned NOT NULL,
  `clerk_type` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `remark` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_fans_groups`
--

CREATE TABLE IF NOT EXISTS `ims_mc_fans_groups` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `groups` varchar(10000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_fans_tag_mapping`
--

CREATE TABLE IF NOT EXISTS `ims_mc_fans_tag_mapping` (
  `id` int(11) unsigned NOT NULL,
  `fanid` int(11) unsigned NOT NULL,
  `tagid` tinyint(3) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_groups`
--

CREATE TABLE IF NOT EXISTS `ims_mc_groups` (
  `groupid` int(11) NOT NULL,
  `uniacid` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `credit` int(10) unsigned NOT NULL,
  `isdefault` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_mc_groups`
--

INSERT INTO `ims_mc_groups` (`groupid`, `uniacid`, `title`, `credit`, `isdefault`) VALUES
(1, 1, '默认会员组', 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_handsel`
--

CREATE TABLE IF NOT EXISTS `ims_mc_handsel` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) NOT NULL,
  `touid` int(10) unsigned NOT NULL,
  `fromuid` varchar(32) NOT NULL,
  `module` varchar(30) NOT NULL,
  `sign` varchar(100) NOT NULL,
  `action` varchar(20) NOT NULL,
  `credit_value` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_mapping_fans`
--

CREATE TABLE IF NOT EXISTS `ims_mc_mapping_fans` (
  `fanid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `groupid` varchar(30) NOT NULL,
  `salt` char(8) NOT NULL,
  `follow` tinyint(1) unsigned NOT NULL,
  `followtime` int(10) unsigned NOT NULL,
  `unfollowtime` int(10) unsigned NOT NULL,
  `tag` varchar(1000) NOT NULL,
  `updatetime` int(10) unsigned DEFAULT NULL,
  `unionid` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_mapping_ucenter`
--

CREATE TABLE IF NOT EXISTS `ims_mc_mapping_ucenter` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `centeruid` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_mass_record`
--

CREATE TABLE IF NOT EXISTS `ims_mc_mass_record` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `groupname` varchar(50) NOT NULL,
  `fansnum` int(10) unsigned NOT NULL,
  `msgtype` varchar(10) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `group` int(10) NOT NULL,
  `attach_id` int(10) unsigned NOT NULL,
  `media_id` varchar(100) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `cron_id` int(10) unsigned NOT NULL,
  `sendtime` int(10) unsigned NOT NULL,
  `finalsendtime` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_members`
--

CREATE TABLE IF NOT EXISTS `ims_mc_members` (
  `uid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(8) NOT NULL,
  `groupid` int(11) NOT NULL,
  `credit1` decimal(10,2) unsigned NOT NULL,
  `credit2` decimal(10,2) unsigned NOT NULL,
  `credit3` decimal(10,2) unsigned NOT NULL,
  `credit4` decimal(10,2) unsigned NOT NULL,
  `credit5` decimal(10,2) unsigned NOT NULL,
  `credit6` decimal(10,2) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `realname` varchar(10) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `vip` tinyint(3) unsigned NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthyear` smallint(6) unsigned NOT NULL,
  `birthmonth` tinyint(3) unsigned NOT NULL,
  `birthday` tinyint(3) unsigned NOT NULL,
  `constellation` varchar(10) NOT NULL,
  `zodiac` varchar(5) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `idcard` varchar(30) NOT NULL,
  `studentid` varchar(50) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `resideprovince` varchar(30) NOT NULL,
  `residecity` varchar(30) NOT NULL,
  `residedist` varchar(30) NOT NULL,
  `graduateschool` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `education` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `revenue` varchar(10) NOT NULL,
  `affectivestatus` varchar(30) NOT NULL,
  `lookingfor` varchar(255) NOT NULL,
  `bloodtype` varchar(5) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` varchar(5) NOT NULL,
  `alipay` varchar(30) NOT NULL,
  `msn` varchar(30) NOT NULL,
  `taobao` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `bio` text NOT NULL,
  `interest` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_member_address`
--

CREATE TABLE IF NOT EXISTS `ims_mc_member_address` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(50) unsigned NOT NULL,
  `username` varchar(20) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `zipcode` varchar(6) NOT NULL,
  `province` varchar(32) NOT NULL,
  `city` varchar(32) NOT NULL,
  `district` varchar(32) NOT NULL,
  `address` varchar(512) NOT NULL,
  `isdefault` tinyint(1) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_member_fields`
--

CREATE TABLE IF NOT EXISTS `ims_mc_member_fields` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) NOT NULL,
  `fieldid` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `displayorder` smallint(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_member_property`
--

CREATE TABLE IF NOT EXISTS `ims_mc_member_property` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(11) NOT NULL,
  `property` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mc_oauth_fans`
--

CREATE TABLE IF NOT EXISTS `ims_mc_oauth_fans` (
  `id` int(10) unsigned NOT NULL,
  `oauth_openid` varchar(50) NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_menu_event`
--

CREATE TABLE IF NOT EXISTS `ims_menu_event` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `keyword` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `picmd5` varchar(32) NOT NULL,
  `openid` varchar(128) NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_message_notice_log`
--

CREATE TABLE IF NOT EXISTS `ims_message_notice_log` (
  `id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `is_read` tinyint(3) NOT NULL,
  `uid` int(11) NOT NULL,
  `sign` varchar(22) NOT NULL,
  `type` tinyint(3) NOT NULL,
  `status` tinyint(3) DEFAULT NULL,
  `create_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_mobilenumber`
--

CREATE TABLE IF NOT EXISTS `ims_mobilenumber` (
  `id` int(11) NOT NULL,
  `rid` int(10) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `dateline` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_modules`
--

CREATE TABLE IF NOT EXISTS `ims_modules` (
  `mid` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `version` varchar(15) NOT NULL,
  `ability` varchar(500) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `author` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `settings` tinyint(1) NOT NULL,
  `subscribes` varchar(500) NOT NULL,
  `handles` varchar(500) NOT NULL,
  `isrulefields` tinyint(1) NOT NULL,
  `issystem` tinyint(1) unsigned NOT NULL,
  `target` int(10) unsigned NOT NULL,
  `iscard` tinyint(3) unsigned NOT NULL,
  `permissions` varchar(5000) NOT NULL,
  `title_initial` varchar(1) NOT NULL,
  `wxapp_support` tinyint(1) NOT NULL,
  `app_support` tinyint(1) NOT NULL,
  `welcome_support` int(2) NOT NULL,
  `oauth_type` tinyint(1) NOT NULL DEFAULT '1',
  `webapp_support` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_modules`
--

INSERT INTO `ims_modules` (`mid`, `name`, `type`, `title`, `version`, `ability`, `description`, `author`, `url`, `settings`, `subscribes`, `handles`, `isrulefields`, `issystem`, `target`, `iscard`, `permissions`, `title_initial`, `wxapp_support`, `app_support`, `welcome_support`, `oauth_type`, `webapp_support`) VALUES
(1, 'basic', 'system', '基本文字回复', '1.0', '和您进行简单对话', '一问一答得简单对话. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的回复内容.', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(2, 'news', 'system', '基本混合图文回复', '1.0', '为你提供生动的图文资讯', '一问一答得简单对话, 但是回复内容包括图片文字等更生动的媒体内容. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的图文回复内容.', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(3, 'music', 'system', '基本音乐回复', '1.0', '提供语音、音乐等音频类回复', '在回复规则中可选择具有语音、音乐等音频类的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝，实现一问一答得简单对话。', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(4, 'userapi', 'system', '自定义接口回复', '1.1', '更方便的第三方接口设置', '自定义接口又称第三方接口，可以让开发者更方便的接入微擎系统，高效的与微信公众平台进行对接整合。', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(5, 'recharge', 'system', '会员中心充值模块', '1.0', '提供会员充值功能', '', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 0, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(6, 'custom', 'system', '多客服转接', '1.0.0', '用来接入腾讯的多客服系统', '', 'WeEngine Team', 'http://bbs.we7.cc', 0, 'a:0:{}', 'a:6:{i:0;s:5:"image";i:1;s:5:"voice";i:2;s:5:"video";i:3;s:8:"location";i:4;s:4:"link";i:5;s:4:"text";}', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(7, 'images', 'system', '基本图片回复', '1.0', '提供图片回复', '在回复规则中可选择具有图片的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝图片。', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(8, 'video', 'system', '基本视频回复', '1.0', '提供图片回复', '在回复规则中可选择具有视频的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝视频。', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(9, 'voice', 'system', '基本语音回复', '1.0', '提供语音回复', '在回复规则中可选择具有语音的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝语音。', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(10, 'chats', 'system', '发送客服消息', '1.0', '公众号可以在粉丝最后发送消息的48小时内无限制发送消息', '', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 0, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(11, 'wxcard', 'system', '微信卡券回复', '1.0', '微信卡券回复', '微信卡券回复', 'WeEngine Team', 'http://www.we7.cc/', 0, '', '', 1, 1, 0, 0, '', '', 1, 2, 0, 0, 1),
(12, 'zh_gjhdbm', 'activity', '活动报名高级版', '3.4', '找好活动、办好活动,用活动行!活动行提供创业、互联网、科技、金融、教育、亲子、生活、聚会交友等50多种活动,是深受主办方信赖和城市白领喜爱的活动平台', '找好活动、办好活动,用活动行!活动行提供创业、互联网、科技、金融、教育、亲子、生活、聚会交友等50多种活动,是深受主办方信赖和城市白领喜爱的活动平台', '微猫源码', 'http://www.weixin2015.cn/', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 0, 'N;', 'H', 2, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `ims_modules_bindings`
--

CREATE TABLE IF NOT EXISTS `ims_modules_bindings` (
  `eid` int(11) NOT NULL,
  `module` varchar(100) NOT NULL,
  `entry` varchar(10) NOT NULL,
  `call` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `do` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `direct` int(11) NOT NULL,
  `url` varchar(100) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `displayorder` tinyint(255) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_modules_plugin`
--

CREATE TABLE IF NOT EXISTS `ims_modules_plugin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `main_module` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_modules_rank`
--

CREATE TABLE IF NOT EXISTS `ims_modules_rank` (
  `id` int(10) unsigned NOT NULL,
  `module_name` varchar(100) NOT NULL,
  `uid` int(10) NOT NULL,
  `rank` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_modules_recycle`
--

CREATE TABLE IF NOT EXISTS `ims_modules_recycle` (
  `id` int(10) NOT NULL,
  `modulename` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_music_reply`
--

CREATE TABLE IF NOT EXISTS `ims_music_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `url` varchar(300) NOT NULL,
  `hqurl` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_news_reply`
--

CREATE TABLE IF NOT EXISTS `ims_news_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `parent_id` int(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `author` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(500) NOT NULL,
  `content` mediumtext NOT NULL,
  `url` varchar(255) NOT NULL,
  `displayorder` int(10) NOT NULL,
  `incontent` tinyint(1) NOT NULL,
  `createtime` int(10) NOT NULL,
  `media_id` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_profile_fields`
--

CREATE TABLE IF NOT EXISTS `ims_profile_fields` (
  `id` int(10) unsigned NOT NULL,
  `field` varchar(255) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `displayorder` smallint(6) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `unchangeable` tinyint(1) NOT NULL,
  `showinregister` tinyint(1) NOT NULL,
  `field_length` int(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_profile_fields`
--

INSERT INTO `ims_profile_fields` (`id`, `field`, `available`, `title`, `description`, `displayorder`, `required`, `unchangeable`, `showinregister`, `field_length`) VALUES
(1, 'realname', 1, '真实姓名', '', 0, 1, 1, 1, 0),
(2, 'nickname', 1, '昵称', '', 1, 1, 0, 1, 0),
(3, 'avatar', 1, '头像', '', 1, 0, 0, 0, 0),
(4, 'qq', 1, 'QQ号', '', 0, 0, 0, 1, 0),
(5, 'mobile', 1, '手机号码', '', 0, 0, 0, 0, 0),
(6, 'vip', 1, 'VIP级别', '', 0, 0, 0, 0, 0),
(7, 'gender', 1, '性别', '', 0, 0, 0, 0, 0),
(8, 'birthyear', 1, '出生生日', '', 0, 0, 0, 0, 0),
(9, 'constellation', 1, '星座', '', 0, 0, 0, 0, 0),
(10, 'zodiac', 1, '生肖', '', 0, 0, 0, 0, 0),
(11, 'telephone', 1, '固定电话', '', 0, 0, 0, 0, 0),
(12, 'idcard', 1, '证件号码', '', 0, 0, 0, 0, 0),
(13, 'studentid', 1, '学号', '', 0, 0, 0, 0, 0),
(14, 'grade', 1, '班级', '', 0, 0, 0, 0, 0),
(15, 'address', 1, '邮寄地址', '', 0, 0, 0, 0, 0),
(16, 'zipcode', 1, '邮编', '', 0, 0, 0, 0, 0),
(17, 'nationality', 1, '国籍', '', 0, 0, 0, 0, 0),
(18, 'resideprovince', 1, '居住地址', '', 0, 0, 0, 0, 0),
(19, 'graduateschool', 1, '毕业学校', '', 0, 0, 0, 0, 0),
(20, 'company', 1, '公司', '', 0, 0, 0, 0, 0),
(21, 'education', 1, '学历', '', 0, 0, 0, 0, 0),
(22, 'occupation', 1, '职业', '', 0, 0, 0, 0, 0),
(23, 'position', 1, '职位', '', 0, 0, 0, 0, 0),
(24, 'revenue', 1, '年收入', '', 0, 0, 0, 0, 0),
(25, 'affectivestatus', 1, '情感状态', '', 0, 0, 0, 0, 0),
(26, 'lookingfor', 1, ' 交友目的', '', 0, 0, 0, 0, 0),
(27, 'bloodtype', 1, '血型', '', 0, 0, 0, 0, 0),
(28, 'height', 1, '身高', '', 0, 0, 0, 0, 0),
(29, 'weight', 1, '体重', '', 0, 0, 0, 0, 0),
(30, 'alipay', 1, '支付宝帐号', '', 0, 0, 0, 0, 0),
(31, 'msn', 1, 'MSN', '', 0, 0, 0, 0, 0),
(32, 'email', 1, '电子邮箱', '', 0, 0, 0, 0, 0),
(33, 'taobao', 1, '阿里旺旺', '', 0, 0, 0, 0, 0),
(34, 'site', 1, '主页', '', 0, 0, 0, 0, 0),
(35, 'bio', 1, '自我介绍', '', 0, 0, 0, 0, 0),
(36, 'interest', 1, '兴趣爱好', '', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ims_qrcode`
--

CREATE TABLE IF NOT EXISTS `ims_qrcode` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `type` varchar(10) NOT NULL,
  `extra` int(10) unsigned NOT NULL,
  `qrcid` bigint(20) NOT NULL,
  `scene_str` varchar(64) NOT NULL,
  `name` varchar(50) NOT NULL,
  `keyword` varchar(100) NOT NULL,
  `model` tinyint(1) unsigned NOT NULL,
  `ticket` varchar(250) NOT NULL,
  `url` varchar(256) NOT NULL,
  `expire` int(10) unsigned NOT NULL,
  `subnum` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_qrcode_stat`
--

CREATE TABLE IF NOT EXISTS `ims_qrcode_stat` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `qid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `qrcid` bigint(20) unsigned NOT NULL,
  `scene_str` varchar(64) NOT NULL,
  `name` varchar(50) NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_rule`
--

CREATE TABLE IF NOT EXISTS `ims_rule` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL,
  `displayorder` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `containtype` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_rule`
--

INSERT INTO `ims_rule` (`id`, `uniacid`, `name`, `module`, `displayorder`, `status`, `containtype`) VALUES
(1, 0, '城市天气', 'userapi', 255, 1, ''),
(2, 0, '百度百科', 'userapi', 255, 1, ''),
(3, 0, '即时翻译', 'userapi', 255, 1, ''),
(4, 0, '今日老黄历', 'userapi', 255, 1, ''),
(5, 0, '看新闻', 'userapi', 255, 1, ''),
(6, 0, '快递查询', 'userapi', 255, 1, ''),
(7, 1, '个人中心入口设置', 'cover', 0, 1, ''),
(8, 1, '微擎团队入口设置', 'cover', 0, 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_rule_keyword`
--

CREATE TABLE IF NOT EXISTS `ims_rule_keyword` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `content` varchar(255) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_rule_keyword`
--

INSERT INTO `ims_rule_keyword` (`id`, `rid`, `uniacid`, `module`, `content`, `type`, `displayorder`, `status`) VALUES
(1, 1, 0, 'userapi', '^.+天气$', 3, 255, 1),
(2, 2, 0, 'userapi', '^百科.+$', 3, 255, 1),
(3, 2, 0, 'userapi', '^定义.+$', 3, 255, 1),
(4, 3, 0, 'userapi', '^@.+$', 3, 255, 1),
(5, 4, 0, 'userapi', '日历', 1, 255, 1),
(6, 4, 0, 'userapi', '万年历', 1, 255, 1),
(7, 4, 0, 'userapi', '黄历', 1, 255, 1),
(8, 4, 0, 'userapi', '几号', 1, 255, 1),
(9, 5, 0, 'userapi', '新闻', 1, 255, 1),
(10, 6, 0, 'userapi', '^(申通|圆通|中通|汇通|韵达|顺丰|EMS) *[a-z0-9]{1,}$', 3, 255, 1),
(11, 7, 1, 'cover', '个人中心', 1, 0, 1),
(12, 8, 1, 'cover', '首页', 1, 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_article`
--

CREATE TABLE IF NOT EXISTS `ims_site_article` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `iscommend` tinyint(1) NOT NULL,
  `ishot` tinyint(1) unsigned NOT NULL,
  `pcate` int(10) unsigned NOT NULL,
  `ccate` int(10) unsigned NOT NULL,
  `template` varchar(300) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `incontent` tinyint(1) NOT NULL,
  `source` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `displayorder` int(10) unsigned NOT NULL,
  `linkurl` varchar(500) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `edittime` int(10) NOT NULL,
  `click` int(10) unsigned NOT NULL,
  `type` varchar(10) NOT NULL,
  `credit` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_category`
--

CREATE TABLE IF NOT EXISTS `ims_site_category` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `nid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `parentid` int(10) unsigned NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `icon` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `linkurl` varchar(500) NOT NULL,
  `ishomepage` tinyint(1) NOT NULL,
  `icontype` tinyint(1) unsigned NOT NULL,
  `css` varchar(500) NOT NULL,
  `multiid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_multi`
--

CREATE TABLE IF NOT EXISTS `ims_site_multi` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(30) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `site_info` text NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `bindhost` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_site_multi`
--

INSERT INTO `ims_site_multi` (`id`, `uniacid`, `title`, `styleid`, `site_info`, `status`, `bindhost`) VALUES
(1, 1, '微擎团队', 1, '', 1, '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_nav`
--

CREATE TABLE IF NOT EXISTS `ims_site_nav` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `section` tinyint(4) NOT NULL,
  `module` varchar(50) NOT NULL,
  `displayorder` smallint(5) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  `icon` varchar(500) NOT NULL,
  `css` varchar(1000) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `categoryid` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_page`
--

CREATE TABLE IF NOT EXISTS `ims_site_page` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `params` longtext NOT NULL,
  `html` longtext NOT NULL,
  `multipage` longtext NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `goodnum` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_slide`
--

CREATE TABLE IF NOT EXISTS `ims_site_slide` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `displayorder` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_store_create_account`
--

CREATE TABLE IF NOT EXISTS `ims_site_store_create_account` (
  `id` int(10) NOT NULL,
  `uid` int(10) NOT NULL,
  `uniacid` int(10) NOT NULL,
  `type` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_store_goods`
--

CREATE TABLE IF NOT EXISTS `ims_site_store_goods` (
  `id` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL,
  `title` varchar(100) NOT NULL,
  `module` varchar(50) NOT NULL,
  `account_num` int(10) NOT NULL,
  `wxapp_num` int(10) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `unit` varchar(15) NOT NULL,
  `slide` varchar(1000) NOT NULL,
  `category_id` int(10) NOT NULL,
  `title_initial` varchar(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createtime` int(10) NOT NULL,
  `synopsis` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `module_group` int(10) NOT NULL,
  `api_num` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_store_order`
--

CREATE TABLE IF NOT EXISTS `ims_site_store_order` (
  `id` int(10) unsigned NOT NULL,
  `orderid` varchar(28) NOT NULL,
  `goodsid` int(10) NOT NULL,
  `duration` int(10) NOT NULL,
  `buyer` varchar(50) NOT NULL,
  `buyerid` int(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `changeprice` tinyint(1) NOT NULL,
  `createtime` int(10) NOT NULL,
  `uniacid` int(10) NOT NULL,
  `endtime` int(15) NOT NULL,
  `wxapp` int(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_styles`
--

CREATE TABLE IF NOT EXISTS `ims_site_styles` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `templateid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_site_styles`
--

INSERT INTO `ims_site_styles` (`id`, `uniacid`, `templateid`, `name`) VALUES
(1, 1, 1, '微站默认模板_gC5C');

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_styles_vars`
--

CREATE TABLE IF NOT EXISTS `ims_site_styles_vars` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `templateid` int(10) unsigned NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `variable` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_site_templates`
--

CREATE TABLE IF NOT EXISTS `ims_site_templates` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  `version` varchar(64) NOT NULL,
  `description` varchar(500) NOT NULL,
  `author` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `sections` int(10) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_site_templates`
--

INSERT INTO `ims_site_templates` (`id`, `name`, `title`, `version`, `description`, `author`, `url`, `type`, `sections`) VALUES
(1, 'default', '微站默认模板', '', '由微擎提供默认微站模板套系', '微擎团队', 'http://we7.cc', '1', 0);

-- --------------------------------------------------------

--
-- 表的结构 `ims_stat_fans`
--

CREATE TABLE IF NOT EXISTS `ims_stat_fans` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `new` int(10) unsigned NOT NULL,
  `cancel` int(10) unsigned NOT NULL,
  `cumulate` int(10) NOT NULL,
  `date` varchar(8) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_stat_fans`
--

INSERT INTO `ims_stat_fans` (`id`, `uniacid`, `new`, `cancel`, `cumulate`, `date`) VALUES
(1, 1, 0, 0, 0, '20171119'),
(2, 1, 0, 0, 0, '20171118'),
(3, 1, 0, 0, 0, '20171117'),
(4, 1, 0, 0, 0, '20171116'),
(5, 1, 0, 0, 0, '20171115'),
(6, 1, 0, 0, 0, '20171114'),
(7, 1, 0, 0, 0, '20171113'),
(8, 1, 0, 0, 0, '20171209'),
(9, 1, 0, 0, 0, '20171208'),
(10, 1, 0, 0, 0, '20171207'),
(11, 1, 0, 0, 0, '20171206'),
(12, 1, 0, 0, 0, '20171205'),
(13, 1, 0, 0, 0, '20171204'),
(14, 1, 0, 0, 0, '20171203'),
(15, 1, 0, 0, 0, '20171212'),
(16, 1, 0, 0, 0, '20171211'),
(17, 1, 0, 0, 0, '20171210'),
(18, 1, 0, 0, 0, '20171226'),
(19, 1, 0, 0, 0, '20171225'),
(20, 1, 0, 0, 0, '20171224'),
(21, 1, 0, 0, 0, '20171223'),
(22, 1, 0, 0, 0, '20171222'),
(23, 1, 0, 0, 0, '20171221'),
(24, 1, 0, 0, 0, '20171220');

-- --------------------------------------------------------

--
-- 表的结构 `ims_stat_keyword`
--

CREATE TABLE IF NOT EXISTS `ims_stat_keyword` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` varchar(10) NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `hit` int(10) unsigned NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_stat_msg_history`
--

CREATE TABLE IF NOT EXISTS `ims_stat_msg_history` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `from_user` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `type` varchar(10) NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_stat_rule`
--

CREATE TABLE IF NOT EXISTS `ims_stat_rule` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `hit` int(10) unsigned NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_stat_visit`
--

CREATE TABLE IF NOT EXISTS `ims_stat_visit` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) NOT NULL,
  `module` varchar(100) NOT NULL,
  `count` int(10) unsigned NOT NULL,
  `date` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_uni_account`
--

CREATE TABLE IF NOT EXISTS `ims_uni_account` (
  `uniacid` int(10) unsigned NOT NULL,
  `groupid` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `default_acid` int(10) unsigned NOT NULL,
  `rank` int(10) DEFAULT NULL,
  `title_initial` varchar(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_uni_account`
--

INSERT INTO `ims_uni_account` (`uniacid`, `groupid`, `name`, `description`, `default_acid`, `rank`, `title_initial`) VALUES
(1, -1, '蓝狐网络', '一个朝气蓬勃的团队', 1, 0, 'W'),
(2, 0, '交警队', '交警队', 2, NULL, 'J');

-- --------------------------------------------------------

--
-- 表的结构 `ims_uni_account_group`
--

CREATE TABLE IF NOT EXISTS `ims_uni_account_group` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `groupid` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_uni_account_menus`
--

CREATE TABLE IF NOT EXISTS `ims_uni_account_menus` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `menuid` int(10) unsigned NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `title` varchar(30) NOT NULL,
  `sex` tinyint(3) unsigned NOT NULL,
  `group_id` int(10) NOT NULL,
  `client_platform_type` tinyint(3) unsigned NOT NULL,
  `area` varchar(50) NOT NULL,
  `data` text NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `isdeleted` tinyint(3) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_uni_account_modules`
--

CREATE TABLE IF NOT EXISTS `ims_uni_account_modules` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `settings` text NOT NULL,
  `shortcut` tinyint(1) unsigned NOT NULL,
  `displayorder` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_uni_account_users`
--

CREATE TABLE IF NOT EXISTS `ims_uni_account_users` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `role` varchar(255) NOT NULL,
  `rank` tinyint(3) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_uni_group`
--

CREATE TABLE IF NOT EXISTS `ims_uni_group` (
  `id` int(10) unsigned NOT NULL,
  `owner_uid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `modules` varchar(15000) NOT NULL,
  `templates` varchar(5000) NOT NULL,
  `uniacid` int(10) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_uni_group`
--

INSERT INTO `ims_uni_group` (`id`, `owner_uid`, `name`, `modules`, `templates`, `uniacid`) VALUES
(1, 0, '体验套餐服务', 'a:1:{i:0;s:9:"zh_gjhdbm";}', 'N;', 0);

-- --------------------------------------------------------

--
-- 表的结构 `ims_uni_settings`
--

CREATE TABLE IF NOT EXISTS `ims_uni_settings` (
  `uniacid` int(10) unsigned NOT NULL,
  `passport` varchar(200) NOT NULL,
  `oauth` varchar(100) NOT NULL,
  `jsauth_acid` int(10) unsigned NOT NULL,
  `uc` varchar(500) NOT NULL,
  `notify` varchar(2000) NOT NULL,
  `creditnames` varchar(500) NOT NULL,
  `creditbehaviors` varchar(500) NOT NULL,
  `welcome` varchar(60) NOT NULL,
  `default` varchar(60) NOT NULL,
  `default_message` varchar(2000) NOT NULL,
  `payment` text NOT NULL,
  `stat` varchar(300) NOT NULL,
  `default_site` int(10) unsigned DEFAULT NULL,
  `sync` tinyint(3) unsigned NOT NULL,
  `recharge` varchar(500) NOT NULL,
  `tplnotice` varchar(1000) NOT NULL,
  `grouplevel` tinyint(3) unsigned NOT NULL,
  `mcplugin` varchar(500) NOT NULL,
  `exchange_enable` tinyint(3) unsigned NOT NULL,
  `coupon_type` tinyint(3) unsigned NOT NULL,
  `menuset` text NOT NULL,
  `statistics` varchar(100) NOT NULL,
  `bind_domain` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_uni_settings`
--

INSERT INTO `ims_uni_settings` (`uniacid`, `passport`, `oauth`, `jsauth_acid`, `uc`, `notify`, `creditnames`, `creditbehaviors`, `welcome`, `default`, `default_message`, `payment`, `stat`, `default_site`, `sync`, `recharge`, `tplnotice`, `grouplevel`, `mcplugin`, `exchange_enable`, `coupon_type`, `menuset`, `statistics`, `bind_domain`) VALUES
(1, 'a:3:{s:8:"focusreg";i:0;s:4:"item";s:5:"email";s:4:"type";s:8:"password";}', 'a:2:{s:6:"status";s:1:"0";s:7:"account";s:1:"0";}', 0, 'a:1:{s:6:"status";i:0;}', 'a:1:{s:3:"sms";a:2:{s:7:"balance";i:0;s:9:"signature";s:0:"";}}', 'a:5:{s:7:"credit1";a:2:{s:5:"title";s:6:"积分";s:7:"enabled";i:1;}s:7:"credit2";a:2:{s:5:"title";s:6:"余额";s:7:"enabled";i:1;}s:7:"credit3";a:2:{s:5:"title";s:0:"";s:7:"enabled";i:0;}s:7:"credit4";a:2:{s:5:"title";s:0:"";s:7:"enabled";i:0;}s:7:"credit5";a:2:{s:5:"title";s:0:"";s:7:"enabled";i:0;}}', 'a:2:{s:8:"activity";s:7:"credit1";s:8:"currency";s:7:"credit2";}', '', '', '', 'a:4:{s:6:"credit";a:1:{s:6:"switch";b:0;}s:6:"alipay";a:4:{s:6:"switch";b:0;s:7:"account";s:0:"";s:7:"partner";s:0:"";s:6:"secret";s:0:"";}s:6:"wechat";a:5:{s:6:"switch";b:0;s:7:"account";b:0;s:7:"signkey";s:0:"";s:7:"partner";s:0:"";s:3:"key";s:0:"";}s:8:"delivery";a:1:{s:6:"switch";b:0;}}', '', 1, 0, '', '', 0, '', 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_uni_verifycode`
--

CREATE TABLE IF NOT EXISTS `ims_uni_verifycode` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `verifycode` varchar(6) NOT NULL,
  `total` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_userapi_cache`
--

CREATE TABLE IF NOT EXISTS `ims_userapi_cache` (
  `id` int(10) unsigned NOT NULL,
  `key` varchar(32) NOT NULL,
  `content` text NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_userapi_reply`
--

CREATE TABLE IF NOT EXISTS `ims_userapi_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `description` varchar(300) NOT NULL,
  `apiurl` varchar(300) NOT NULL,
  `token` varchar(32) NOT NULL,
  `default_text` varchar(100) NOT NULL,
  `cachetime` int(10) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_userapi_reply`
--

INSERT INTO `ims_userapi_reply` (`id`, `rid`, `description`, `apiurl`, `token`, `default_text`, `cachetime`) VALUES
(1, 1, '"城市名+天气", 如: "北京天气"', 'weather.php', '', '', 0),
(2, 2, '"百科+查询内容" 或 "定义+查询内容", 如: "百科姚明", "定义自行车"', 'baike.php', '', '', 0),
(3, 3, '"@查询内容(中文或英文)"', 'translate.php', '', '', 0),
(4, 4, '"日历", "万年历", "黄历"或"几号"', 'calendar.php', '', '', 0),
(5, 5, '"新闻"', 'news.php', '', '', 0),
(6, 6, '"快递+单号", 如: "申通1200041125"', 'express.php', '', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `ims_users`
--

CREATE TABLE IF NOT EXISTS `ims_users` (
  `uid` int(10) unsigned NOT NULL,
  `owner_uid` int(10) NOT NULL,
  `groupid` int(10) unsigned NOT NULL,
  `founder_groupid` tinyint(4) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `joindate` int(10) unsigned NOT NULL,
  `joinip` varchar(15) NOT NULL,
  `lastvisit` int(10) unsigned NOT NULL,
  `lastip` varchar(15) NOT NULL,
  `remark` varchar(500) NOT NULL,
  `starttime` int(10) unsigned NOT NULL,
  `endtime` int(10) unsigned NOT NULL,
  `register_type` tinyint(3) NOT NULL,
  `openid` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_users`
--

INSERT INTO `ims_users` (`uid`, `owner_uid`, `groupid`, `founder_groupid`, `username`, `password`, `salt`, `type`, `status`, `joindate`, `joinip`, `lastvisit`, `lastip`, `remark`, `starttime`, `endtime`, `register_type`, `openid`) VALUES
(1, 0, 1, 0, 'admin', '4cadcc6c9fcbca527fc4d84fc8feee721f5e2775', 'ed959f35', 0, 0, 1528101825, '', 1530325756, '221.0.90.28', '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_users_bind`
--

CREATE TABLE IF NOT EXISTS `ims_users_bind` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `bind_sign` varchar(50) NOT NULL,
  `third_type` tinyint(4) NOT NULL,
  `third_nickname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_users_failed_login`
--

CREATE TABLE IF NOT EXISTS `ims_users_failed_login` (
  `id` int(10) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL,
  `username` varchar(32) NOT NULL,
  `count` tinyint(1) unsigned NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_users_founder_group`
--

CREATE TABLE IF NOT EXISTS `ims_users_founder_group` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `package` varchar(5000) NOT NULL,
  `maxaccount` int(10) unsigned NOT NULL,
  `maxsubaccount` int(10) unsigned NOT NULL,
  `timelimit` int(10) unsigned NOT NULL,
  `maxwxapp` int(10) unsigned NOT NULL,
  `maxwebapp` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_users_group`
--

CREATE TABLE IF NOT EXISTS `ims_users_group` (
  `id` int(10) unsigned NOT NULL,
  `owner_uid` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `package` varchar(5000) NOT NULL,
  `maxaccount` int(10) unsigned NOT NULL,
  `maxsubaccount` int(10) unsigned NOT NULL,
  `timelimit` int(10) unsigned NOT NULL,
  `maxwxapp` int(10) unsigned NOT NULL,
  `maxwebapp` int(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_users_invitation`
--

CREATE TABLE IF NOT EXISTS `ims_users_invitation` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(64) NOT NULL,
  `fromuid` int(10) unsigned NOT NULL,
  `inviteuid` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_users_permission`
--

CREATE TABLE IF NOT EXISTS `ims_users_permission` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `type` varchar(100) NOT NULL,
  `permission` varchar(10000) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_users_profile`
--

CREATE TABLE IF NOT EXISTS `ims_users_profile` (
  `id` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `edittime` int(10) NOT NULL,
  `realname` varchar(10) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `fakeid` varchar(30) NOT NULL,
  `vip` tinyint(3) unsigned NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthyear` smallint(6) unsigned NOT NULL,
  `birthmonth` tinyint(3) unsigned NOT NULL,
  `birthday` tinyint(3) unsigned NOT NULL,
  `constellation` varchar(10) NOT NULL,
  `zodiac` varchar(5) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `idcard` varchar(30) NOT NULL,
  `studentid` varchar(50) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `resideprovince` varchar(30) NOT NULL,
  `residecity` varchar(30) NOT NULL,
  `residedist` varchar(30) NOT NULL,
  `graduateschool` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `education` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `revenue` varchar(10) NOT NULL,
  `affectivestatus` varchar(30) NOT NULL,
  `lookingfor` varchar(255) NOT NULL,
  `bloodtype` varchar(5) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` varchar(5) NOT NULL,
  `alipay` varchar(30) NOT NULL,
  `msn` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `taobao` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `bio` text NOT NULL,
  `interest` text NOT NULL,
  `workerid` varchar(64) NOT NULL,
  `is_send_mobile_status` tinyint(3) NOT NULL,
  `send_expire_status` tinyint(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_video_reply`
--

CREATE TABLE IF NOT EXISTS `ims_video_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_voice_reply`
--

CREATE TABLE IF NOT EXISTS `ims_voice_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_wechat_attachment`
--

CREATE TABLE IF NOT EXISTS `ims_wechat_attachment` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `media_id` varchar(255) NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `type` varchar(15) NOT NULL,
  `model` varchar(25) NOT NULL,
  `tag` varchar(5000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `module_upload_dir` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_wechat_news`
--

CREATE TABLE IF NOT EXISTS `ims_wechat_news` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned DEFAULT NULL,
  `attach_id` int(10) unsigned NOT NULL,
  `thumb_media_id` varchar(60) NOT NULL,
  `thumb_url` varchar(255) NOT NULL,
  `title` varchar(50) NOT NULL,
  `author` varchar(30) NOT NULL,
  `digest` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `content_source_url` varchar(200) NOT NULL,
  `show_cover_pic` tinyint(3) unsigned NOT NULL,
  `url` varchar(200) NOT NULL,
  `displayorder` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_wxapp_general_analysis`
--

CREATE TABLE IF NOT EXISTS `ims_wxapp_general_analysis` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) NOT NULL,
  `session_cnt` int(10) NOT NULL,
  `visit_pv` int(10) NOT NULL,
  `visit_uv` int(10) NOT NULL,
  `visit_uv_new` int(10) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `stay_time_uv` varchar(10) NOT NULL,
  `stay_time_session` varchar(10) NOT NULL,
  `visit_depth` varchar(10) NOT NULL,
  `ref_date` varchar(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_wxapp_versions`
--

CREATE TABLE IF NOT EXISTS `ims_wxapp_versions` (
  `id` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `version` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `modules` varchar(1000) NOT NULL,
  `design_method` tinyint(1) NOT NULL,
  `template` int(10) NOT NULL,
  `quickmenu` varchar(2500) NOT NULL,
  `createtime` int(10) NOT NULL,
  `type` int(2) NOT NULL DEFAULT '0',
  `entry_id` int(11) NOT NULL DEFAULT '0',
  `appjson` text NOT NULL,
  `default_appjson` text NOT NULL,
  `use_default` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_wxapp_versions`
--

INSERT INTO `ims_wxapp_versions` (`id`, `uniacid`, `multiid`, `version`, `description`, `modules`, `design_method`, `template`, `quickmenu`, `createtime`, `type`, `entry_id`, `appjson`, `default_appjson`, `use_default`) VALUES
(1, 2, 0, '1.01', '交警队', 'a:1:{s:9:"zh_gjhdbm";a:4:{s:4:"name";s:9:"zh_gjhdbm";s:7:"newicon";N;s:7:"version";s:3:"3.4";s:12:"defaultentry";N;}}', 3, 0, 'a:6:{s:5:"color";s:7:"#428bca";s:14:"selected_color";s:4:"#0f0";s:8:"boundary";s:4:"#fff";s:7:"bgcolor";s:7:"#bebebe";s:4:"show";i:1;s:5:"menus";a:2:{i:0;a:5:{s:4:"name";s:6:"首页";s:4:"icon";s:36:"./resource/images/bottom-default.png";s:12:"selectedicon";s:36:"./resource/images/bottom-default.png";s:3:"url";N;s:12:"defaultentry";N;}i:1;a:5:{s:4:"name";s:6:"首页";s:4:"icon";s:36:"./resource/images/bottom-default.png";s:12:"selectedicon";s:36:"./resource/images/bottom-default.png";s:3:"url";N;s:12:"defaultentry";N;}}}', 1528102129, 0, 0, '', '', 1);

-- --------------------------------------------------------

--
-- 表的结构 `ims_wxcard_reply`
--

CREATE TABLE IF NOT EXISTS `ims_wxcard_reply` (
  `id` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(30) NOT NULL,
  `card_id` varchar(50) NOT NULL,
  `cid` int(10) unsigned NOT NULL,
  `brand_name` varchar(30) NOT NULL,
  `logo_url` varchar(255) NOT NULL,
  `success` varchar(255) NOT NULL,
  `error` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_activity`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '发布人id',
  `title` varchar(300) NOT NULL COMMENT '标题',
  `link_tel` varchar(20) NOT NULL COMMENT '联系电话',
  `sponsor` varchar(30) NOT NULL COMMENT '主办方',
  `logo` varchar(200) NOT NULL COMMENT '活动海报',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `total_num` int(11) NOT NULL COMMENT '总数',
  `limit_num` int(11) NOT NULL COMMENT '限制购买数',
  `yd_num` int(11) NOT NULL COMMENT '阅读数量',
  `activity_type` varchar(500) NOT NULL COMMENT '活动场地1线上,2线下',
  `join_type` int(4) NOT NULL COMMENT '参加方式 1在线报名,2无需报名,3其它方式',
  `content` text NOT NULL COMMENT '活动详情',
  `sort` int(11) NOT NULL COMMENT '排序',
  `type_id` int(11) NOT NULL COMMENT '分类id',
  `status` int(4) NOT NULL COMMENT '活动状态1待审核,2审核通过,3拒绝',
  `cityname` varchar(30) NOT NULL COMMENT '城市名称',
  `address` varchar(200) NOT NULL COMMENT '地址信息',
  `coordinates` varchar(50) NOT NULL COMMENT '经纬度',
  `cost` decimal(10,2) NOT NULL COMMENT '活动价格',
  `bm_check` int(4) NOT NULL DEFAULT '1' COMMENT '报名是否需要审核,1是,2否',
  `bm_start` int(11) NOT NULL COMMENT '报名开始时间',
  `bm_end` int(11) NOT NULL COMMENT '活动结束时间',
  `is_close` int(4) NOT NULL DEFAULT '1' COMMENT '活动报名状态1开启,2关闭',
  `bm_info` varchar(100) NOT NULL COMMENT '活动报名信息',
  `hd_imgs` varchar(500) NOT NULL COMMENT '详情多图',
  `views` int(11) NOT NULL COMMENT '浏览量',
  `gz_num` int(11) NOT NULL COMMENT '关注人数',
  `time` int(11) NOT NULL COMMENT '活动创建时间',
  `sh_time` int(11) NOT NULL COMMENT '活动审核时间',
  `uniacid` varchar(50) NOT NULL,
  `zd_money` decimal(10,2) NOT NULL,
  `hx_code` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_activity`
--

INSERT INTO `ims_zh_gjhdbm_activity` (`id`, `user_id`, `title`, `link_tel`, `sponsor`, `logo`, `start_time`, `end_time`, `total_num`, `limit_num`, `yd_num`, `activity_type`, `join_type`, `content`, `sort`, `type_id`, `status`, `cityname`, `address`, `coordinates`, `cost`, `bm_check`, `bm_start`, `bm_end`, `is_close`, `bm_info`, `hd_imgs`, `views`, `gz_num`, `time`, `sh_time`, `uniacid`, `zd_money`, `hx_code`) VALUES
(1, 0, '9999', '99999', '平台', '9999', 1528165125, 1530325080, 0, 0, 0, '1', 0, '<p>99999</p>', 0, 1, 2, '', '', '', '0.00', 1, 0, 0, 1, '', '', 4, 0, 1528166539, 1528166539, '2', '0.00', '9999999'),
(2, 1, '1', '111', 'undefined', '2622438158823103.jpg', 1528128000, 1536422640, 0, 0, 0, '1', 3, '', 0, 1, 1, '', '山东省烟台市莱山区枫林路24号', '37.51104,121.44547', '0.00', 0, 0, 0, 1, ',', '', 0, 0, 1528182390, 0, '2', '0.00', '11111'),
(3, 1, '2', '2222', 'undefined', '9766417522589182.jpg', 1528128000, 1528214400, 0, 0, 0, '1', 3, '', 0, 1, 1, '', '山东省烟台市莱山区新星北街6号', '37.50978,121.44513', '0.00', 0, 0, 0, 1, ',', '', 0, 0, 1528182553, 0, '2', '0.00', '2222');

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_ad`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_ad` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL COMMENT '轮播图标题',
  `logo` varchar(200) NOT NULL COMMENT '图片',
  `status` int(11) NOT NULL COMMENT '1.开启  2.关闭',
  `src` varchar(100) NOT NULL COMMENT '链接',
  `orderby` int(11) NOT NULL COMMENT '排序',
  `xcx_name` varchar(20) NOT NULL,
  `appid` varchar(20) NOT NULL,
  `uniacid` int(11) NOT NULL COMMENT '小程序id',
  `type` int(11) NOT NULL COMMENT '1.首页2.专题',
  `cityname` varchar(50) NOT NULL COMMENT '城市名称',
  `wb_src` varchar(300) NOT NULL COMMENT '外部链接',
  `state` int(4) NOT NULL DEFAULT '1' COMMENT '1内部，2外部,3跳转'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_ad`
--

INSERT INTO `ims_zh_gjhdbm_ad` (`id`, `title`, `logo`, `status`, `src`, `orderby`, `xcx_name`, `appid`, `uniacid`, `type`, `cityname`, `wb_src`, `state`) VALUES
(1, '1', 'images/2/2018/06/dkt1199eEN91q6D9eETDnT7049dK19.jpg', 1, '', 0, '', '', 2, 1, '', '', 1);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_article`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_article` (
  `article_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `add_time` int(11) NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  `title` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_article`
--

INSERT INTO `ims_zh_gjhdbm_article` (`article_id`, `content`, `add_time`, `is_delete`, `title`) VALUES
(1, '123123123123123', 0, 0, '2222'),
(2, '&lt;p&gt;12312333333333333333333333&lt;/p&gt;', 1, 0, '23131'),
(3, '&lt;p&gt;3333666&lt;/p&gt;', 1528680169, 0, '21');

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_assess`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_assess` (
  `id` int(11) NOT NULL,
  `activity_id` int(4) NOT NULL COMMENT '活动id',
  `content` text NOT NULL COMMENT '留言内容',
  `cerated_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `uniacid` varchar(50) NOT NULL,
  `status` int(4) NOT NULL COMMENT '1未回复,2已回复',
  `reply` text NOT NULL,
  `reply_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_attestation`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_attestation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `name` varchar(30) NOT NULL COMMENT '真是姓名/企业全称',
  `zj_name` varchar(50) NOT NULL COMMENT '证件类型/法人',
  `code` varchar(50) NOT NULL COMMENT '身份证号/工商注册码',
  `zm_img` varchar(100) NOT NULL COMMENT '正面照',
  `bm_img` varchar(100) NOT NULL COMMENT '反面照',
  `rz_time` int(11) NOT NULL COMMENT '时间',
  `sh_time` int(11) NOT NULL COMMENT '审核时间',
  `type` int(4) NOT NULL COMMENT '1个人,2企业',
  `state` int(4) NOT NULL COMMENT '状态 1申请,2通过,3拒绝',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='身份认证表';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_bmextend`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_bmextend` (
  `id` int(11) NOT NULL,
  `bm_id` int(11) NOT NULL COMMENT '报名id',
  `order_num` varchar(32) NOT NULL COMMENT '订单号',
  `activity_id` int(11) NOT NULL COMMENT '活动id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `name` varchar(30) NOT NULL COMMENT '属性',
  `value` varchar(50) NOT NULL COMMENT '属性值',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='报名信息扩展表';

--
-- 转存表中的数据 `ims_zh_gjhdbm_bmextend`
--

INSERT INTO `ims_zh_gjhdbm_bmextend` (`id`, `bm_id`, `order_num`, `activity_id`, `user_id`, `name`, `value`, `uniacid`) VALUES
(1, 0, '201806051044111286', 1, 1, '姓名', '也一样', 2),
(2, 0, '201806051044111286', 1, 1, '手机号', '13234323321', 2);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_bmlist`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_bmlist` (
  `id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL COMMENT '活动id',
  `order_num` varchar(32) NOT NULL COMMENT '订单编号',
  `hd_title` varchar(100) NOT NULL COMMENT '活动标题',
  `start_time` int(11) NOT NULL COMMENT '开始时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  `ticket_id` int(11) NOT NULL COMMENT '票id',
  `hd_place` varchar(30) NOT NULL COMMENT '活动地点',
  `tk_name` varchar(30) NOT NULL COMMENT '票卷名字',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `code` varchar(20) NOT NULL COMMENT '取票码',
  `time` int(11) NOT NULL COMMENT '报名时间',
  `state` int(4) NOT NULL COMMENT '报名状态1待审核,2待参加,3已验票,4待退票,5已退票,6已拒绝',
  `money` decimal(10,2) NOT NULL COMMENT '报名费用',
  `uniacid` int(11) NOT NULL,
  `status` int(4) NOT NULL COMMENT '订单付款状态0未付款,1已付款',
  `sh_ordernum` varchar(32) NOT NULL COMMENT '商户号',
  `hx_code` varchar(10) NOT NULL,
  `sj_num` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='报名表';

--
-- 转存表中的数据 `ims_zh_gjhdbm_bmlist`
--

INSERT INTO `ims_zh_gjhdbm_bmlist` (`id`, `activity_id`, `order_num`, `hd_title`, `start_time`, `end_time`, `ticket_id`, `hd_place`, `tk_name`, `user_id`, `code`, `time`, `state`, `money`, `uniacid`, `status`, `sh_ordernum`, `hx_code`, `sj_num`) VALUES
(1, 1, '201806051044111286', '9999', 1528165125, 1530325080, 3, '1', '999', 1, '375406026781', 1528166651, 1, '0.00', 2, 1, '', '9999999', '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_car`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_car` (
  `id` int(11) NOT NULL,
  `driving_num` varchar(100) NOT NULL,
  `car_num` varchar(100) NOT NULL,
  `secure_company` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_car`
--

INSERT INTO `ims_zh_gjhdbm_car` (`id`, `driving_num`, `car_num`, `secure_company`, `user_id`) VALUES
(7, '啊', '啊', '啊', 3),
(8, '吧', '7', '7', 3),
(9, '77', '44', '11', 4),
(10, '88', '55', '22', 4),
(11, '99', '车牌', '33', 4),
(12, '123456789', '鲁NFA365', '平安', 2),
(13, 'aa', 'aa', 'aa', 3),
(14, 'bb', 'bb', 'bb', 3);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_case`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_case` (
  `case_id` int(10) NOT NULL,
  `apply_id` int(10) NOT NULL COMMENT '案件申请者',
  `address` varchar(255) NOT NULL COMMENT '定位地址',
  `add_time` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0 待立案  6交警已接受案件  1调查处理中   2已结案 3拒绝立案 4技术鉴定中 5 下发认证书',
  `check_time` int(10) NOT NULL COMMENT '立案时间',
  `deal_id` int(10) NOT NULL COMMENT '案件处理者（交警）编号',
  `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '0 未删除 1删除',
  `acc_pic` varchar(255) NOT NULL COMMENT '事故图片',
  `case_type` tinyint(1) NOT NULL COMMENT '事故类型  1一般事故 2紧急事故',
  `address_descript` varchar(255) NOT NULL COMMENT '位置描述',
  `end_time` int(10) NOT NULL COMMENT '结案时间',
  `event_num` varchar(50) NOT NULL COMMENT '事故编号',
  `cert_info` text NOT NULL,
  `form_id` varchar(100) NOT NULL,
  `is_high_speed` tinyint(1) NOT NULL COMMENT '0 否 1是',
  `is_secure` tinyint(1) NOT NULL COMMENT '0 否 1是 交强险'
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_case`
--

INSERT INTO `ims_zh_gjhdbm_case` (`case_id`, `apply_id`, `address`, `add_time`, `status`, `check_time`, `deal_id`, `is_delete`, `acc_pic`, `case_type`, `address_descript`, `end_time`, `event_num`, `cert_info`, `form_id`, `is_high_speed`, `is_secure`) VALUES
(1, 2, '山东省烟台市莱山区枫林路24号', 1528424467, 6, 0, 0, 0, 'https://jjd.aodd.cn/attachment/../attachment/upload/4823428845012744.jpg', 1, '', 0, '', '', '', 0, 0),
(2, 2, '山东省烟台市莱山区枫林路24号', 1528424537, 0, 0, 0, 0, 'https://jjd.aodd.cn/attachment/../attachment/upload/4823428845012744.jpg', 1, '', 0, '', '', '', 0, 0),
(3, 2, '山东省烟台市莱山区枫林路24号', 1528424652, 1, 0, 0, 0, 'https://jjd.aodd.cn/attachment/../attachment/upload/4948254471312486.jpg', 1, '', 0, '', '', '', 0, 0),
(4, 2, '山东省烟台市莱山区枫林路24号', 1528424799, 2, 0, 0, 0, 'https://jjd.aodd.cn/attachment/../attachment/upload/4023898392214527.jpg', 1, '', 0, '', '', '', 0, 0),
(5, 2, '山东省烟台市莱山区枫林路24号', 1528424901, 0, 0, 0, 0, 'https://jjd.aodd.cn/attachment/../attachment/upload/7588524846433132.jpg', 1, '', 0, '', '', '', 0, 0),
(6, 2, '山东省烟台市莱山区枫林路24号', 1528426957, 0, 0, 0, 0, 'https://jjd.aodd.cn/attachment/', 1, '', 0, '', '', '', 0, 0),
(7, 2, '山东省烟台市莱山区枫林路24号', 1528428046, 2, 0, 0, 0, '../attachment/upload/8426915945979259.jpg', 1, '', 0, '', '', '', 0, 0),
(8, 2, '山东烟台', 1528433467, 0, 0, 0, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '', '', '', 0, 0),
(9, 2, '山东烟台', 1528433471, 2, 0, 0, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '', '', '', 0, 0),
(10, 2, '山东烟台', 1528433471, 0, 0, 0, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '', '', '', 0, 0),
(11, 2, '山东烟台', 1528433471, 2, 0, 0, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '', '', '', 0, 0),
(12, 2, '山东烟台', 1528433472, 0, 0, 0, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '', '', '', 0, 0),
(13, 2, '山东烟台', 1528433472, 0, 0, 0, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '', '', '', 0, 0),
(14, 2, '山东省德州市德城区新湖大街1751号', 1528438123, 0, 0, 0, 0, '../attachment/upload/0188717653286004.jpg', 2, '十字路口', 0, '', '', '', 0, 0),
(15, 2, '山东省烟台市莱山区新星北街6号', 1528440565, 0, 0, 0, 0, '../attachment/upload/2851400584925749.jpg', 2, '报警', 0, '', '', '', 0, 0),
(16, 2, '山东省烟台市莱山区枫林路24号', 1528441274, 0, 0, 0, 0, '../attachment/upload/1101402212598245.jpg;../attachment/upload/2724258812364411.jpg', 1, '', 0, '', '', '', 0, 0),
(17, 2, '山东省烟台市莱山区新星北街6号', 1528441365, 0, 0, 0, 0, '../attachment/upload/1707274741514823.jpg', 1, '', 0, '', '', '', 0, 0),
(18, 2, '山东省烟台市莱山区枫林路24号', 1528441717, 0, 0, 0, 0, '../attachment/upload/1707274741514823.jpg;../attachment/upload/5428914147545178.jpg', 1, '', 0, '', '', '', 0, 0),
(19, 3, '山东省烟台市莱山区科技创业大厦(海普路南100米)', 1528444172, 0, 0, 0, 0, '../attachment/upload/3354347284118115.png;../attachment/upload/4429722851212824.jpg;../attachment/upload/4268116624524812.png;../attachment/upload/4785825141244344.jpg;../attachment/upload/1748885214416643.png', 1, '', 0, '', '', '', 0, 0),
(20, 2, '山东省德州市德城区长河明珠(天衢中路北100米)', 1528686678, 2, 0, 2, 0, '../attachment/upload/5966489136263802.jpg', 1, '', 0, '鲁20180611-1', '', '', 0, 0),
(21, 2, '德州市德州市公安局高速公路交警支队事故处理大队', 1528699363, 2, 0, 2, 0, '../attachment/upload/6525892252586919.jpg', 1, '', 0, '还和我Hi好或或或 ', '&lt;p&gt;&lt;img src=&quot;http://jjd.aodd.cn/attachment/images/2/2018/06/mwlFh3luf5rFQR0t0z40F44ZOWU44q.jpg&quot; width=&quot;100%&quot; style=&quot;&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;img src=&quot;http://jjd.aodd.cn/attachment/images/2/2018/06/xz43lp2727R3ct333YMMRy4YpzZ3Yp.jpg&quot; width=&quot;100%&quot; style=&quot;&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', '', 0, 0),
(22, 3, '山东省烟台市莱山区枫林路24号', 1528778214, 0, 0, 0, 0, '../attachment/upload/3785948701248342.jpg', 1, '', 0, '', '', '', 0, 0),
(23, 3, '山东省烟台市莱山区枫林路24号', 1528778509, 0, 0, 0, 0, '../attachment/upload/5862767189540699.jpg', 1, '', 0, '', '', '', 0, 0),
(24, 3, '山东省烟台市莱山区枫林路24号', 1528778638, 0, 0, 0, 0, '../attachment/upload/6112567214878513.jpg', 1, '', 0, '', '', '', 0, 0),
(25, 3, '山东省烟台市莱山区枫林路24号', 1528779241, 0, 0, 0, 0, '../attachment/upload/8375817860711260.jpg', 1, '', 0, '', '', '', 0, 0),
(26, 3, '山东省烟台市莱山区枫林路24号', 1528779278, 1, 0, 2, 0, '../attachment/upload/8375817860711260.jpg', 1, '', 0, 'eee', '', '', 0, 0),
(27, 3, '山东省烟台市莱山区枫林路24号', 1528784316, 1, 0, 2, 0, '../attachment/upload/8646765528172122.jpg', 2, '', 0, '333', '', '', 0, 0),
(28, 3, '山东省烟台市莱山区枫林路24号', 1528787004, 1, 0, 2, 0, '../attachment/upload/5408087950221216.jpg', 1, '', 0, '333', '&lt;p&gt;&lt;img src=&quot;http://jjd.aodd.cn/attachment/images/2/2018/06/dkt1199eEN91q6D9eETDnT7049dK19.jpg&quot; width=&quot;100%&quot; style=&quot;&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;img src=&quot;http://jjd.aodd.cn/attachment/images/2/2018/06/xz43lp2727R3ct333YMMRy4YpzZ3Yp.jpg&quot; width=&quot;100%&quot; style=&quot;&quot;/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', '', 0, 0),
(29, 3, '科技创业大厦C座(烟台市莱山区)', 1528787539, 2, 0, 2, 0, '../attachment/upload/6352472318198471.jpg;../attachment/upload/4817315728061351.jpg;../attachment/upload/7245568442511874.png', 1, '', 0, '鲁20180611-1', '', '', 0, 0),
(30, 3, '科技创业大厦A座(迎春大街133号附1)', 1528789687, 1, 0, 2, 0, '../attachment/upload/6751651504798082.png', 1, '', 0, '还和我Hi好或或或 ', '', '', 0, 0),
(31, 3, '山东省烟台市莱山区枫林路24号', 1528789938, 1, 0, 2, 0, '../attachment/upload/0189412709752598.jpg', 1, '', 0, '1111111111111', '', '', 0, 0),
(32, 3, '科技创业大厦C座(烟台市莱山区)', 1528794923, 1, 0, 2, 0, '../attachment/upload/8592085879271542.png', 1, '', 0, '333', '', '', 0, 0),
(33, 3, '悦海中心A座(悦海中心永旺西南50米)', 1528855388, 1, 0, 2, 0, '../attachment/upload/9350850228158686.png', 1, '', 0, '333', '', '', 0, 0),
(34, 1, '山东烟台', 1528855851, 1, 0, 2, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '还和我Hi好或或或 ', '', '', 0, 0),
(35, 1, '山东烟台', 1528855914, 1, 0, 2, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '还和我Hi好或或或 ', '', '', 0, 0),
(36, 1, '山东烟台', 1528855937, 1, 0, 3, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '还和我Hi好或或或 ', '', '', 0, 0),
(37, 3, '悦海中心A座(悦海中心永旺西南50米)', 1528856675, 1, 0, 2, 0, '../attachment/upload/2145660811659581.png', 1, '', 0, '还和我Hi好或或或 ', '', '', 0, 0),
(38, 3, '悦海中心A座(悦海中心永旺西南50米)', 1528856849, 1, 0, 3, 0, '../attachment/upload/8301240488665835.png', 1, '', 0, '鲁20180611-1', '', '1528856848797', 0, 0),
(39, 3, '悦海中心A座(悦海中心永旺西南50米)', 1528859357, 1, 0, 3, 0, '../attachment/upload/5185849425394423.jpg', 1, '', 0, '这是事故编号测试', '', '1528859357467', 0, 0),
(40, 3, '悦海中心A座(悦海中心永旺西南50米)', 1528859873, 1, 0, 3, 0, '../attachment/upload/8869126551758538.png', 1, '', 0, '编号测试', '', '1528859873018', 0, 0),
(41, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528860471, 1, 0, 3, 0, '../attachment/upload/4864510760248481.jpg', 1, '', 0, '999', '', '9de3c09142762a9417b7ced26d50084c', 0, 0),
(42, 4, '烟台市中国农业银行(莱山支行)', 1528860623, 1, 0, 2, 0, '../attachment/upload/6108267095115814.jpg', 1, '', 0, '还和我Hi好或或或 2', '', 'bbe59071c58a64d1c9e83a6654622cc3', 0, 0),
(43, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528860785, 1, 0, 2, 0, '../attachment/upload/7724552105783862.jpg', 1, '', 0, '99999', '', '2583ee8d135acae26d37c33ff1b9c5de', 0, 0),
(44, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528861297, 1, 0, 2, 0, '../attachment/upload/5935586828871129.jpg', 1, '', 0, 's444444444444444444', '', 'c389cd33d5e663d3d39b178d634b4be8', 0, 0),
(45, 3, '中国银行莱山支行(莱山区迎春大街139)', 1528861470, 1, 0, 3, 0, '../attachment/upload/5485517189251464.jpg', 1, '', 0, '3333', '', '1528861470082', 0, 0),
(46, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528861587, 1, 0, 3, 0, '../attachment/upload/8518570762588917.jpg', 1, '', 0, '555555', '', '6675267b25612d378bf2731b33ba34e4', 0, 0),
(47, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528861653, 1, 0, 3, 0, '../attachment/upload/8845169962631158.jpg', 1, '', 0, '33', '', '94510be15887c57ce7583ae89186f007', 0, 0),
(48, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528861895, 2, 0, 2, 0, '../attachment/upload/8858214871186138.jpg', 1, '', 0, '6666666', '', '6cb1d1a3d4509e60a13457634f1270ab', 0, 0),
(49, 3, '悦海中心A座(悦海中心永旺西南50米)', 1528861975, 1, 0, 2, 0, '../attachment/upload/5968186158919582.png', 1, '', 0, '333333', '', '1528861974839', 0, 0),
(50, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528862135, 2, 0, 2, 0, '../attachment/upload/4228066151828253.jpg', 1, '', 0, '44444', '', 'a02671cd4f7120924bb999bc6af6ae6a', 0, 0),
(51, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528862944, 4, 0, 3, 0, '../attachment/upload/6484291285098893.jpg', 1, '', 1528873370, '44444444444', '<p>666666</p>', '29689442ce0e20fc0dabff41256e74dc', 0, 0),
(52, 4, '烟台市科技创业大厦C座(烟台市莱山区)', 1528862967, 0, 0, 0, 0, '../attachment/upload/2195258286159896.jpg;../attachment/upload/6215852178398511.jpg', 1, '', 0, '', '', '29689442ce0e20fc0dabff41256e74dc', 0, 0),
(53, 4, '山东省烟台市莱山区光华路16', 1528871349, 5, 0, 2, 0, '../attachment/upload/5815217142780814.jpg;../attachment/upload/8125288151378273.jpg;../attachment/upload/1257376387703118.jpg;../attachment/upload/5858116799721321.jpg;../attachment/upload/7572601318951882.jpg', 1, '这里是山东烟台莱山景区门票的价格买入为均', 1528873156, '这是事件编号', '<p>22222222</p>', 'ded50cfb0875fce52dfc662ebe5492ee', 0, 0),
(54, 3, '悦海中心A座(悦海中心永旺西南50米)', 1528871757, 2, 0, 3, 0, '../attachment/upload/2580447175148192.png', 1, '', 1528871948, '3333', '<p>结案了</p>', '1528871757020', 0, 0),
(55, -1, '山东烟台', 1528876024, 5, 0, 2, 0, '', 1, '', 1528876128, '222222', '<p>33333333333333333</p>', '', 0, 0),
(56, -1, '333333333', 1528876257, 0, 0, 0, 0, '', 2, '', 0, '', '', '', 0, 0),
(57, -1, '2222', 1528876927, 2, 0, 2, 0, '', 1, '', 1528959241, '33', '<p>结案：</p><p><img src="http://jjd.aodd.cn/attachment/images/2/2018/06/dkt1199eEN91q6D9eETDnT7049dK19.jpg" width="100%" style=""/></p><p><img src="http://jjd.aodd.cn/attachment/images/2/2018/06/xz43lp2727R3ct333YMMRy4YpzZ3Yp.jpg" width="100%" style=""/></p><p><br/></p>', '', 0, 0),
(58, 0, '中国银行莱山支行(莱山区迎春大街139)', 1528878086, 0, 0, 0, 0, '../attachment/upload/8675816608805527.png', 1, '', 0, '', '', '1528878086557', 0, 0),
(59, 0, '山东省烟台市', 1528879322, 0, 0, 0, 0, '../attachment/upload/4162580359792810.jpg', 1, '', 0, '', '', 'the formId is a mock one', 0, 0),
(60, 0, '山东省烟台市莱山区新星北街6号', 1528879409, 0, 0, 0, 0, '../attachment/upload/8788591307284071.jpg', 2, '', 0, '', '&lt;p&gt;&lt;img src=&quot;http://img.baidu.com/hi/jx2/j_0025.gif&quot;/&gt;33333&lt;/p&gt;', 'the formId is a mock one', 0, 0),
(61, 3, '科技创业大厦A座(迎春大街133号附1)', 1528939257, 4, 0, 2, 0, '../attachment/upload/5521972309833824.png;../attachment/upload/8631215892184319.jpg', 1, '科技创业大厦A座(迎春大街133号附1)', 1528939767, '鲁F12333-h123', '&lt;p&gt;&lt;img src=&quot;http://jjd.aodd.cn/attachment/images/2/2018/06/dkt1199eEN91q6D9eETDnT7049dK19.jpg&quot; width=&quot;100%&quot; alt=&quot;timg.jpg&quot;/&gt;11&lt;/p&gt;', '1528939256842', 0, 0),
(62, 3, '悦海中心A座(悦海中心永旺西南50米)', 1528939866, 2, 0, 2, 0, '../attachment/upload/5158979825584183.jpg', 1, '', 1528959179, '案件编号：121232', '<p>999999999999999999<img src="http://jjd.aodd.cn/attachment/images/2/2018/06/dkt1199eEN91q6D9eETDnT7049dK19.jpg" width="100%" alt="timg.jpg"/></p>', '1528939866691', 0, 0),
(63, -1, '444', 1528954892, 0, 0, 0, 0, '', 1, '', 0, '', '', '', 0, 0),
(64, -1, '测试数据', 1528957656, 0, 0, 0, 0, '', 2, '', 0, '', '', '', 0, 0),
(65, 2, '德州市德州市公安局高速公路交警支队事故处理大队', 1529483666, 0, 0, 0, 0, '../attachment/upload/2946379821518854.jpg', 1, '嗯哼', 0, '', '', 'edba43c682c9ffaf1a39cd0ec9ba68af', 0, 0),
(66, 7, '山东烟台', 1530167106, 0, 0, 0, 0, '/attachment/upload/0343851991829483.jpg;/attachment/upload/0343851991829483.jpg', 1, '11111111111111', 0, '', '', '', 0, 0),
(67, 3, '烟台市科技创业大厦C座(烟台市莱山区)', 1530233353, 1, 0, 2, 0, '[{&quot;type&quot;:&quot;1&quot;,&quot;url&quot;:&quot;../attachment/upload/6330953294132280.jpg&quot;},{&quot;type&quot;:&quot;2&quot;,&quot;url&quot;:&quot;../attachment/upload/6736351120133422.jpg&quot;},{&quot;type&quot;:&quot;3&quot;,&quot;url&quot;:', 1, '', 0, '123', '', '5b16ca3750d9cd7e71c4545a362355bf', 1, 1),
(68, 3, '山东省烟台市', 1530267779, 0, 0, 0, 0, '[{&quot;type&quot;:&quot;1&quot;,&quot;url&quot;:&quot;../attachment/upload/6735274285815502.jpg&quot;},{&quot;type&quot;:&quot;2&quot;,&quot;url&quot;:&quot;../attachment/upload/5102976340379862.jpg&quot;},{&quot;type&quot;:&quot;3&quot;,&quot;url&quot;:', 1, '', 0, '', '', 'the formId is a mock one', 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_city`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_city` (
  `id` int(11) NOT NULL,
  `cityname` varchar(50) NOT NULL COMMENT '名称',
  `time` int(11) NOT NULL COMMENT '时间',
  `sort` int(11) NOT NULL COMMENT '排序',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='城市表';

--
-- 转存表中的数据 `ims_zh_gjhdbm_city`
--

INSERT INTO `ims_zh_gjhdbm_city` (`id`, `cityname`, `time`, `sort`, `uniacid`) VALUES
(1, '烟台市', 1528165471, 0, 2),
(2, '德州市', 1528686579, 0, 2);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_commission_withdrawal`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_commission_withdrawal` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `state` int(11) NOT NULL COMMENT '1.审核中2.通过3.拒绝',
  `time` int(11) NOT NULL COMMENT '申请时间',
  `sh_time` int(11) NOT NULL COMMENT '审核时间',
  `uniacid` int(11) NOT NULL,
  `user_name` varchar(20) NOT NULL COMMENT '姓名',
  `account` varchar(100) NOT NULL COMMENT '账号',
  `tx_cost` decimal(10,2) NOT NULL COMMENT '提现金额',
  `sj_cost` decimal(10,2) NOT NULL COMMENT '实际到账金额',
  `note` varchar(50) NOT NULL DEFAULT '提现'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='佣金提现';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_contact`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_contact` (
  `contact_id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `contact_name` varchar(200) NOT NULL,
  `contact_tel` varchar(200) NOT NULL,
  `plate_num` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_contact`
--

INSERT INTO `ims_zh_gjhdbm_contact` (`contact_id`, `case_id`, `contact_name`, `contact_tel`, `plate_num`, `type`) VALUES
(1, 22, '[&quot;A&quot;,&quot;B&quot;,&quot;C&quot;]', '[&quot;11&quot;,&quot;22&quot;,&quot;33&quot;]', '[&quot;11&quot;,&quot;22&quot;,&quot;33&quot;]', 'A'),
(2, 23, '[&quot;A&quot;,&quot;B&quot;,&quot;C&quot;]', '[&quot;11&quot;,&quot;22&quot;,&quot;33&quot;]', '[&quot;11&quot;,&quot;22&quot;,&quot;33&quot;]', 'A'),
(3, 24, '[&quot;A&quot;,&quot;B&quot;,&quot;c&quot;]', '[&quot;11&quot;,&quot;22&quot;,&quot;33&quot;]', '[&quot;11&quot;,&quot;22&quot;,&quot;33&quot;]', 'A'),
(4, 27, 'aaaaaaaa', 'aaaaaaa', 'aaaaaaa', '[&quot'),
(5, 27, 'bbbbbbb', 'bbbbbb', 'bbbbbb', 'A&quot'),
(6, 28, 'aa', '187', '鲁F1234', '[&quot'),
(7, 28, 'bb', '287', '鲁F2345', 'A&quot'),
(8, 28, 'cc', '387', '鲁F3456', ',&quot'),
(9, 29, '单', '187', '鲁F1', '[&quot'),
(10, 29, '胡', '287', '鲁F2', 'A&quot'),
(11, 29, '衣', '387', '鲁F3', ',&quot'),
(12, 29, '张', '487', '鲁F4', 'B&quot'),
(13, 30, '事故a方', '111', '111', 'A'),
(14, 30, '事故b方', '222', '222', 'B'),
(15, 31, '茄子', '234', '23456', 'A'),
(16, 32, '123', '123', '123', 'A'),
(17, 33, '阿', '1', '1', 'A'),
(18, 34, '1,2,3', 'a,b,c', 'e,r,t', 'A,B,C'),
(19, 35, '1,2,3', 'a,b,c', 'e,r,t', 'A,B,C'),
(20, 36, '1,2,3', 'a,b,c', 'e,r,t', 'A,B,C'),
(21, 37, '2', '2', '2', 'A'),
(22, 38, '3', '3', '3', 'A'),
(23, 39, '是', '1', '去', 'A'),
(24, 41, 's', '7', '7', 'B'),
(25, 41, '11', '15558071536', '车牌', 'A'),
(26, 42, '123', '15558071536', '111', 'A'),
(27, 43, '36', '12', '11', 'A'),
(28, 44, '125', '111', '111', 'A'),
(29, 45, '1', '1', '1', 'A'),
(30, 46, '36', '2', '1', 'A'),
(31, 47, '11', '111', '11', 'A'),
(32, 48, '1558', '1222', '11', 'A'),
(33, 49, '阿', '1', '1', 'A'),
(34, 50, '588', '155', '111', 'A'),
(35, 51, '1', '1', '1', 'A'),
(36, 52, '7088', '158', '145', 'A'),
(37, 53, '小飞', '15558071536', '鲁F1234567890', 'A'),
(38, 54, '88888', '88888', '88888', 'A'),
(39, 55, '111', '15558072016', '鲁1234567890', 'A'),
(40, 55, '2222', '15558071536', '鲁9876543210', 'B'),
(41, 56, '3', '3', '3', 'A'),
(42, 56, '', '', '', 'B'),
(43, 57, '2', '2', '2', 'A'),
(44, 58, '1', '1', '1', 'A'),
(45, 59, '1', '1', '1', 'A'),
(46, 60, '1', '1', '1', 'A'),
(47, 61, '11111', '11111', '11111', 'A'),
(48, 62, '2222222', '22222222', '2222222', 'A'),
(49, 62, '33', '33', '33', ''),
(50, 63, 'w', 'w', 'w', 'A'),
(51, 63, '55', '55', '55', 'B'),
(52, 63, '1', '1', '1', 'C'),
(53, 63, '2', '2', '2', 'D'),
(54, 63, '44', '44', '44', ''),
(55, 64, '1', '1', '1', 'A'),
(56, 64, '2', '2', '2', 'B'),
(58, 64, '5', '5', '5', 'C'),
(59, 64, '6', '6', '6', 'D'),
(60, 65, '我', '18605344485', '鲁NFA333', 'A'),
(61, 65, '他', '18605344486', '鲁NFA555', 'B'),
(62, 66, '1,2,3', 'a,b,c', 'e,r,t', 'A,B,C'),
(63, 67, '单正晗', '18766593760', 'aa', 'A'),
(64, 67, '单正晗', '18766564720', 'bb', 'B'),
(65, 68, '单正晗', '18766593760', 'bb', '甲');

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_deal`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_deal` (
  `deal_id` int(11) NOT NULL,
  `deal_name` varchar(50) NOT NULL COMMENT '名字',
  `deal_tel` varchar(50) NOT NULL COMMENT '电话',
  `add_time` int(10) NOT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '0 未删除 1删除'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_deal`
--

INSERT INTO `ims_zh_gjhdbm_deal` (`deal_id`, `deal_name`, `deal_tel`, `add_time`, `is_delete`) VALUES
(1, 'test3', '15678905423', 1528685601, 1),
(2, '张三', '18605348888', 1528685676, 0),
(3, '李四', '18505346666', 1528685740, 0);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_distribution`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_distribution` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_tel` varchar(20) NOT NULL,
  `state` int(11) NOT NULL COMMENT '1.审核中2.通过3.拒绝',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分销申请';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_earnings`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_earnings` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL COMMENT '订单ID',
  `user_id` int(11) NOT NULL,
  `son_id` int(11) NOT NULL COMMENT '下线',
  `money` decimal(10,2) NOT NULL,
  `time` int(11) NOT NULL,
  `note` varchar(50) NOT NULL COMMENT '备注',
  `state` int(4) NOT NULL COMMENT '佣金状态,1冻结,2有效,3无效',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='佣金收益表';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_enroll`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_enroll` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL COMMENT '名称',
  `time` int(11) NOT NULL COMMENT '时间',
  `uniacid` int(11) NOT NULL COMMENT '小程序ID',
  `sort` int(11) NOT NULL COMMENT '排序',
  `status` int(4) NOT NULL DEFAULT '1' COMMENT '状态 1启用,2禁用'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报名信息表';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_follow`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_follow` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `activity_id` int(11) NOT NULL COMMENT '活动id',
  `time` int(11) NOT NULL COMMENT '关注时间',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='关注表';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_fxset`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_fxset` (
  `id` int(11) NOT NULL,
  `fx_details` text NOT NULL COMMENT '分销商申请协议',
  `tx_details` text NOT NULL COMMENT '佣金提现协议',
  `is_fx` int(11) NOT NULL COMMENT '1.开启分销审核2.不开启',
  `is_ej` int(11) NOT NULL COMMENT '是否开启二级分销1.是2.否',
  `tx_rate` int(11) NOT NULL COMMENT '提现手续费',
  `commission` varchar(10) NOT NULL COMMENT '一级佣金',
  `commission2` varchar(10) NOT NULL COMMENT '二级佣金',
  `tx_money` int(11) NOT NULL COMMENT '提现门槛',
  `img` varchar(100) NOT NULL COMMENT '分销中心图片',
  `img2` varchar(100) NOT NULL COMMENT '申请分销图片',
  `uniacid` int(11) NOT NULL,
  `is_open` int(11) NOT NULL DEFAULT '1' COMMENT '1.开启2关闭',
  `instructions` text NOT NULL COMMENT '分销商说明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_fxuser`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_fxuser` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '一级分销',
  `fx_user` int(11) NOT NULL COMMENT '二级分销',
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_hx`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_hx` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `hx_id` int(11) NOT NULL COMMENT '核销人id',
  `time` int(11) NOT NULL COMMENT '时间',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='核销表';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_menu`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL COMMENT '菜单名称',
  `src` varchar(100) NOT NULL COMMENT '跳转路径',
  `icon1` varchar(100) NOT NULL COMMENT '选中图标',
  `icon2` varchar(100) NOT NULL COMMENT '未选中图标',
  `sort` int(11) NOT NULL COMMENT '排序',
  `time` int(11) NOT NULL COMMENT '时间',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_message`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_message` (
  `id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `deal_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `belong_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_message`
--

INSERT INTO `ims_zh_gjhdbm_message` (`id`, `case_id`, `deal_id`, `message`, `belong_id`) VALUES
(1, 1, 1, '这是message 交警下发饿', 0),
(2, 1, 0, '', 3),
(3, 1, 0, '', 2),
(4, 1, 0, '', 3),
(5, 1, 0, '', 2),
(6, 1, 1, '这是我的警号', 3),
(7, 1, 1, '这是我的警号', 2),
(8, 1, 1, '这是我的警号', 3),
(9, 1, 1, '这是我的警号', 2);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_nav`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_nav` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL COMMENT '名称',
  `logo` varchar(200) NOT NULL COMMENT '图标',
  `status` int(11) NOT NULL COMMENT '1.开启  2.关闭',
  `src` varchar(100) NOT NULL COMMENT '内部链接',
  `orderby` int(11) NOT NULL COMMENT '排序',
  `xcx_name` varchar(20) NOT NULL COMMENT '小程序名称',
  `appid` varchar(20) NOT NULL COMMENT 'APPID',
  `uniacid` int(11) NOT NULL COMMENT '小程序id',
  `wb_src` varchar(300) NOT NULL COMMENT '外部链接',
  `state` int(4) NOT NULL DEFAULT '1' COMMENT '1内部，2外部,3跳转'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_nav`
--

INSERT INTO `ims_zh_gjhdbm_nav` (`id`, `title`, `logo`, `status`, `src`, `orderby`, `xcx_name`, `appid`, `uniacid`, `wb_src`, `state`) VALUES
(2, '111', 'images/2/2018/06/M4q0PQjPXYG10kg2Szp60ZjsZjY442.png', 1, '', 0, '', '', 2, '', 1);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_pic`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_pic` (
  `pic_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1 侧前方 2侧后方 3碰撞部位',
  `url` varchar(200) NOT NULL,
  `case_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_pic`
--

INSERT INTO `ims_zh_gjhdbm_pic` (`pic_id`, `type`, `url`, `case_id`) VALUES
(1, 1, '../attachment/upload/6735274285815502.jpg', 41),
(2, 2, '../attachment/upload/5102976340379862.jpg', 41),
(3, 3, '../attachment/upload/4236733137650648.jpg', 41);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_qiniu`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_qiniu` (
  `id` int(11) NOT NULL,
  `accesskey` varchar(50) NOT NULL,
  `secretkey` varchar(50) NOT NULL,
  `bucket` varchar(30) NOT NULL,
  `url` varchar(50) NOT NULL,
  `uniacid` int(11) NOT NULL,
  `is_open` int(4) NOT NULL DEFAULT '2' COMMENT '是否开启,1开启,2不开启'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='七牛配置表';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_sponsor`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_sponsor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `name` varchar(30) NOT NULL COMMENT '主办方名称',
  `logo` varchar(100) NOT NULL COMMENT '主办方头像',
  `tel` varchar(20) NOT NULL COMMENT '手机',
  `details` text NOT NULL COMMENT '简介',
  `time` int(11) NOT NULL COMMENT '时间',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主办方表';

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_system`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_system` (
  `id` int(11) NOT NULL,
  `appid` varchar(100) NOT NULL COMMENT 'appid',
  `appsecret` varchar(200) NOT NULL COMMENT 'appsecret',
  `mchid` varchar(20) NOT NULL COMMENT '商户号',
  `wxkey` varchar(100) NOT NULL COMMENT '商户秘钥',
  `uniacid` varchar(50) NOT NULL,
  `url_name` varchar(20) NOT NULL COMMENT '网址名称',
  `details` text NOT NULL COMMENT '关于我们',
  `url_logo` varchar(100) NOT NULL COMMENT '网址logo',
  `bq_name` varchar(50) NOT NULL COMMENT '版权名称',
  `link_name` varchar(30) NOT NULL COMMENT '网站名称',
  `link_logo` varchar(100) NOT NULL COMMENT '网站logo',
  `support` varchar(20) NOT NULL COMMENT '技术支持',
  `bq_logo` varchar(100) NOT NULL,
  `color` varchar(20) NOT NULL,
  `tz_appid` varchar(30) NOT NULL,
  `tz_name` varchar(30) NOT NULL,
  `pt_name` varchar(30) NOT NULL COMMENT '平台名称',
  `tel` varchar(20) NOT NULL COMMENT '平台电话',
  `rz_xuz` text NOT NULL COMMENT '入驻须知',
  `ft_xuz` text NOT NULL COMMENT '发帖须知',
  `total_num` int(11) NOT NULL COMMENT '访问量',
  `appkey` varchar(50) NOT NULL COMMENT '短信appkey',
  `tpl_id` varchar(20) NOT NULL COMMENT '短信模板id',
  `rc_content` text NOT NULL COMMENT '认筹规则',
  `apiclient_cert` text NOT NULL COMMENT '证书',
  `apiclient_key` text NOT NULL COMMENT '证书',
  `zd_money` decimal(10,2) NOT NULL COMMENT '最低提现金额',
  `tx_sxf` varchar(10) NOT NULL COMMENT '提现手续费',
  `rc_tk` text NOT NULL COMMENT '认筹条款',
  `tid1` varchar(50) NOT NULL COMMENT '报名成功通知模板id',
  `tid2` varchar(50) NOT NULL COMMENT '发布活动通知模板id',
  `tid3` varchar(50) NOT NULL COMMENT '认证通知模板id',
  `tx_notice` text NOT NULL COMMENT '提现须知',
  `hdsh_open` int(4) NOT NULL DEFAULT '1' COMMENT '活动审核类型 1手动,2自动',
  `bmsh_open` int(4) NOT NULL DEFAULT '1' COMMENT '报名审核类型 1手动,2自动',
  `style` int(4) NOT NULL DEFAULT '1' COMMENT '风格设置,1左右布局,2上下布局',
  `fwf_notice` text NOT NULL COMMENT '服务费须知',
  `tx_mode` int(4) NOT NULL COMMENT '1手动打款,2自动打款',
  `is_sfrz` int(4) NOT NULL DEFAULT '1' COMMENT '身份认证1开通,2不开通',
  `client_ip` varchar(30) NOT NULL,
  `rz_notice` text NOT NULL,
  `fb_notice` text NOT NULL,
  `is_dxyz` int(4) NOT NULL DEFAULT '1' COMMENT '是否开启大鱼短信',
  `map_key` varchar(50) NOT NULL,
  `ly_num` int(11) NOT NULL DEFAULT '1',
  `city_open` int(4) NOT NULL DEFAULT '2',
  `sign_name` varchar(200) NOT NULL COMMENT '签名',
  `sms_secret` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_system`
--

INSERT INTO `ims_zh_gjhdbm_system` (`id`, `appid`, `appsecret`, `mchid`, `wxkey`, `uniacid`, `url_name`, `details`, `url_logo`, `bq_name`, `link_name`, `link_logo`, `support`, `bq_logo`, `color`, `tz_appid`, `tz_name`, `pt_name`, `tel`, `rz_xuz`, `ft_xuz`, `total_num`, `appkey`, `tpl_id`, `rc_content`, `apiclient_cert`, `apiclient_key`, `zd_money`, `tx_sxf`, `rc_tk`, `tid1`, `tid2`, `tid3`, `tx_notice`, `hdsh_open`, `bmsh_open`, `style`, `fwf_notice`, `tx_mode`, `is_sfrz`, `client_ip`, `rz_notice`, `fb_notice`, `is_dxyz`, `map_key`, `ly_num`, `city_open`, `sign_name`, `sms_secret`) VALUES
(1, 'wxfb2115622142e037', 'a15ead880b4ca951d9db14b0b30af6ff', '', '', '2', '', '', '', '技术支持：山东同联科技发展有限公司', '事故管理系统', '', '', '', '#ED414A', '', '', '德州事故处理', '18605344485', '', '', 0, '23579524', 'SMS_35800057', '', '', '', '0.00', '', '', '', 'zydA-RFZMTtWptUR04nGxYRuQWOhjOTPjP4ZK25eOMg', '', '', 1, 0, 1, '', 0, 2, '', '', '', 1, 'OQ4BZ-RHLW3-4RB34-YLIBH-F47RJ-42BDJ', 1, 2, '网站注册', '2c2bd57b9ab50c6d3c15266ae9b826cb');

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_tel`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_tel` (
  `id` int(11) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_tel`
--

INSERT INTO `ims_zh_gjhdbm_tel` (`id`, `tel`, `user_id`) VALUES
(9, '0000', 3),
(10, '7', 3),
(11, '15558071536', 4),
(12, '181253595818', 4),
(13, '18605344485', 2),
(14, '18766593760', 3),
(15, '18766564720', 3);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_ticket`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_ticket` (
  `id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL COMMENT '活动id',
  `tk_name` varchar(30) NOT NULL COMMENT '票卷名字',
  `type` int(4) NOT NULL COMMENT '类型1免费,2收费',
  `money` decimal(10,2) NOT NULL COMMENT '价格',
  `total_num` int(11) NOT NULL COMMENT '总张数',
  `limit_num` int(11) NOT NULL COMMENT '限制张数',
  `bm_check` int(4) NOT NULL DEFAULT '2' COMMENT '是否需要审核,1是,2否',
  `yg_num` int(11) NOT NULL COMMENT '已购数量',
  `uniacid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='票卷表';

--
-- 转存表中的数据 `ims_zh_gjhdbm_ticket`
--

INSERT INTO `ims_zh_gjhdbm_ticket` (`id`, `activity_id`, `tk_name`, `type`, `money`, `total_num`, `limit_num`, `bm_check`, `yg_num`, `uniacid`) VALUES
(3, 1, '999', 1, '0.00', 999, 999, 1, 1, 2),
(4, 2, '票券1', 1, '0.00', 1, 1, 2, 0, 2),
(5, 3, '票券1', 1, '0.00', 99, 9, 2, 0, 2);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_type`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(30) NOT NULL COMMENT '分类名称',
  `img` varchar(100) NOT NULL COMMENT '图标',
  `num` int(11) NOT NULL COMMENT '排序',
  `state` int(4) NOT NULL COMMENT '1开启,2关闭',
  `time` int(11) NOT NULL COMMENT '时间',
  `uniacid` int(11) NOT NULL COMMENT '小程序ID'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='分类表';

--
-- 转存表中的数据 `ims_zh_gjhdbm_type`
--

INSERT INTO `ims_zh_gjhdbm_type` (`id`, `type_name`, `img`, `num`, `state`, `time`, `uniacid`) VALUES
(1, '严重事故', '', 1, 1, 1528192211, 2),
(2, '一般事故', '', 0, 1, 1528192233, 2);

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_user`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `join_time` int(11) NOT NULL,
  `img` varchar(500) NOT NULL,
  `openid` varchar(200) NOT NULL,
  `uniacid` varchar(50) NOT NULL,
  `link_tel` varchar(20) NOT NULL COMMENT '交警电话',
  `idcard` varchar(50) NOT NULL COMMENT '身份证号',
  `user_name` varchar(50) NOT NULL COMMENT '姓名',
  `is_police` tinyint(1) NOT NULL COMMENT '1是交警 0不是',
  `police_num` varchar(100) NOT NULL COMMENT '警号'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `ims_zh_gjhdbm_user`
--

INSERT INTO `ims_zh_gjhdbm_user` (`id`, `name`, `join_time`, `img`, `openid`, `uniacid`, `link_tel`, `idcard`, `user_name`, `is_police`, `police_num`) VALUES
(2, '奥轩', 1528333667, 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqM0zicd2icS53y0ls5SEDXiaz8gII4JzvvvDfSQoKpbsDNAKTLU3ulXMZ9Fib8d5H0KgVmx3ibWTTWhEw/132', 'o_5nD5EWg_MRTD1jHdTMwRon8aqk', '2', '', '371428198706072532', '杨月振', 0, ''),
(3, '月影', 1528423220, 'https://wx.qlogo.cn/mmopen/vi_32/DYAIOgq83equZXQgcI2efAUqoTaH81tllHMa20efbhuqbjeQy2ShQ9Gp3he7ujVj8M6vMFVicXicQ9nvgrryKFhw/132', 'o_5nD5O16dmxX-O69H3s0zGReVYE', '2', '', '370682198911135932', '单正晗', 1, ''),
(4, '妮妮', 1528426890, 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLRPRkZJKNL6D6GQmDC5icDwkzU1l2uLfku0H64Riawc00WmiauxD6xliaDE5TJic3B1raVQJpxLUPiaouw/132', 'o_5nD5FyLocZB_foMin6oe17hM7A', '2', '', '37068211348022', '衣妮妮1', 0, ''),
(5, '风猫', 1528858516, 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI6nskCoqMibz1CxYYEg5SvWVhwQdqEKuMNq3B3P1nnW8S4z4dkPica0wcpMnhut0l6Jo8kcadZu8icw/132', 'o_5nD5KUCw-AXhtJkev6uNaGxtYQ', '2', '155558021324', '', '111', 0, '这是我的警号'),
(6, '风猫', 1528860320, 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIJTEGJia8YNjIRsQ8p0TJ5EIaW3cMIckqXOHWJXTqibO1Lwe0rQH1TvRNyzohNPNicnOe4WZHd1NsKA/132', 'o_5nD5AjF-LQr2m8XsuHs_R1b2ds', '2', '', '', '', 0, ''),
(7, '巴掌脸攻击', 1528870714, 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTImzIXARHcUMOxWicgCOTVTn6gy0tmWdAcAw7p22K052ULOiaibKqOuuh49f4gGuDO2OAzGGs5UiaIwCQ/132', 'o_5nD5DGgbZXqlN_ALVELIsZ65XI', '2', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `ims_zh_gjhdbm_withdrawal`
--

CREATE TABLE IF NOT EXISTS `ims_zh_gjhdbm_withdrawal` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL COMMENT '开户姓名',
  `account` varchar(100) NOT NULL COMMENT '账号',
  `time` int(11) NOT NULL COMMENT '申请时间',
  `sh_time` int(11) NOT NULL COMMENT '审核时间',
  `state` int(11) NOT NULL COMMENT '1.待审核 2.通过  3.拒绝',
  `tx_cost` decimal(10,2) NOT NULL COMMENT '提现金额',
  `sj_cost` decimal(10,2) NOT NULL COMMENT '实际金额',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `uniacid` int(11) NOT NULL,
  `type` int(4) NOT NULL COMMENT '提现方式',
  `is_delete` int(4) NOT NULL DEFAULT '1' COMMENT '1显示,2不显示'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提现表';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ims_account`
--
ALTER TABLE `ims_account`
  ADD PRIMARY KEY (`acid`),
  ADD KEY `idx_uniacid` (`uniacid`);

--
-- Indexes for table `ims_account_wechats`
--
ALTER TABLE `ims_account_wechats`
  ADD PRIMARY KEY (`acid`),
  ADD KEY `idx_key` (`key`);

--
-- Indexes for table `ims_account_wxapp`
--
ALTER TABLE `ims_account_wxapp`
  ADD PRIMARY KEY (`acid`),
  ADD KEY `uniacid` (`uniacid`);

--
-- Indexes for table `ims_article_category`
--
ALTER TABLE `ims_article_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `ims_article_news`
--
ALTER TABLE `ims_article_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`),
  ADD KEY `cateid` (`cateid`);

--
-- Indexes for table `ims_article_notice`
--
ALTER TABLE `ims_article_notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`),
  ADD KEY `cateid` (`cateid`);

--
-- Indexes for table `ims_article_unread_notice`
--
ALTER TABLE `ims_article_unread_notice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `notice_id` (`notice_id`);

--
-- Indexes for table `ims_basic_reply`
--
ALTER TABLE `ims_basic_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_business`
--
ALTER TABLE `ims_business`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_lat_lng` (`lng`,`lat`);

--
-- Indexes for table `ims_core_attachment`
--
ALTER TABLE `ims_core_attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_core_cache`
--
ALTER TABLE `ims_core_cache`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `ims_core_cron`
--
ALTER TABLE `ims_core_cron`
  ADD PRIMARY KEY (`id`),
  ADD KEY `createtime` (`createtime`),
  ADD KEY `nextruntime` (`nextruntime`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `cloudid` (`cloudid`);

--
-- Indexes for table `ims_core_cron_record`
--
ALTER TABLE `ims_core_cron_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `tid` (`tid`),
  ADD KEY `module` (`module`);

--
-- Indexes for table `ims_core_menu`
--
ALTER TABLE `ims_core_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_core_paylog`
--
ALTER TABLE `ims_core_paylog`
  ADD PRIMARY KEY (`plid`),
  ADD KEY `idx_openid` (`openid`),
  ADD KEY `idx_tid` (`tid`),
  ADD KEY `idx_uniacid` (`uniacid`),
  ADD KEY `uniontid` (`uniontid`);

--
-- Indexes for table `ims_core_performance`
--
ALTER TABLE `ims_core_performance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_core_queue`
--
ALTER TABLE `ims_core_queue`
  ADD PRIMARY KEY (`qid`),
  ADD KEY `uniacid` (`uniacid`,`acid`),
  ADD KEY `module` (`module`),
  ADD KEY `dateline` (`dateline`);

--
-- Indexes for table `ims_core_refundlog`
--
ALTER TABLE `ims_core_refundlog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refund_uniontid` (`refund_uniontid`),
  ADD KEY `uniontid` (`uniontid`);

--
-- Indexes for table `ims_core_resource`
--
ALTER TABLE `ims_core_resource`
  ADD PRIMARY KEY (`mid`),
  ADD KEY `acid` (`uniacid`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `ims_core_sendsms_log`
--
ALTER TABLE `ims_core_sendsms_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_core_sessions`
--
ALTER TABLE `ims_core_sessions`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `ims_core_settings`
--
ALTER TABLE `ims_core_settings`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `ims_coupon_location`
--
ALTER TABLE `ims_coupon_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`,`acid`);

--
-- Indexes for table `ims_cover_reply`
--
ALTER TABLE `ims_cover_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_custom_reply`
--
ALTER TABLE `ims_custom_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_images_reply`
--
ALTER TABLE `ims_images_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_mc_cash_record`
--
ALTER TABLE `ims_mc_cash_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `ims_mc_chats_record`
--
ALTER TABLE `ims_mc_chats_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`,`acid`),
  ADD KEY `openid` (`openid`),
  ADD KEY `createtime` (`createtime`);

--
-- Indexes for table `ims_mc_credits_recharge`
--
ALTER TABLE `ims_mc_credits_recharge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_uniacid_uid` (`uniacid`,`uid`),
  ADD KEY `idx_tid` (`tid`);

--
-- Indexes for table `ims_mc_credits_record`
--
ALTER TABLE `ims_mc_credits_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `ims_mc_fans_groups`
--
ALTER TABLE `ims_mc_fans_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`);

--
-- Indexes for table `ims_mc_fans_tag_mapping`
--
ALTER TABLE `ims_mc_fans_tag_mapping`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mapping` (`fanid`,`tagid`),
  ADD KEY `fanid_index` (`fanid`),
  ADD KEY `tagid_index` (`tagid`);

--
-- Indexes for table `ims_mc_groups`
--
ALTER TABLE `ims_mc_groups`
  ADD PRIMARY KEY (`groupid`),
  ADD KEY `uniacid` (`uniacid`);

--
-- Indexes for table `ims_mc_handsel`
--
ALTER TABLE `ims_mc_handsel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`touid`),
  ADD KEY `uniacid` (`uniacid`);

--
-- Indexes for table `ims_mc_mapping_fans`
--
ALTER TABLE `ims_mc_mapping_fans`
  ADD PRIMARY KEY (`fanid`),
  ADD KEY `acid` (`acid`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `nickname` (`nickname`),
  ADD KEY `updatetime` (`updatetime`),
  ADD KEY `uid` (`uid`),
  ADD KEY `openid` (`openid`);

--
-- Indexes for table `ims_mc_mapping_ucenter`
--
ALTER TABLE `ims_mc_mapping_ucenter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_mc_mass_record`
--
ALTER TABLE `ims_mc_mass_record`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`,`acid`);

--
-- Indexes for table `ims_mc_members`
--
ALTER TABLE `ims_mc_members`
  ADD PRIMARY KEY (`uid`),
  ADD KEY `groupid` (`groupid`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `email` (`email`),
  ADD KEY `mobile` (`mobile`);

--
-- Indexes for table `ims_mc_member_address`
--
ALTER TABLE `ims_mc_member_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_uinacid` (`uniacid`),
  ADD KEY `idx_uid` (`uid`);

--
-- Indexes for table `ims_mc_member_fields`
--
ALTER TABLE `ims_mc_member_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_uniacid` (`uniacid`),
  ADD KEY `idx_fieldid` (`fieldid`);

--
-- Indexes for table `ims_mc_member_property`
--
ALTER TABLE `ims_mc_member_property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_mc_oauth_fans`
--
ALTER TABLE `ims_mc_oauth_fans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_oauthopenid_acid` (`oauth_openid`,`acid`);

--
-- Indexes for table `ims_menu_event`
--
ALTER TABLE `ims_menu_event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `picmd5` (`picmd5`);

--
-- Indexes for table `ims_message_notice_log`
--
ALTER TABLE `ims_message_notice_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_mobilenumber`
--
ALTER TABLE `ims_mobilenumber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_modules`
--
ALTER TABLE `ims_modules`
  ADD PRIMARY KEY (`mid`),
  ADD KEY `idx_name` (`name`);

--
-- Indexes for table `ims_modules_bindings`
--
ALTER TABLE `ims_modules_bindings`
  ADD PRIMARY KEY (`eid`),
  ADD KEY `idx_module` (`module`);

--
-- Indexes for table `ims_modules_plugin`
--
ALTER TABLE `ims_modules_plugin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `main_module` (`main_module`);

--
-- Indexes for table `ims_modules_rank`
--
ALTER TABLE `ims_modules_rank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_name` (`module_name`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `ims_modules_recycle`
--
ALTER TABLE `ims_modules_recycle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modulename` (`modulename`);

--
-- Indexes for table `ims_music_reply`
--
ALTER TABLE `ims_music_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_news_reply`
--
ALTER TABLE `ims_news_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_profile_fields`
--
ALTER TABLE `ims_profile_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_qrcode`
--
ALTER TABLE `ims_qrcode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_qrcid` (`qrcid`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `ticket` (`ticket`);

--
-- Indexes for table `ims_qrcode_stat`
--
ALTER TABLE `ims_qrcode_stat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_rule`
--
ALTER TABLE `ims_rule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_rule_keyword`
--
ALTER TABLE `ims_rule_keyword`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_content` (`content`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_site_article`
--
ALTER TABLE `ims_site_article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_iscommend` (`iscommend`),
  ADD KEY `idx_ishot` (`ishot`);

--
-- Indexes for table `ims_site_category`
--
ALTER TABLE `ims_site_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_site_multi`
--
ALTER TABLE `ims_site_multi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `bindhost` (`bindhost`);

--
-- Indexes for table `ims_site_nav`
--
ALTER TABLE `ims_site_nav`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `multiid` (`multiid`);

--
-- Indexes for table `ims_site_page`
--
ALTER TABLE `ims_site_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `multiid` (`multiid`);

--
-- Indexes for table `ims_site_slide`
--
ALTER TABLE `ims_site_slide`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `multiid` (`multiid`);

--
-- Indexes for table `ims_site_store_create_account`
--
ALTER TABLE `ims_site_store_create_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_site_store_goods`
--
ALTER TABLE `ims_site_store_goods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module` (`module`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `price` (`price`);

--
-- Indexes for table `ims_site_store_order`
--
ALTER TABLE `ims_site_store_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `goodid` (`goodsid`),
  ADD KEY `buyerid` (`buyerid`);

--
-- Indexes for table `ims_site_styles`
--
ALTER TABLE `ims_site_styles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_site_styles_vars`
--
ALTER TABLE `ims_site_styles_vars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_site_templates`
--
ALTER TABLE `ims_site_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_stat_fans`
--
ALTER TABLE `ims_stat_fans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`,`date`);

--
-- Indexes for table `ims_stat_keyword`
--
ALTER TABLE `ims_stat_keyword`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_createtime` (`createtime`);

--
-- Indexes for table `ims_stat_msg_history`
--
ALTER TABLE `ims_stat_msg_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_createtime` (`createtime`);

--
-- Indexes for table `ims_stat_rule`
--
ALTER TABLE `ims_stat_rule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_createtime` (`createtime`);

--
-- Indexes for table `ims_stat_visit`
--
ALTER TABLE `ims_stat_visit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `date` (`date`),
  ADD KEY `module` (`module`),
  ADD KEY `uniacid` (`uniacid`);

--
-- Indexes for table `ims_uni_account`
--
ALTER TABLE `ims_uni_account`
  ADD PRIMARY KEY (`uniacid`);

--
-- Indexes for table `ims_uni_account_group`
--
ALTER TABLE `ims_uni_account_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_uni_account_menus`
--
ALTER TABLE `ims_uni_account_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `menuid` (`menuid`);

--
-- Indexes for table `ims_uni_account_modules`
--
ALTER TABLE `ims_uni_account_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_module` (`module`),
  ADD KEY `idx_uniacid` (`uniacid`);

--
-- Indexes for table `ims_uni_account_users`
--
ALTER TABLE `ims_uni_account_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_memberid` (`uid`),
  ADD KEY `uniacid` (`uniacid`);

--
-- Indexes for table `ims_uni_group`
--
ALTER TABLE `ims_uni_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`);

--
-- Indexes for table `ims_uni_settings`
--
ALTER TABLE `ims_uni_settings`
  ADD PRIMARY KEY (`uniacid`);

--
-- Indexes for table `ims_uni_verifycode`
--
ALTER TABLE `ims_uni_verifycode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_userapi_cache`
--
ALTER TABLE `ims_userapi_cache`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_userapi_reply`
--
ALTER TABLE `ims_userapi_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_users`
--
ALTER TABLE `ims_users`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `ims_users_bind`
--
ALTER TABLE `ims_users_bind`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uid` (`uid`),
  ADD KEY `bind_sign` (`bind_sign`);

--
-- Indexes for table `ims_users_failed_login`
--
ALTER TABLE `ims_users_failed_login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip_username` (`ip`,`username`);

--
-- Indexes for table `ims_users_founder_group`
--
ALTER TABLE `ims_users_founder_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_users_group`
--
ALTER TABLE `ims_users_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_users_invitation`
--
ALTER TABLE `ims_users_invitation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_code` (`code`);

--
-- Indexes for table `ims_users_permission`
--
ALTER TABLE `ims_users_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_users_profile`
--
ALTER TABLE `ims_users_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_video_reply`
--
ALTER TABLE `ims_video_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_voice_reply`
--
ALTER TABLE `ims_voice_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_wechat_attachment`
--
ALTER TABLE `ims_wechat_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `media_id` (`media_id`),
  ADD KEY `acid` (`acid`);

--
-- Indexes for table `ims_wechat_news`
--
ALTER TABLE `ims_wechat_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `attach_id` (`attach_id`);

--
-- Indexes for table `ims_wxapp_general_analysis`
--
ALTER TABLE `ims_wxapp_general_analysis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uniacid` (`uniacid`),
  ADD KEY `ref_date` (`ref_date`);

--
-- Indexes for table `ims_wxapp_versions`
--
ALTER TABLE `ims_wxapp_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `version` (`version`),
  ADD KEY `uniacid` (`uniacid`);

--
-- Indexes for table `ims_wxcard_reply`
--
ALTER TABLE `ims_wxcard_reply`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rid` (`rid`);

--
-- Indexes for table `ims_zh_gjhdbm_activity`
--
ALTER TABLE `ims_zh_gjhdbm_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_ad`
--
ALTER TABLE `ims_zh_gjhdbm_ad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_article`
--
ALTER TABLE `ims_zh_gjhdbm_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `ims_zh_gjhdbm_assess`
--
ALTER TABLE `ims_zh_gjhdbm_assess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_attestation`
--
ALTER TABLE `ims_zh_gjhdbm_attestation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_bmextend`
--
ALTER TABLE `ims_zh_gjhdbm_bmextend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_bmlist`
--
ALTER TABLE `ims_zh_gjhdbm_bmlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_car`
--
ALTER TABLE `ims_zh_gjhdbm_car`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_case`
--
ALTER TABLE `ims_zh_gjhdbm_case`
  ADD PRIMARY KEY (`case_id`);

--
-- Indexes for table `ims_zh_gjhdbm_city`
--
ALTER TABLE `ims_zh_gjhdbm_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_commission_withdrawal`
--
ALTER TABLE `ims_zh_gjhdbm_commission_withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_contact`
--
ALTER TABLE `ims_zh_gjhdbm_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `ims_zh_gjhdbm_deal`
--
ALTER TABLE `ims_zh_gjhdbm_deal`
  ADD PRIMARY KEY (`deal_id`);

--
-- Indexes for table `ims_zh_gjhdbm_distribution`
--
ALTER TABLE `ims_zh_gjhdbm_distribution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_earnings`
--
ALTER TABLE `ims_zh_gjhdbm_earnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_enroll`
--
ALTER TABLE `ims_zh_gjhdbm_enroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_follow`
--
ALTER TABLE `ims_zh_gjhdbm_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_fxset`
--
ALTER TABLE `ims_zh_gjhdbm_fxset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_fxuser`
--
ALTER TABLE `ims_zh_gjhdbm_fxuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_hx`
--
ALTER TABLE `ims_zh_gjhdbm_hx`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_menu`
--
ALTER TABLE `ims_zh_gjhdbm_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_message`
--
ALTER TABLE `ims_zh_gjhdbm_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_nav`
--
ALTER TABLE `ims_zh_gjhdbm_nav`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_pic`
--
ALTER TABLE `ims_zh_gjhdbm_pic`
  ADD PRIMARY KEY (`pic_id`);

--
-- Indexes for table `ims_zh_gjhdbm_qiniu`
--
ALTER TABLE `ims_zh_gjhdbm_qiniu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_sponsor`
--
ALTER TABLE `ims_zh_gjhdbm_sponsor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_system`
--
ALTER TABLE `ims_zh_gjhdbm_system`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_tel`
--
ALTER TABLE `ims_zh_gjhdbm_tel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_ticket`
--
ALTER TABLE `ims_zh_gjhdbm_ticket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_type`
--
ALTER TABLE `ims_zh_gjhdbm_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_user`
--
ALTER TABLE `ims_zh_gjhdbm_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ims_zh_gjhdbm_withdrawal`
--
ALTER TABLE `ims_zh_gjhdbm_withdrawal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ims_account`
--
ALTER TABLE `ims_account`
  MODIFY `acid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ims_account_wxapp`
--
ALTER TABLE `ims_account_wxapp`
  MODIFY `acid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ims_article_category`
--
ALTER TABLE `ims_article_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_article_news`
--
ALTER TABLE `ims_article_news`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_article_notice`
--
ALTER TABLE `ims_article_notice`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_article_unread_notice`
--
ALTER TABLE `ims_article_unread_notice`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_basic_reply`
--
ALTER TABLE `ims_basic_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_business`
--
ALTER TABLE `ims_business`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_core_attachment`
--
ALTER TABLE `ims_core_attachment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ims_core_cron`
--
ALTER TABLE `ims_core_cron`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_core_cron_record`
--
ALTER TABLE `ims_core_cron_record`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_core_menu`
--
ALTER TABLE `ims_core_menu`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_core_paylog`
--
ALTER TABLE `ims_core_paylog`
  MODIFY `plid` bigint(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_core_performance`
--
ALTER TABLE `ims_core_performance`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_core_queue`
--
ALTER TABLE `ims_core_queue`
  MODIFY `qid` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_core_refundlog`
--
ALTER TABLE `ims_core_refundlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_core_resource`
--
ALTER TABLE `ims_core_resource`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_core_sendsms_log`
--
ALTER TABLE `ims_core_sendsms_log`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_coupon_location`
--
ALTER TABLE `ims_coupon_location`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_cover_reply`
--
ALTER TABLE `ims_cover_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ims_custom_reply`
--
ALTER TABLE `ims_custom_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_images_reply`
--
ALTER TABLE `ims_images_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_cash_record`
--
ALTER TABLE `ims_mc_cash_record`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_chats_record`
--
ALTER TABLE `ims_mc_chats_record`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_credits_recharge`
--
ALTER TABLE `ims_mc_credits_recharge`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_credits_record`
--
ALTER TABLE `ims_mc_credits_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_fans_groups`
--
ALTER TABLE `ims_mc_fans_groups`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_fans_tag_mapping`
--
ALTER TABLE `ims_mc_fans_tag_mapping`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_groups`
--
ALTER TABLE `ims_mc_groups`
  MODIFY `groupid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_mc_handsel`
--
ALTER TABLE `ims_mc_handsel`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_mapping_fans`
--
ALTER TABLE `ims_mc_mapping_fans`
  MODIFY `fanid` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_mapping_ucenter`
--
ALTER TABLE `ims_mc_mapping_ucenter`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_mass_record`
--
ALTER TABLE `ims_mc_mass_record`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_members`
--
ALTER TABLE `ims_mc_members`
  MODIFY `uid` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_member_address`
--
ALTER TABLE `ims_mc_member_address`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_member_fields`
--
ALTER TABLE `ims_mc_member_fields`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_member_property`
--
ALTER TABLE `ims_mc_member_property`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mc_oauth_fans`
--
ALTER TABLE `ims_mc_oauth_fans`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_menu_event`
--
ALTER TABLE `ims_menu_event`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_message_notice_log`
--
ALTER TABLE `ims_message_notice_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_mobilenumber`
--
ALTER TABLE `ims_mobilenumber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_modules`
--
ALTER TABLE `ims_modules`
  MODIFY `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ims_modules_bindings`
--
ALTER TABLE `ims_modules_bindings`
  MODIFY `eid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_modules_plugin`
--
ALTER TABLE `ims_modules_plugin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_modules_rank`
--
ALTER TABLE `ims_modules_rank`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_modules_recycle`
--
ALTER TABLE `ims_modules_recycle`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_music_reply`
--
ALTER TABLE `ims_music_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_news_reply`
--
ALTER TABLE `ims_news_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_profile_fields`
--
ALTER TABLE `ims_profile_fields`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `ims_qrcode`
--
ALTER TABLE `ims_qrcode`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_qrcode_stat`
--
ALTER TABLE `ims_qrcode_stat`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_rule`
--
ALTER TABLE `ims_rule`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ims_rule_keyword`
--
ALTER TABLE `ims_rule_keyword`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `ims_site_article`
--
ALTER TABLE `ims_site_article`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_category`
--
ALTER TABLE `ims_site_category`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_multi`
--
ALTER TABLE `ims_site_multi`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_site_nav`
--
ALTER TABLE `ims_site_nav`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_page`
--
ALTER TABLE `ims_site_page`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_slide`
--
ALTER TABLE `ims_site_slide`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_store_create_account`
--
ALTER TABLE `ims_site_store_create_account`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_store_goods`
--
ALTER TABLE `ims_site_store_goods`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_store_order`
--
ALTER TABLE `ims_site_store_order`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_styles`
--
ALTER TABLE `ims_site_styles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_site_styles_vars`
--
ALTER TABLE `ims_site_styles_vars`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_site_templates`
--
ALTER TABLE `ims_site_templates`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_stat_fans`
--
ALTER TABLE `ims_stat_fans`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `ims_stat_keyword`
--
ALTER TABLE `ims_stat_keyword`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_stat_msg_history`
--
ALTER TABLE `ims_stat_msg_history`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_stat_rule`
--
ALTER TABLE `ims_stat_rule`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_stat_visit`
--
ALTER TABLE `ims_stat_visit`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_uni_account`
--
ALTER TABLE `ims_uni_account`
  MODIFY `uniacid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ims_uni_account_group`
--
ALTER TABLE `ims_uni_account_group`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_uni_account_menus`
--
ALTER TABLE `ims_uni_account_menus`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_uni_account_modules`
--
ALTER TABLE `ims_uni_account_modules`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_uni_account_users`
--
ALTER TABLE `ims_uni_account_users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_uni_group`
--
ALTER TABLE `ims_uni_group`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_uni_verifycode`
--
ALTER TABLE `ims_uni_verifycode`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_userapi_cache`
--
ALTER TABLE `ims_userapi_cache`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_userapi_reply`
--
ALTER TABLE `ims_userapi_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ims_users`
--
ALTER TABLE `ims_users`
  MODIFY `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_users_bind`
--
ALTER TABLE `ims_users_bind`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_users_failed_login`
--
ALTER TABLE `ims_users_failed_login`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `ims_users_founder_group`
--
ALTER TABLE `ims_users_founder_group`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_users_group`
--
ALTER TABLE `ims_users_group`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_users_invitation`
--
ALTER TABLE `ims_users_invitation`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_users_permission`
--
ALTER TABLE `ims_users_permission`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_users_profile`
--
ALTER TABLE `ims_users_profile`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_video_reply`
--
ALTER TABLE `ims_video_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_voice_reply`
--
ALTER TABLE `ims_voice_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_wechat_attachment`
--
ALTER TABLE `ims_wechat_attachment`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_wechat_news`
--
ALTER TABLE `ims_wechat_news`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_wxapp_general_analysis`
--
ALTER TABLE `ims_wxapp_general_analysis`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_wxapp_versions`
--
ALTER TABLE `ims_wxapp_versions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_wxcard_reply`
--
ALTER TABLE `ims_wxcard_reply`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_activity`
--
ALTER TABLE `ims_zh_gjhdbm_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_ad`
--
ALTER TABLE `ims_zh_gjhdbm_ad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_article`
--
ALTER TABLE `ims_zh_gjhdbm_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_assess`
--
ALTER TABLE `ims_zh_gjhdbm_assess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_attestation`
--
ALTER TABLE `ims_zh_gjhdbm_attestation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_bmextend`
--
ALTER TABLE `ims_zh_gjhdbm_bmextend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_bmlist`
--
ALTER TABLE `ims_zh_gjhdbm_bmlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_car`
--
ALTER TABLE `ims_zh_gjhdbm_car`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_case`
--
ALTER TABLE `ims_zh_gjhdbm_case`
  MODIFY `case_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_city`
--
ALTER TABLE `ims_zh_gjhdbm_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_commission_withdrawal`
--
ALTER TABLE `ims_zh_gjhdbm_commission_withdrawal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_contact`
--
ALTER TABLE `ims_zh_gjhdbm_contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_deal`
--
ALTER TABLE `ims_zh_gjhdbm_deal`
  MODIFY `deal_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_distribution`
--
ALTER TABLE `ims_zh_gjhdbm_distribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_earnings`
--
ALTER TABLE `ims_zh_gjhdbm_earnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_enroll`
--
ALTER TABLE `ims_zh_gjhdbm_enroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_follow`
--
ALTER TABLE `ims_zh_gjhdbm_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_fxset`
--
ALTER TABLE `ims_zh_gjhdbm_fxset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_fxuser`
--
ALTER TABLE `ims_zh_gjhdbm_fxuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_hx`
--
ALTER TABLE `ims_zh_gjhdbm_hx`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_menu`
--
ALTER TABLE `ims_zh_gjhdbm_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_message`
--
ALTER TABLE `ims_zh_gjhdbm_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_nav`
--
ALTER TABLE `ims_zh_gjhdbm_nav`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_pic`
--
ALTER TABLE `ims_zh_gjhdbm_pic`
  MODIFY `pic_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_qiniu`
--
ALTER TABLE `ims_zh_gjhdbm_qiniu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_sponsor`
--
ALTER TABLE `ims_zh_gjhdbm_sponsor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_system`
--
ALTER TABLE `ims_zh_gjhdbm_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_tel`
--
ALTER TABLE `ims_zh_gjhdbm_tel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_ticket`
--
ALTER TABLE `ims_zh_gjhdbm_ticket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_type`
--
ALTER TABLE `ims_zh_gjhdbm_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_user`
--
ALTER TABLE `ims_zh_gjhdbm_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ims_zh_gjhdbm_withdrawal`
--
ALTER TABLE `ims_zh_gjhdbm_withdrawal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
