<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/header', TEMPLATE_INCLUDEPATH)) : (include template('public/header', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/comhead', TEMPLATE_INCLUDEPATH)) : (include template('public/comhead', TEMPLATE_INCLUDEPATH));?>
<link rel="stylesheet" type="text/css" href="../addons/zh_gjhdbm/template/public/ygcss.css">
<style type="text/css">
    .dizhi{margin-top: 10px;color: #44ABF7;}
    .yginp{width: 50%;}
    .ygspan{line-height: 35px;margin-left: 10px;}
    .form-group>label>b {
        color: red;
    }
</style>
<ul class="nav nav-tabs">
    <span class="ygxian"></span>
    <div class="ygdangq">当前位置:</div>
    <li ><a href="<?php  echo $this->createWebUrl('article');?>">文章管理</a></li>
    <li class="active"><a href="<?php  echo $this->createWebUrl('addarticle');?>">添加文章</a></li>
</ul>
<div class="main ygmain">
    <form action="" method="post" class="form-horizontal form" enctype="multipart/form-data">
        <div class="panel panel-default ygdefault">
            <div class="panel-heading wyheader">
                内容编辑
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label"><b>*</b>标题</label>
                    <div class="col-sm-9">
                        <input type="text"  name="title" value="<?php  echo $list['title'];?>" class="form-control" placeholder="请填写文章标题">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><b>*</b>内容</label>
                        <div class="col-sm-9">
                            <?php  echo tpl_ueditor('content',$list['content']);?>
                        </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9" style="margin: 0 auto">
                    <input type="submit" name="submit" value="提交" class="btn col-lg-3" style="color: white;background-color: #44ABF7;" />
                    <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" />
                    <input type="hidden" name="case_id" value="<?php  echo $list['article_id'];?>" />
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(function() {
        $("#frame-17").show();
        $("#yframe-17").addClass("wyarticle");
    })
</script>