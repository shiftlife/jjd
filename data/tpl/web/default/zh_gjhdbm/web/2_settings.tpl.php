<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/header', TEMPLATE_INCLUDEPATH)) : (include template('public/header', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/comhead', TEMPLATE_INCLUDEPATH)) : (include template('public/comhead', TEMPLATE_INCLUDEPATH));?>
<link rel="stylesheet" type="text/css" href="../addons/zh_gjhdbm/template/public/ygcss.css">
<style type="text/css">
    .yginp{width: 40%;}
    .ygspan{line-height: 35px;margin-left: 10px;}
</style>
<ul class="nav nav-tabs">    
    <span class="ygxian"></span>
    <div class="ygdangq">当前位置:</div>
    <li class="active"><a href="javascript:void(0);">基本信息</a></li>
</ul>
<div class="main">
    <form action="" method="post" class="form-horizontal form" enctype="multipart/form-data">
        <!--<input type="hidden" name="parentid" value="<?php  echo $parent['id'];?>" />-->
        <div class="panel panel-default ygdefault">
            <div class="panel-heading wyheader">
                基本信息
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">平台名称</label>
                    <div class="col-sm-9">
                     <input type="text" name="pt_name" class="form-control" value="<?php  echo $item['pt_name'];?>" />
                 </div>
             </div>  
             <div class="form-group">
                <label class="col-xs-12 col-sm-3 col-md-2 control-label">平台电话</label>
                <div class="col-sm-9">
                    <input type="text" name="tel"  class="form-control" value="<?php  echo $item['tel'];?>" />
                </div>
            </div>       
          <!--   <div class="form-group">
              <label class="col-xs-12 col-sm-3 col-md-2 control-label">平台风格颜色</label>
              <div class="col-sm-9">
               <?php  echo tpl_form_field_color('color', $item['color'])?> 
               <span class="help-block">*不填写会默认选中红色</span>
           </div>
       </div>  
       <div class="form-group">
        <label class="col-xs-12 col-sm-3 col-md-2 control-label">平台访问量</label>
        <div class="col-sm-9">
         <input type="number" name="total_num"  class="form-control" value="<?php  echo $item['total_num'];?>" />
         <span class="help-block">*空或不填则为真实访问数据</span>
     </div>
 </div> -->

<!--  <div class="form-group">
 <label class="col-xs-12 col-sm-3 col-md-2 control-label">活动审核类型</label>
    <div class="col-sm-9">
        <label class="radio-inline">
            <input type="radio" id="emailwy1" name="hdsh_open" value="1" <?php  if($item['hdsh_open']==1 || empty($item['hdsh_open'])) { ?>checked<?php  } ?> />
            <label for="emailwy1">手动审核</label>
        </label>
        <label class="radio-inline">
            <input type="radio" id="emailwy2" name="hdsh_open" value="2" <?php  if($item['hdsh_open']==2) { ?>checked<?php  } ?> />
            <label for="emailwy2">自动审核</label>
        </label>    
    </div>
</div> -->
<!-- <div class="form-group">
    <label class="col-xs-12 col-sm-3 col-md-2 control-label">报名审核类型</label>
    <div class="col-sm-9">
        <label class="radio-inline">
            <input type="radio" id="emailwy1" name="bmsh_open" value="1" <?php  if($item['bmsh_open']==1 || empty($item['bmsh_open'])) { ?>checked<?php  } ?> />
            <label for="emailwy1">手动审核</label>
        </label>
        <label class="radio-inline">
            <input type="radio" id="emailwy2" name="bmsh_open" value="2" <?php  if($item['bmsh_open']==2) { ?>checked<?php  } ?> />
            <label for="emailwy2">自动审核</label>
        </label>    
    </div>
</div> -->
<div class="form-group">
  <label class="col-xs-12 col-sm-3 col-md-2 control-label">活动列表样式</label>
  <div class="col-sm-9">
    <label class="radio-inline">
      <input type="radio" id="emailwy1" name="style" value="1" <?php  if($item['style']==1 || empty($item['style'])) { ?>checked<?php  } ?> />
      <label for="emailwy1">左右布局</label>
    </label>
    <label class="radio-inline">
      <input type="radio" id="emailwy2" name="style" value="2" <?php  if($item['style']==2) { ?>checked<?php  } ?> />
      <label for="emailwy2">上下布局</label>
    </label>    
  </div>
</div>
<div class="form-group">
  <label class="col-xs-12 col-sm-3 col-md-2 control-label">留言次数</label>
  <div class="col-sm-9">
    <input type="number" name="ly_num"  class="form-control" value="<?php  echo $item['ly_num'];?>" />
  </div>
</div>       

<div class="form-group">
<label class="col-xs-12 col-sm-3 col-md-2 control-label">多城市管理</label>
  <div class="col-sm-9">
    <label class="radio-inline">
      <input type="radio" id="emailwy1" name="city_open" value="1" <?php  if($item['city_open']==1 ) { ?>checked<?php  } ?> />
      <label for="emailwy1">开启</label>
    </label>
    <label class="radio-inline">
      <input type="radio" id="emailwy2" name="city_open" value="2" <?php  if($item['city_open']==2 || empty($item['city_open'])) { ?>checked<?php  } ?> />
      <label for="emailwy2">关闭</label>
    </label>    
<span class="help-block">*开启后平台可切换多个城市运营</span>
  </div>
</div>
<div class="form-group">
    <label class="col-xs-12 col-sm-3 col-md-2 control-label">发布须知</label>
    <div class="col-sm-9">
       <?php  echo tpl_ueditor('fb_notice',$item['fb_notice']);?>
   </div>
</div>
<div class="form-group">
    <label class="col-xs-12 col-sm-3 col-md-2 control-label">平台简介</label>
    <div class="col-sm-9">
       <?php  echo tpl_ueditor('details',$item['details']);?>
   </div>
</div> 
<div class="form-group">
    <input type="submit" name="submit" value="提交" class="btn col-lg-3" style="color: white;background-color: #44ABF7;"/>
    <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" />
    <input type="hidden" name="id" value="<?php  echo $item['id'];?>" />
</div>               
</div>
</div>
</form>
</div>
<script type="text/javascript">
    $(function(){
        $("#frame-5").show();
        $("#yframe-5").addClass("wyactive");
    })
</script>