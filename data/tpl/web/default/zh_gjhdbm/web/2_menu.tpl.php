<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/header', TEMPLATE_INCLUDEPATH)) : (include template('public/header', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/comhead', TEMPLATE_INCLUDEPATH)) : (include template('public/comhead', TEMPLATE_INCLUDEPATH));?>
<link rel="stylesheet" type="text/css" href="../addons/zh_gjhdbm/template/public/ygcsslist.css">
<ul class="nav nav-tabs">
    <span class="ygxian"></span>
    <div class="ygdangq">当前位置:</div>    
    <li class="active"><a href="<?php  echo $this->createWebUrl('menu')?>">菜单管理</a></li>
    <li><a href="<?php  echo $this->createWebUrl('addmenu')?>">添加菜单</a></li>

</ul>
<div class="main">
    <div class="panel panel-default">
        <div class="panel-heading">
            菜单管理(仅允许替换两个底部菜单)
        </div>
        <div class="panel-body" style="padding: 0px 15px;">
            <div class="row">
                <table class="yg5_tabel col-md-12">
                    <tr class="yg5_tr1">
                        <th class="store_td1 col-md-1">排序</th>                       
                        <th class="col-md-2">菜单名称</th>
                        <th class="col-md-2">路径</th>
                        <th class="col-md-2">选中图标</th>                       
                      <th class="col-md-2">未选中图标</th>
                        <th class="col-md-2">操作</th>
                    </tr>
                    <?php  if(is_array($list)) { foreach($list as $row) { ?>
                    <tr class="yg5_tr2">
                        <!-- <td><div class="type-parent"><?php  echo $row['orderby'];?>&nbsp;&nbsp;</div></td> -->
                        <tr class="yg5_tr2">
                            <td class="num<?php  echo $row['id'];?>">
                                <span class="numspan<?php  echo $row['id'];?>"><?php  echo $row['sort'];?></span>
                              
                            </td>                           
                        <td><?php  echo $row['name'];?></td>
                         <td><?php  echo $row['src'];?></td>
                        <td><div class="type-parent"><img height="40" src="<?php  echo tomedia($row['icon1']);?>">&nbsp;&nbsp;</div></td>
                       
                       <td><div class="type-parent"><img height="40" src="<?php  echo tomedia($row['icon2']);?>">&nbsp;&nbsp;</div></td>
                        <td>
                            <a href="<?php  echo $this->createWebUrl('addmenu', array('id' => $row['id']))?>" class="storespan btn btn-xs">
                                <span class="fa fa-pencil"></span>
                                <span class="bianji">编辑
                                    <span class="arrowdown"></span>
                                </span>                            
                            </a>
                            <a href="javascript:void(0);" class="storespan btn btn-xs" data-toggle="modal" data-target="#myModal<?php  echo $row['id'];?>">
                                <span class="fa fa-trash-o"></span>
                                <span class="bianji">删除
                                    <span class="arrowdown"></span>
                                </span>
                            </a>
                            <!-- <a class="btn btn-warning btn-xs" href="<?php  echo $this->createWebUrl('addad', array('id' => $row['id']))?>" title="编辑">改</a>&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#myModal<?php  echo $row['id'];?>">删</button> -->
                        </td>
                    </tr>
                    <div class="modal fade" id="myModal<?php  echo $row['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel" style="font-size: 20px;">提示</h4>
                        </div>
                        <div class="modal-body" style="font-size: 20px">
                            确定删除么？
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                            <a href="<?php  echo $this->createWebUrl('menu', array('op' => 'delete', 'id' => $row['id']))?>" type="button" class="btn btn-info" >确定</a>
                        </div>
                    </div>
                </div>
            </div>
           
                    <?php  } } ?>
                     <?php  if(empty($list)) { ?>
                      <tr class="yg5_tr2">
                      <td colspan="12">
                            暂无菜单信息
                        </td>
                    </tr>
                    <?php  } ?>

            
                </table>
            </div>
        </form>
    </div>

</div>
<div class="text-right we7-margin-top">
   <?php  echo $pager;?>
</div>
<script type="text/javascript">
    $(function(){
        $("#frame-18").show();
        $("#yframe-18").addClass("wyactive");
    })
</script>