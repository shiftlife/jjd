<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/header', TEMPLATE_INCLUDEPATH)) : (include template('public/header', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/comhead', TEMPLATE_INCLUDEPATH)) : (include template('public/comhead', TEMPLATE_INCLUDEPATH));?>
<link rel="stylesheet" type="text/css" href="../addons/zh_gjhdbm/template/public/ygcss.css">
<style type="text/css">
    .dizhi{margin-top: 10px;color: #44ABF7;}
    .yginp{width: 50%;}
    .ygspan{line-height: 35px;margin-left: 10px;}
    .form-group>label>b {
        color: red;
    }
</style>
<ul class="nav nav-tabs">
    <span class="ygxian"></span>
    <div class="ygdangq">当前位置:</div>
    <li ><a href="<?php  echo $this->createWebUrl('deal');?>">交警管理</a></li>
</ul>
<div class="main ygmain">
    <form action="" method="post" class="form-horizontal form" enctype="multipart/form-data">
        <div class="panel panel-default ygdefault">
            <div class="panel-heading wyheader">
                内容编辑
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label"><b>*</b>名字</label>
                    <div class="col-sm-9">
                        <input type="text"  name="user_name" value="<?php  echo $list['user_name'];?>" class="form-control" placeholder="请填写交警名字">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><b>*</b>电话</label>
                    <div class="col-sm-9">
                        <input type="text"  name="link_tel" value="<?php  echo $list['link_tel'];?>" class="form-control" placeholder="请填写交警电话">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"><b>*</b>警号</label>
                    <div class="col-sm-9">
                        <input type="text"  name="police_num" value="<?php  echo $list['police_num'];?>" class="form-control" placeholder="请填写警号">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9" style="margin: 0 auto">
                        <input type="submit" name="submit" value="提交" class="btn col-lg-3" style="color: white;background-color: #44ABF7;" />
                        <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" />
                        <input type="hidden" name="id" value="<?php  echo $list['id'];?>" />
                    </div>
                </div>
        </div>
    </form>
</div>
<script>
    $(function() {
        $("#frame-18").show();
        $("#yframe-18").addClass("wyarticle");
    })
</script>
