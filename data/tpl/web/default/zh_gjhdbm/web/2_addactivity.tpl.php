<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/header', TEMPLATE_INCLUDEPATH)) : (include template('public/header', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/comhead', TEMPLATE_INCLUDEPATH)) : (include template('public/comhead', TEMPLATE_INCLUDEPATH));?>
<link rel="stylesheet" type="text/css" href="../addons/zh_gjhdbm/template/public/ygcss.css">
<style type="text/css">
    .dizhi{margin-top: 10px;color: #44ABF7;}
    .yginp{width: 50%;}
    .ygspan{line-height: 35px;margin-left: 10px;}
    .form-group>label>b {
        color: red;
    }
</style>
<ul class="nav nav-tabs">
    <span class="ygxian"></span>
    <div class="ygdangq">当前位置:</div>
    <li ><a href="<?php  echo $this->createWebUrl('activity');?>">案件管理</a></li>
    <li class="active"><a href="<?php  echo $this->createWebUrl('addactivity');?>">添加案件</a></li>
</ul>
<div class="main ygmain">
    <form action="" method="post" class="form-horizontal form" enctype="multipart/form-data">
        <div class="panel panel-default ygdefault">
            <div class="panel-heading wyheader">
                案件详情
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">定位地点</label>
                    <div class="col-sm-9">
                        <input type="text"  name="address" value="<?php  echo $list['address'];?>" class="form-control" placeholder="定位地点">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">位置描述</label>
                    <div class="col-sm-9">
                        <input type="text"  name="address_descript" value="<?php  echo $list['address_descript'];?>" class="form-control" placeholder="定位地点">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">事故类型</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="case_type">
                            <option value="0">请选择</option>
                            <option value="1" <?php  if($list['case_type']==1) { ?>selected<?php  } ?>>一般事故</option>
                            <option value="2" <?php  if($list['case_type']==2) { ?>selected<?php  } ?>>严重事故</option>
                        </select>
                    </div>
                </div>
                <?php  if($contact_arr) { ?>
                <?php  if(is_array($contact_arr)) { foreach($contact_arr as $key => $contact) { ?>
                <div class="form-group show_contact">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label"><?php  echo $contact['type'];?>方信息</label>
                    <div class="contact_html">
                        <div class="col-lg-2">
                            <input type="text"  class="form-control"  name="contact[]"  class='col-sm-2' value="<?php  echo $contact['contact_name'];?>"  placeholder="联系人" />
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" name="tel[]"   value="<?php  echo $contact['contact_tel'];?>"  placeholder="联系电话"/>
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" name="plate_num[]" value="<?php  echo $contact['plate_num'];?>" placeholder="车牌号" />
                        </div>
                        <input type='hidden' class='contact_id' name='contact_ids[]' value="<?php  echo $contact['contact_id'];?>"/>
                        <input type="hidden" class="form-control" name="type[]" value="<?php  echo $contact['type'];?>"/>
                        <?php  if($key== 0) { ?>
                        <!--<a href='javascript:void(0)' class='btn btn-info add_contact' >添加</a>-->
                        <!--<a href='javascript:void(0)' class='btn btn-info del_attr' <?php  if(count($contact_arr)==1 ) { ?>style='display:none'<?php  } ?> onclick="delContactAttr()">删除</a>-->
                        <?php  } ?>
                    </div>
                </div>
                <?php  } } ?>
                <?php  } else { ?>
                <div class="form-group show_contact">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">A方信息</label>
                    <div class="contact_html">
                        <div class="col-lg-2">
                            <input type="text"  class="form-control"  name="contact[]"  class='col-sm-2'  placeholder="联系人" />
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" name="tel[]"  placeholder="联系电话"/>
                        </div>
                        <div class="col-lg-2">
                            <input type="text" class="form-control" name="plate_num[]" placeholder="车牌号" />
                        </div>
                        <input type='hidden' name='contact_ids[]'/>
                        <input type="hidden" class="form-control" name="type[]" />
                        <a href='javascript:void(0)' class='btn btn-info add_contact' >添加</a>
                        <a href='javascript:void(0)' class='btn btn-info del_attr' style="display: none" onclick="delContactAttr()">删除</a>
                    </div>
                </div>
                <?php  } ?>
                <!--<div class="form-group">-->
                    <!--<label class="col-xs-12 col-sm-3 col-md-2 control-label">处理状态</label>-->
                    <!--<div class="col-sm-9">-->
                        <!--<input type="text" readonly  class="form-control" value="<?php  if($list['status'] == 0) { ?>未处理<?php  } else if($list['status'] == 1) { ?>已通过<?php  } else { ?>已拒绝<?php  } ?>" />-->
                    <!--</div>-->
                <!--</div>-->
                <div class="form-group">
                    <label class="col-sm-2 control-label">结案内容</label>
                    <div class="col-sm-9">
                        <textarea name="content"  class="form-control" placeholder="结案内容"><?php  echo $list['content'];?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">事故图片</label>
                    <div class="col-sm-9">
                        <?php  echo tpl_form_field_multi_image('acc_pic',$list['pic_arr'],1);?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-3 col-md-2 control-label">结案事故图片</label>
                    <div class="col-sm-9">
                        <?php  echo tpl_form_field_multi_image('end_pic',$list['end_arr'],2);?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-9" style="margin: 0 auto">
                    <!--<input type="submit" name="submit" value="提交" class="btn col-lg-3" style="color: white;background-color: #44ABF7;" />-->
                    <input type="hidden" name="token" value="<?php  echo $_W['token'];?>" />
                    <input type="hidden" name="case_id" value="<?php  echo $list['case_id'];?>" />
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(function() {
        $("#frame-8").show();
        $("#yframe-8").addClass("wyarticle");
    })
    var contact_index = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
    $(".add_contact").click(function() {
        var index = $('.show_contact').length;
        if(index==1){
            $(".del_attr").show();
        }
        //添加
        var html =  "<div class='form-group show_contact'><label class='col-xs-12 col-sm-3 col-md-2 control-label'>" + contact_index[index] +"方信息</label> <div class='contact_html'> <div class='col-lg-2'><input type='text'  class='form-control'  name='contact[]' class='col-sm-2'  placeholder='请填写联系人' /> </div> <div class='col-lg-2'> <input type='text' class='form-control' name='tel[]' placeholder='请填写联系电话'/> </div> <div class='col-lg-2'> <input type='text' class='form-control' name='plate_num[]' placeholder='请填写车牌号' /></div> <input type='hidden' name='contact_ids[]' value=''/> <input type='hidden' name='type[]' value=''/></div> </div>";
        $(".show_contact:last").after(html);

    });
    //删除属性
    function delContactAttr(){
        var id = $('.show_contact:last .contact_id').val();
        if(confirm("你确定删除该条数据吗？删除后不能恢复"))
        {
            $.ajax({
                type: 'post',
                url: "<?php  echo $this->createWebUrl('addactivity',array('op'=>del_contact))?>",
                data: {
                    "contact_id": id
                },
                success: function (result) {
                    if($('.show_contact').length>1){
                        $('.show_contact:last').remove();
                    }
                    if($('.show_contact').length==1){
                        $('.del_attr').css("display","none");
                    }
                }
            });
        }


    }
</script>
