<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/header', TEMPLATE_INCLUDEPATH)) : (include template('public/header', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/comhead', TEMPLATE_INCLUDEPATH)) : (include template('public/comhead', TEMPLATE_INCLUDEPATH));?>
<link rel="stylesheet" type="text/css" href="../addons/zh_gjhdbm/template/public/ygcsslist.css">
<style type="text/css">
  .actiul>.clearfix{border-bottom: 1px solid #e4e7e9;position: relative;padding: 10px 0;}
  .actiul .clearfix .li-box{display: inline-block;}
  .text-right{text-align: right;top: 30px;right: 15px;}
  .chebox{float: left;margin-right: 20px;margin-top: 45px;}
  .actimg{float: left;}
  .actimg>img{width: 160px;height: 120px;border-radius: 3px;}
  .actibox{font-size: 12px;margin-left: 16px;float: left;position: relative;}
  .actiname{color: #009688;max-width: 480px;min-width: 20px;overflow: hidden;text-overflow: ellipsis;white-space: normal;font-size: 16px;margin: 6px 0;}
  .actiname>a:hover{color: #333;}
  .actime{color: #999;font-size: 12px;line-height: 2.3em;}
  .actime>span:nth-child(2){margin-left: 32px;}
  .actime>span:nth-child(3){margin-left: 32px;}
  .actixin{font-size: 12px;line-height: 2.3em;color: #333;}
  .actixin>.actixinli{margin-right: 20px;}
  .actiguan{height: 30px;line-height: 30px;margin-top: 5px;font-size: 14px;}
  .actiguan>a{border-left: 1px solid #ddd;padding: 0 10px;color: #333;}
  .actiguan a .fa{color: #00BCD4;line-height: 18px;font-size: 20px;}
  .actiguan a span{display: inline-block;}
  .actiguan a .fa{margin-right: 4px;}
</style>
<script type="text/javascript" src="<?php  echo $_W['siteroot'];?>/web/resource/webuploader-0.1.5/dist/jquery-1.11.1.min.js"></script>

<ul class="nav nav-tabs">
  <span class="ygxian"></span>
  <div class="ygdangq">当前位置:</div>
    <li class="active"><a href="<?php  echo $this->createWebUrl('article')?>">文章管理</a></li>
    <li><a href="<?php  echo $this->createWebUrl('addarticle')?>">添加文章</a></li>
</ul>

<div class="main">
  <div class="panel panel-default">
    <div class="panel-heading">文章管理</div>
        <div class="panel-body" style="padding: 0px 15px;">
            <div class="row">
                <table class="yg5_tabel col-md-12">
                  <tr class="yg5_tr1">
                      <td>编号</td>
                      <td class="store_td1">标题</td>
                      <td>时间</td>
                      <td>操作</td>
                  </tr>
                 <?php  if(is_array($list)) { foreach($list as $key => $item) { ?>
                  <tr class="yg5_tr2">
                      <td><?php  echo $item['article_id'];?></td>
                      <td class="store_td1"><?php  echo $item['title'];?></td>
                      <td><?php  echo date("Y-m-d H:i:s",$item['add_time'])?></td>
                      <td>

                          <a href="<?php  echo $this->createWebUrl('addarticle', array('article_id' => $item['article_id']))?>" class="storespan btn btn-xs">
                            <span class="fa fa-pencil"></span>
                            <span class="bianji">编辑<span class="aritemdown"></span></span>
                          </a>
                          <a class="storespan btn btn-xs" href="<?php  echo $this->createWebUrl('article', array('article_id' => $item['article_id'],'op'=>'delete'))?>" onclick="return confirm('确认删除吗？');return false;">
                            <span class="fa fa-trash-o"></span>
                            <span class="bianji">删除<span class="aritemdown"></span></span>
                          </a>
                      </td>
                  </tr>
            <?php  } } ?>
            <?php  if(empty($list)) { ?>
                <tr class="yg5_tr2">
                  <td colspan="9">
                    暂无文章
                  </td>
                </tr>
            <?php  } ?>
          </table>
            </div>
        </div>
    </div>
</div>
<div class="text-right we7-margin-top">
    <?php  echo $pager;?>
</div>
<script>
    $(function() {
        $("#frame-17").show();
        $("#yframe-17").addClass("wyactive");
    })
</script>