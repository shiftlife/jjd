<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/header', TEMPLATE_INCLUDEPATH)) : (include template('public/header', TEMPLATE_INCLUDEPATH));?>
<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/comhead', TEMPLATE_INCLUDEPATH)) : (include template('public/comhead', TEMPLATE_INCLUDEPATH));?>
<link rel="stylesheet" type="text/css" href="../addons/zh_gjhdbm/template/public/ygcsslist.css">
<style type="text/css">
  .actiul>.clearfix{border-bottom: 1px solid #e4e7e9;position: relative;padding: 10px 0;}
  .actiul .clearfix .li-box{display: inline-block;}
  .text-right{text-align: right;top: 30px;right: 15px;}
  .chebox{float: left;margin-right: 20px;margin-top: 45px;}
  .actimg{float: left;}
  .actimg>img{width: 160px;height: 120px;border-radius: 3px;}
  .actibox{font-size: 12px;margin-left: 16px;float: left;position: relative;}
  .actiname{color: #009688;max-width: 480px;min-width: 20px;overflow: hidden;text-overflow: ellipsis;white-space: normal;font-size: 16px;margin: 6px 0;}
  .actiname>a:hover{color: #333;}
  .actime{color: #999;font-size: 12px;line-height: 2.3em;}
  .actime>span:nth-child(2){margin-left: 32px;}
  .actime>span:nth-child(3){margin-left: 32px;}
  .actixin{font-size: 12px;line-height: 2.3em;color: #333;}
  .actixin>.actixinli{margin-right: 20px;}
  .actiguan{height: 30px;line-height: 30px;margin-top: 5px;font-size: 14px;}
  .actiguan>a{border-left: 1px solid #ddd;padding: 0 10px;color: #333;}
  .actiguan a .fa{color: #00BCD4;line-height: 18px;font-size: 20px;}
  .actiguan a span{display: inline-block;}
  .actiguan a .fa{margin-right: 4px;}
</style>
<link rel="stylesheet" type="text/css" href="<?php  echo $_W['siteroot'];?>/web/resource/js/select2/select2.css">
<script type="text/javascript" src="<?php  echo $_W['siteroot'];?>/web/resource/webuploader-0.1.5/dist/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php  echo $_W['siteroot'];?>/web/resource/js/select2/select2.min.js"></script>

<ul class="nav nav-tabs">
  <span class="ygxian"></span>
  <div class="ygdangq">当前位置:</div>
  <li  <?php  if($type=='all') { ?> class="active" <?php  } ?>><a href="<?php  echo $this->createWebUrl('activity',array('type'=>all));?>">全部</a></li>
  <li   <?php  if($type=='wait') { ?> class="active" <?php  } ?>><a href="<?php  echo $this->createWebUrl('activity',array('type'=>wait,'state'=>0));?>">待立案</a></li>
  <li   <?php  if($type=='agree') { ?> class="active" <?php  } ?>><a href="<?php  echo $this->createWebUrl('activity',array('type'=>agree,'state'=>4));?>">已指派</a></li>
  <li   <?php  if($type=='now') { ?> class="active" <?php  } ?>><a href="<?php  echo $this->createWebUrl('activity',array('type'=>now,'state'=>1));?>">处理中</a></li>
  <li   <?php  if($type=='delivery') { ?> class="active" <?php  } ?>><a href="<?php  echo $this->createWebUrl('activity',array('type'=>delivery,'state'=>2));?>">已结案</a></li>
</ul>
<div class="row ygrow">
  <div class="col-lg-12">
    <form action="" method="get" class="col-md-6">
      <input type="hidden" name="c" value="site"/>
      <input type="hidden" name="a" value="entry"/>
      <input type="hidden" name="m" value="zh_gjhdbm"/>
      <input type="hidden" name="do" value="activity"/>
      <div class="input-group" style="width: 300px">
        <input type="text" name="keywords" class="form-control" value="<?php  echo $_GPC['keywords'];?>" placeholder="请输入位置">
        <span class="input-group-btn">
          <input type="submit" class="btn btn-default" name="submit" value="查找"/>
        </span>
      </div>
      <input type="hidden" name="token" value="<?php  echo $_W['token'];?>"/>
    </form>
  </div>
</div>

<div class="main">
  <div class="panel panel-default">
    <div class="panel-heading">案件管理</div>
        <div class="panel-body" style="padding: 0px 15px;">
            <div class="row">
                <table class="yg5_tabel col-md-12">
                  <tr class="yg5_tr1">
                      <td>编号</td>
                      <td>案件编号</td>
                      <td class="store_td1">位置</td>
                      <td>案件时间</td>
                      <td>状态</td>
                      <td>交警</td>
                      <td>操作</td>
                  </tr>
                 <?php  if(is_array($list)) { foreach($list as $key => $item) { ?>
                  <tr class="yg5_tr2">
                      <td><?php  echo $item['case_id'];?></td>
                      <td><?php  echo $item['event_num'];?></td>
                      <td class="store_td1"><?php  echo $item['address'];?></td>
                      <td><?php  echo date("Y-m-d H:i:s",$item['add_time'])?></td>
                      <?php  if($item['status']==0) { ?>
                      <td >
                        <span class="label storered">待立案</span>
                      </td >
                      <?php  } else if($item['status']==4) { ?>
                      <td >
                          <span class="label storeblue">已指派</span>
                      </td>
                      <?php  } else if($item['status']==1) { ?>
                      <td >
                          <span class="label storeblue">进行中（<?php  if($item['sub_status']==1) { ?>调查处理中<?php  } else if($item['sub_status']==2) { ?>技术鉴定中<?php  } else { ?>下发认证书<?php  } ?>）</span>
                      </td>
                      <?php  } else if($item['status']==2) { ?>
                      <td >
                       <span class="label storegrey">已结案</span>
                      </td>
                      <?php  } else if($item['status'] == 3) { ?>
                      <td >
                          <span class="label storegrey">已拒绝</span>
                      </td>
                      <?php  } ?>
                      <td><?php  echo $item['deal_name'];?></td>
                      <td>
                          <?php  if($item['status']==0) { ?>
                          <a href="#infoModal" role="button" data-toggle="modal" data-id="<?php  echo $item['case_id'];?>" data-event-num="<?php  echo $item['event_num'];?>" class="btn ygyouhui2 btn-xs">审核</a>
                          <?php  } else { ?>
                          <a href="#infoModal" role="button" data-toggle="modal"  data-deal-tel="<?php  echo $item['tel'];?>"  data-deal-name="<?php  echo $item['deal_name'];?>" data-deal-id="<?php  echo $item['deal_id'];?>"  data-id="<?php  echo $item['case_id'];?>" data-event-num="<?php  echo $item['event_num'];?>" class="btn ygyouhui2 btn-xs">更换交警</a>
                          <?php  } ?>
                          <?php  if($item['status']==1) { ?>
                          <!--<a  href="<?php  echo $this->createWebUrl('endcase', array('case_id' => $item['case_id']))?>" role="button" data-toggle="modal" data-id="<?php  echo $item['case_id'];?>" class="btn ygyouhui2 btn-xs">结案</a>-->
                          <?php  } ?>
                          <a href="<?php  echo $this->createWebUrl('addactivity', array('case_id' => $item['case_id'],'type'=>1))?>" class="storespan btn btn-xs">
                            <span class="fa fa-pencil"></span>
                            <span class="bianji">详情<span class="aritemdown"></span></span>
                          </a>
                          <a class="storespan btn btn-xs" href="<?php  echo $this->createWebUrl('activity', array('case_id' => $item['case_id'],'op'=>'delete'))?>" onclick="return confirm('确认删除吗？');return false;">
                            <span class="fa fa-trash-o"></span>
                            <span class="bianji">删除<span class="aritemdown"></span></span>
                          </a>
                      </td>
                  </tr>
            <?php  } } ?>
            <?php  if(empty($list)) { ?>
                <tr class="yg5_tr2">
                  <td colspan="9">
                    暂无案件
                  </td>
                </tr>
            <?php  } ?>
          </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">审核</h4>
            </div>
            <div class="modal-body">
                <form class="form-dialog" report-submit="ture">
                    <div class="form-group">
                            <label class="col-sm-2 control-label">请选择交警</label>
                            <div class="col-sm-9 col-xs-12">
                                <select name="deal_id" id="deal_id" class='select2' style="width:200px;">
                                    <option value="0">请选择交警</option>
                                    <?php  if(is_array($deal_data)) { foreach($deal_data as $deal) { ?>
                                    <option value="<?php  echo $deal['id'];?>"> <?php  echo $deal['link_tel'];?> <?php  echo $deal['user_name'];?></option>
                                    <?php  } } ?>
                                </select>
                            </div>
                    </div>

                    <input type="hidden" name='case_id' value="" id="hidden_case_id">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addoredit">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
<!-- 结案弹窗 -->
<div class="modal fade"  id="case" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">结案</h4>
            </div>
            <div class="modal-body">
                <form class="form-end">
                   <input type="hidden" name='case_id' value="" id="end_case_id">
                   <?php  echo tpl_ueditor('cert_info',$list['cert_info']);?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="end_case">结案</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>
<!-- 结案弹窗 -->
<div class="text-right we7-margin-top">
    <?php  echo $pager;?>
</div>

<script type="text/javascript">
    $(function() {
        $("#frame-8").show();
        $("#yframe-8").addClass("wyactive");
        $("#deal_id").select2();
    })
    $('#infoModal').on('show.bs.modal',function(event){
        var button = $(event.relatedTarget);
        var modal = $(this);
        var case_id = button.data('id');//获取要操作的ID
        var event_num = button.data('event-num');//获取要操作的ID
        var deal_id = button.data('deal-id');//获取要操作的ID
        var deal_tel = button.data('deal-tel');//获取要操作的ID
        var deal_name = button.data('deal-name');//获取要操作的ID
        var str = deal_tel+' '+deal_name;
        $('#deal_id').val(deal_id);
        $('#select2-chosen-1').text(str);
        $('#hidden_case_id').val(case_id);
        $('#model_event_num').val(event_num)
    });

    $('#case').on('show.bs.modal',function(event){
        var button = $(event.relatedTarget);
        var modal = $(this);
        var case_id = button.data('id');//获取要操作的ID
        $('#end_case_id').val(case_id);
    });
    $('#addoredit').click(function(){
        console.log($('.form-dialog').serialize());
        $.ajax({
            type: "POST",
            url: "<?php  echo $this->createWebUrl('activity',array('op'=>check))?>",
            data: $('.form-dialog').serialize(),
            async: false,
            success: function(data) {
               data = JSON.parse(data);
               console.log(data);
               if(data.type == 'error'||data.redirect == 'error'){
                   util.message(data.message);
               }else{
                   location.reload();
               }
            },
            error: function(request) {

            }
        });
    });
    //结案
     $('#end_case').click(function(){
        $.ajax({
            type: "POST",
            url: "<?php  echo $this->createWebUrl('activity',array('op'=>'end'))?>",
            data: $('.form-end').serialize(),
            async: false,
            success: function(data) {
                console.log(data);
                data = JSON.parse(data);
               if(data.type == 'error'||data.redirect == 'error'){
                   util.message(data.message);
               }else{
                   alert(data.message);
                   location.reload();
               }
            },
            error: function(request) {

            }
        });
    })
</script>
