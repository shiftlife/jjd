<?php defined('IN_IA') or exit('Access Denied');?><?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/header', TEMPLATE_INCLUDEPATH)) : (include template('public/header', TEMPLATE_INCLUDEPATH));?>

<?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('public/comhead', TEMPLATE_INCLUDEPATH)) : (include template('public/comhead', TEMPLATE_INCLUDEPATH));?>
<link rel="stylesheet" type="text/css" href="../addons/zh_gjhdbm/template/public/ygcsslist.css">
<style type="text/css">
    .ygrow{font-size: 12px;color: #44ABF7;}
    .userremark{line-height: 35px;}
    .userinfolist{font-size: 14px;line-height: 30px;}
    .userilfirst{font-weight: bold;}
    .userilsecond{margin-left: 30px;}
    .userinfolist2>li>span{cursor: pointer;}
    .usertuinfo{width: 100%;height: 100%;position: absolute;left: 0px;top: 0px;z-index: 10;border:1px solid #eee;display: none;background-color: rgba(0,0,0,0.5);text-align: center;}
    .usertuinfo>img{width: 400px;height: 233px;margin-top: 15px;}
    .usertuinfo>div{position: absolute;right: 20%;top: 5px;z-index: 11;}
</style>
<ul class="nav nav-tabs">
    <span class="ygxian"></span>
    <div class="ygdangq">当前位置:</div>
    <li class="active"><a href="javascript:void(0);">会员列表</a></li>
</ul>

<div class="row ygrow">
    <form action="" method="get" class="col-md-3">
        <input type="hidden" name="c" value="site" />
        <input type="hidden" name="a" value="entry" />
        <input type="hidden" name="m" value="zh_gjhdbm" />
        <input type="hidden" name="do" value="user2" />
        <div class="input-group">
            <input type="text" name="keywords" class="form-control" value="<?php  echo $_GPC['keywords'];?>" placeholder="请输入昵称" style="font-size: 12px;">
            <span class="input-group-btn">
                <input type="submit" class="btn btn-default" name="submit" value="查找"/>
            </span>
        </div>
        <input type="hidden" name="token" value="<?php  echo $_W['token'];?>"/>
    </form>
    <!--   <div class="col-md-4 userremark">人工充值：双击可修改余额，输入值与原有值进行累加充值。</div> -->
</div>
<div class="main">
    <div class="panel panel-default">

        <div class="panel-heading">用户列表</div>
        <div class="panel-body" style="padding: 0px 15px;">

            <div class="row">
                <table class="yg5_tabel col-md-12">
                    <tr class="yg5_tr1">
                        <th class="store_td1 col-md-1" >id</th>
                        <th class="store_td1 col-md-2">用户头像</th>
                        <th class="col-md-1">用户昵称</th>
                        <th class="col-md-1">姓名</th>
                        <th class="col-md-1">身份证</th>
                        <th class="col-md-1">是否实名</th>
                        <th class="col-md-1">是否改为交警</th>
                        <th class="col-md-2">注册时间</th>
                        <th class="col-md-2">操作</th>
                    </tr>
                    <?php  if(is_array($list)) { foreach($list as $row) { ?>
                    <tr class="yg5_tr2">
                        <td ><?php  echo $row['id'];?></td>
                        <td><img class="store_list_img" src="<?php  echo $row['img'];?>"/></td>
                        <td><?php  echo $row['name'];?></td>
                        <td><?php  echo $row['user_name'];?></td>
                        <td><?php  echo $row['idcard'];?></td>
                        <td><?php  if($row['user_name']) { ?>是<?php  } else { ?>否<?php  } ?></td>
                        <td>
                            <a  href="<?php  echo $this->createWebUrl('user2', array('id' => $row['id'],'op'=>'police'))?>" onClick="return confirm('确定更改为交警?');"><span class="label label-primary">用户</span></a>

                        </td>
                        <td><?php  echo date("Y-m-d H:i",$row['join_time']);?></td>
                        <td>
                            <a class="storespan btn btn-xs" href="<?php  echo $this->createWebUrl('user2', array('id' => $row['id'],'op'=>'delete'))?>" onclick="return confirm('确认删除吗？');return false;">
                                <span class="fa fa-trash-o"></span>
                                <span class="bianji">删除<span class="aritemdown"></span></span>
                            </a>
                        </td>

                    </tr>
                    <?php  } } ?>

                    <?php  if(empty($list)) { ?>

                    <tr class="yg5_tr2">

                        <td colspan="9">

                            暂无用户信息
                        </td>
                    </tr>
                    <?php  } ?>
                </table>
            </div>

        </div>

    </div>

</div>

<div class="text-right we7-margin-top"><?php  echo $pager;?></div>
<!-- <?php (!empty($this) && $this instanceof WeModuleSite || 1) ? (include $this->template('common/footer', TEMPLATE_INCLUDEPATH)) : (include template('common/footer', TEMPLATE_INCLUDEPATH));?> -->
<script type="text/javascript">
    $(function(){
        $("#frame-4").show();
        $("#yframe-4").addClass("wyactive");

        // $(".userinfolist2>li").each(function(){})
        $(".check_all").click(function () {

            var checked = $(this).get(0).checked;

            $("input[type=checkbox]").attr("checked", checked);

        });

        $("input[name=btn_printall]").click(function () {

            var check = $("input[type=checkbox][class!=check_all]:checked");

            if (check.length < 1) {

                alert('请选择要删除的订单!');

                return false;

            }

            if (confirm("确认要删除选择的订单?")) {

                var id = new Array();

                check.each(function (i) {

                    id[i] = $(this).val();

                });

                var url = "<?php  echo $this->createWebUrl('user', array('op' => 'delete', 'id' => $row['id']))?>";

                $.post(

                    url,

                    {idArr: id},

                    function (data) {

                        alert(data.error);

                        location.reload();

                    }, 'json'

                );

            }

        });

        // var all = $("#allCheckBox");

        // var oInp = $("input[type=checkbox]")

        // //全选的复选框加点击事件

        // all.click(function(){

        //     for (var i = 0; i < oInp.length; i++) {

        //         oInp[i].checked = all.checked;

        //     }

        // })



        // //2.根据商品前的复选框是否选中来决定全选的复选框是否选中

        // //完成效果：所有商品前的复选框选中时，才能全选的复选框选中

        // /*思路：①for循环 判断每个商品前的复选框是否选中

        //   ②if判断 如果*/

        //   //for循环

        // for (var i = 0; i < oInp.length; i++) {

        //     oInp[i].click(function(){

        //         var k = 0;

        //         for (var i = 0; i < oInp.length; i++) {

        //             if(oInp[i].checked == false){

        //                 k = 1;

        //                 break;

        //             }

        //         }



        //         if(k == 0){

        //             all.checked = true;

        //         }else{

        //             all.checked = false;

        //         }

        //     })

        // }//for循环结束符

    })

</script>

