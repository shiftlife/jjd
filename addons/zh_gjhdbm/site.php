<?php
/**
 * 志汇活动报名高级版模块微站定义
 *
 * @author 武汉志汇科技
 * @url http://bbs.we7.cc/
 */
defined('IN_IA') or exit('Access Denied');
require 'inc/func/core.php';

class Zh_gjhdbmModuleSite extends  Core {


	//留言批量删除
public function doMobileDelly(){
    global $_W, $_GPC;
    $res=pdo_delete('zh_gjhdbm_assess',array('id'=>$_GPC['id']));
    if($res){
        message('删除成功',$this->createWebUrl('message',array()),'success');
    }else{
        message('删除失败','','error');
    }

}

//修改排序
public function doMobileUpdHd() {
	global $_W,$_GPC;
	$res=pdo_update('zh_gjhdbm_activity',array('sort'=>$_GPC['sort']),array('id'=>$_GPC['activity_id']));
	if($res){
		echo '1';
	}else{
		echo '2';
	}   

}

//查看报名信息
public function doMobileGetBmInfo(){
	global $_W,$_GPC;
	$res=pdo_getall('zh_gjhdbm_bmextend',array('order_num'=>$_GPC['order_num']));
	echo json_encode($res);

}



  public function doMobileSelectUser(){
    global $_W, $_GPC;
    $sql =" select id,name from ".tablename('zh_gjhdbm_user')." where uniacid={$_W['uniacid']} and id not in (select hx_id  from" .tablename('zh_gjhdbm_hx')."where uniacid={$_W['uniacid']} and  user_id=0)  and  openid like '%{$_GPC['keywords']}%'";    
    echo json_encode(pdo_fetchall($sql));
  }












}