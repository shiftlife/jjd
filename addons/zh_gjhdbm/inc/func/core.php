<?php
defined('IN_IA') or exit ('Access Denied');
class Core extends WeModuleSite
{

    public function getMainMenu()
    {
        global $_W, $_GPC;
         
        $do = $_GPC['do'];
        $navemenu = array();
        $cur_color = ' style="color:#d9534f;" '; 
        $navemenu[14] = array(
            'title' => '<a href="index.php?c=site&a=entry&op=display&do=index&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-14"><icon style="color:#8d8d8d;" class="fa fa-newspaper-o"></icon>  数据概况</a>',
            'items' => array(
               0 => $this->createMainMenu('数据展示 ', $do, 'index', ''),
                     // 1 => $this->createMainMenu('自定义数据 ', $do, 'numdata', ''),
               )
            );
        $navemenu[8] = array(
            'title' => '<a href="index.php?c=site&a=entry&op=display&do=activity&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-8"><icon style="color:#8d8d8d;" class="fa fa-cart-plus"></icon>  案件管理</a>',
            'items' => array(
                 0 => $this->createMainMenu('案件管理 ', $do, 'activity', ''),
                 1=> $this->createMainMenu('添加案件', $do, 'addactivity', ''),
            )
        );
        $navemenu[17] = array(
                'title' => '<a href="index.php?c=site&a=entry&op=display&do=article&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-17"><icon style="color:#8d8d8d;" class="fa fa-compass"></icon>文章管理</a>',
                'items' => array(
                     0 => $this->createMainMenu('文章管理 ', $do, 'article', ''),
                     1=> $this->createMainMenu('添加文章 ', $do, 'addarticle', ''),
                )
            );
         $navemenu[18] = array(
                'title' => '<a href="index.php?c=site&a=entry&op=display&do=deal&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-18"><icon style="color:#8d8d8d;" class="fa fa-server"></icon>交警管理</a>',
                'items' => array(
                     0 => $this->createMainMenu('交警管理 ', $do, 'deal', ''),
                )
            );
//
//        // $navemenu[1] = array(
//        //     'title' => '<a href="index.php?c=site&a=entry&op=display&do=city&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-1"><icon style="color:#8d8d8d;" class="fa fa-cart-plus"></icon>  城市管理</a>',
//        //     'items' => array(
//        //          0 => $this->createMainMenu('城市管理 ', $do, 'city', ''),
//        //          1=> $this->createMainMenu('添加城市', $do, 'addcity', ''),
//        //     )
//        // );
//
//
//          $navemenu[2] = array(
//                'title' => '<a href="index.php?c=site&a=entry&op=display&do=ad&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-2"><icon style="color:#8d8d8d;" class="fa fa-gift"></icon>广告管理</a>',
//                'items' => array(
//                     0 => $this->createMainMenu('广告管理 ', $do, 'ad', ''),
//                     1=> $this->createMainMenu('添加广告', $do, 'addad', ''),
//                )
//            );
//
//           $navemenu[10] = array(
//                'title' => '<a href="index.php?c=site&a=entry&op=display&do=sponsor&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-10"><icon style="color:#8d8d8d;" class="fa fa-users"></icon> 主办方管理</a>',
//                'items' => array(
//                     0 => $this->createMainMenu('主办方管理 ', $do, 'sponsor', ''),
//                     1=> $this->createMainMenu('认证管理 ', $do, 'attestation', ''),
//                     2=> $this->createMainMenu('认证设置 ', $do, 'rzcheck', ''),
//
//                )
//            );
//           $navemenu[9] = array(
//            'title' => '<a href="index.php?c=site&a=entry&op=display&do=fxlist&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-9"><icon style="color:#8d8d8d;" class="fa fa-cart-plus"></icon>  分销系统</a>',
//            'items' => array(
//                0 => $this->createMainMenu('分销商管理', $do, 'fxlist', ''),
//                1 => $this->createMainMenu('分销设置', $do, 'fxset', ''),
//                2 => $this->createMainMenu('提现管理', $do, 'fxtx', ''),
//                    //3 => $this->createMainMenu('提现设置', $do, 'fxtxsz', ''),
//                )
//            );
//
//        //     $navemenu[7] = array(
//        //     'title' => '<a href="index.php?c=site&a=entry&op=display&do=enroll&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-7"><icon style="color:#8d8d8d;" class="fa fa-cart-plus"></icon>  报名标签</a>',
//        //     'items' => array(
//        //          0 => $this->createMainMenu('报名标签 ', $do, 'enroll', ''),
//        //          1=> $this->createMainMenu('添加标签', $do, 'addenroll', ''),
//        //     )
//        // );
//
//            // $navemenu[9] = array(
//            //     'title' => '<a href="index.php?c=site&a=entry&op=display&do=attestation&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-9"><icon style="color:#8d8d8d;" class="fa fa-child"></icon>     认证管理</a>',
//            //     'items' => array(
//            //          0 => $this->createMainMenu('认证管理 ', $do, 'attestation', ''),
//            //          1=> $this->createMainMenu('认证设置 ', $do, 'rzcheck', ''),
//
//            //     )
//            // );
//               $navemenu[11] = array(
//                'title' => '<a href="index.php?c=site&a=entry&op=display&do=assess&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-11"><icon style="color:#8d8d8d;" class="fa fa-user"></icon> 留言管理</a>',
//                'items' => array(
//                     0 => $this->createMainMenu('留言管理 ', $do, 'assess', ''),
//
//
//                )
//                );
//
//
//
//          $navemenu[3] = array(
//                'title' => '<a href="index.php?c=site&a=entry&op=display&do=txlist&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-3"><icon style="color:#8d8d8d;" class="fa fa-money"></icon>  提现管理</a>',
//                'items' => array(
//                     0 => $this->createMainMenu('提现列表 ', $do, 'txlist', ''),
//                     1 => $this->createMainMenu('提现设置 ', $do, 'txsz', ''),
//                )
//            );
//
           $navemenu[4] = array(
               'title' => '<a href="index.php?c=site&a=entry&op=display&do=user2&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-4"><icon style="color:#8d8d8d;" class="fa fa-user"></icon>  会员管理</a>',
               'items' => array(
                    0 => $this->createMainMenu('会员列表 ', $do, 'user2', ''),
               )
           );
            $navemenu[5] = array(
                'title' => '<a href="index.php?c=site&a=entry&op=display&do=settings&m=zh_gjhdbm" class="panel-title wytitle" id="yframe-5"><icon style="color:#8d8d8d;" class="fa fa-cog"></icon>  系统设置</a>',
                'items' => array(
                    0 => $this->createMainMenu('基本信息 ', $do, 'settings', ''),
                    1 => $this->createMainMenu('小程序配置', $do, 'peiz', ''),
                    2 => $this->createMainMenu('支付配置', $do, 'pay', ''),
                    // 3 => $this->createMainMenu('分享设置', $do, 'fenx', ''),
                    4 => $this->createMainMenu('短信配置', $do, 'sms', ''),
                    5 => $this->createMainMenu('模板消息配置', $do, 'news', ''),
                    //6 => $this->createMainMenu('七牛配置', $do, 'qiniu', ''),
                    8 => $this->createMainMenu('版权设置', $do, 'copyright', ''),

                )
            );
     
        return $navemenu;
    }

   

    function createCoverMenu($title, $method, $op, $icon = "fa-image", $color = '#d9534f')
    {
        global $_GPC, $_W;
        $cur_op = $_GPC['op'];
        $color = ' style="color:'.$color.';" ';
        return array('title' => $title, 'url' => $op != $cur_op ? $this->createWebUrl($method, array('op' => $op)) : '',
            'active' => $op == $cur_op ? ' active' : '',
            'append' => array(
                'title' => '<i class="fa fa-angle-right"></i>',
            )
        );
    }

    function createMainMenu($title, $do, $method, $icon = "fa-image", $color = '')
    {
        $color = ' style="color:'.$color.';" ';

        return array('title' => $title, 'url' => $do != $method ? $this->createWebUrl($method, array('op' => 'display')) : '',
            'active' => $do == $method ? ' active' : '',
            'append' => array(
                'title' => '<i '.$color.' class="fa fa-angle-right"></i>',
            )
        );
    }

/*    function createSubMenu($title, $do, $method, $icon = "fa-image", $color = '#d9534f', $storeid)
    {
        $color = ' style="color:'.$color.';" ';
        $url = $this->createWebUrl($method, array('op' => 'display', 'storeid' => $storeid));
        if ($method == 'stores') {
            $url = $this->createWebUrl('stores', array('op' => 'post', 'id' => $storeid, 'storeid' => $storeid));
        }

        return array('title' => $title, 'url' => $do != $method ? $url : '',
            'active' => $do == $method ? ' active' : '',
            'append' => array(
                'title' => '<i class="fa '.$icon.'"></i>',
            )
        );
    }*/
    function createSubMenu($title, $do, $method, $icon = "fa-image", $color = '#d9534f', $city)
    {
        $color = ' style="color:'.$color.';" ';
        $url = $this->createWebUrl2($method, array('op' => 'display', 'city' => $city));
        if ($method == 'stores2') {
            $url = $this->createWebUrl2('stores2', array('op' => 'post', 'id' => $storeid, 'city' =>$city));
        }



        return array('title' => $title, 'url' => $do != $method ? $url : '',
            'active' => $do == $method ? ' active' : '',
            'append' => array(
                'title' => '<i class="fa '.$icon.'"></i>',
                )
            );
    }

    public function getStoreById($id)
    {
        $store = pdo_fetch("SELECT * FROM " . tablename('wpdc_store') . " WHERE id=:id LIMIT 1", array(':id' => $id));
        return $store;
    }


    public function set_tabbar($action, $storeid)
    {
        $actions_titles = $this->actions_titles;
        $html = '<ul class="nav nav-tabs">';
        foreach ($actions_titles as $key => $value) {
            if ($key == 'stores') {
                $url = $this->createWebUrl('stores', array('op' => 'post', 'id' => $storeid));
            } else {
                $url = $this->createWebUrl($key, array('op' => 'display', 'storeid' => $storeid));
            }

            $html .= '<li class="' . ($key == $action ? 'active' : '') . '"><a href="' . $url . '">' . $value . '</a></li>';
        }
        $html .= '</ul>';
        return $html;
    }

    //退票
public function wxrefund($ticket_id){
    global $_W, $_GPC;
    include_once IA_ROOT . '/addons/zh_gjhdbm/cert/WxPay.Api.php';
    load()->model('account');
    load()->func('communication');
    $refund_order =pdo_get('zh_gjhdbm_bmlist',array('id'=>$ticket_id));  
    $total=pdo_get('zh_gjhdbm_bmlist',array('sh_ordernum'=>$refund_order['sh_ordernum']),array('sum(money) as money'));
    $WxPayApi = new WxPayApi();
    $input = new WxPayRefund();
    $path_cert = IA_ROOT . "/addons/zh_gjhdbm/cert/".'apiclient_cert_' .$_W['uniacid'] . '.pem';
    $path_key = IA_ROOT . "/addons/zh_gjhdbm/cert/".'apiclient_key_' . $_W['uniacid'] . '.pem';
    $account_info = $_W['account'];     
    $res=pdo_get('zh_gjhdbm_system',array('uniacid'=>$_W['uniacid']));
    $appid=$res['appid'];
    $key=$res['wxkey'];
    $mchid=$res['mchid']; 
    $out_trade_no=$refund_order['sh_ordernum'];
    $total_fee=$total['money']*100;
    $fee=$refund_order['money'] * 100;
        /*$out_trade_no=$refund_order['sh_ordernum'];
        $fee = $refund_order['money'] * 100;*/
    $input->SetAppid($appid);
    $input->SetMch_id($mchid);
    $input->SetOp_user_id($mchid);
    $input->SetRefund_fee($fee);
    $input->SetTotal_fee($total_fee);
           // $input->SetTransaction_id($refundid);
    $input->SetOut_refund_no($refund_order['code']);
    $input->SetOut_trade_no($out_trade_no);
    //var_dump($input);die;
    $result = $WxPayApi->refund($input, 6, $path_cert, $path_key, $key);
    return $result;


}


   
}