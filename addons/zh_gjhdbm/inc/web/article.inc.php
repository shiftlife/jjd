<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';

$pageindex = max(1, intval($_GPC['page']));
$pagesize=10;

$sql = "select * from ".tablename('zh_gjhdbm_article')." where is_delete = 0 order by add_time desc";
$total=pdo_fetchcolumn( "select * from ".tablename('zh_gjhdbm_article')." where is_delete = 0");
$select_sql =$sql." LIMIT " .($pageindex - 1) * $pagesize.",".$pagesize;

$list=pdo_fetchall($select_sql);
$pager = pagination($total, $pageindex, $pagesize);


if($operation=='delete'){
    $article_id = $_GPC['article_id'];
    $res=pdo_update('zh_gjhdbm_article',array('is_delete'=>1),array('article_id'=>$article_id));
    if($res){
        message('删除成功',$this->createWebUrl('article',array()),'success');
    }else{
        message('删除失败','','error');
    }
}
include $this->template('web/article');