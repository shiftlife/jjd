<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();

//获取数据
$article_id = $_GPC['article_id']?intval($_GPC['article_id']):0;
$list = pdo_fetch("select * from ".tablename('zh_gjhdbm_article')." where article_id = ".$article_id);

//编辑+添加
if(checksubmit('submit')){
    $title = trim($_GPC['title']);
    $content = $_GPC['content'];

	if(empty($title)){
		message('标题不能为空!','','error');
	}
    if(empty($content)){
        message('内容不能为空','','error');
    }

   $data = array(
       'title'=>$title,
       'content'=>$content,
   );
   if(empty($article_id)){
         $data['add_time'] = time();
         $res=pdo_insert('zh_gjhdbm_article',$data);
    }else {
       $res = pdo_update('zh_gjhdbm_article', $data, array('article_id' => $article_id));
    }
    if($res){
        message('操作成功！', $this->createWebUrl('article'), 'recommend');
    }else{
        message('操作失败！','','error');
    }
}
include $this->template('web/addarticle');