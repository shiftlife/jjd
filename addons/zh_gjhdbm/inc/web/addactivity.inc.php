<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$op = $_GPC['op'];
$type = $_GPC['type']?intval($_GPC['type']):0;
//获取数据
$case_id = $_GPC['case_id']?intval($_GPC['case_id']):0;
$list = pdo_fetch("select c.*,d.deal_name from ".tablename('zh_gjhdbm_case')." as c left join ".tablename('zh_gjhdbm_deal')." as d on c.deal_id = d.deal_id where case_id = ".$case_id);
$list['pic_arr'] = array();
if($list){
    $list['pic_arr'] = pdo_fetchall("select * from ".tablename('zh_gjhdbm_pic')." where case_id = ".$case_id);
    $list['end_arr'] = array_filter(explode(';',$list['end_pic']));
    if($case_id){
        $contact_arr = pdo_fetchall("select * from ".tablename('zh_gjhdbm_contact')." where case_id = ".$case_id);
    }
}
//获取受理人
$deal_sql = "select * from " . tablename('zh_gjhdbm_user') . " where is_police = 1 and is_delete = 0";
$deal_data = pdo_fetchall($deal_sql);

if($op == 'del_contact'){
  $contact_id = $_GPC['contact_id'];
  $res = pdo_delete('zh_gjhdbm_contact',array('contact_id'=>$contact_id));
  if($res){
      message('操作成功！','', 'recommend');
  }else{
      message('操作失败！, ','error');
  }
}
if(checksubmit('add')){
    $contact_index = array("甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸");
    $tel = $_GPC['tel'];
    $data['event_num'] = $event_num = $_GPC['event_num'];
    $data['add_time'] = $add_time =time();
    $data['apply_id'] = -1;
    $data['deal_id'] = $deal_id = $_GPC['deal_id'];
    $data['status'] = 1;
    $data['sub_status'] = 1;
    $data['is_recieve'] = 1;
    $data['method'] = 3;

    if(empty($event_num)){
        message('请填写事故编号','','error');
    }
    if(empty($tel)){
        message('请填写事故方信息','','error');
    }
    if(empty($deal_id)){
        message('请指派交警','','error');
    }
    $res=pdo_insert('zh_gjhdbm_case',$data);
    $case_id = pdo_insertid();
    //添加联系人信息
    foreach ($tel as $key => $contact_name) {
       $telInfo = pdo_fetch("select * from ".tablename('zh_gjhdbm_tel')." where tel = '".$contact_name."'");
        $user_id = 0;
       if($telInfo){
           $user_id = $telInfo['user_id'];
       }
        $contact_info = array(
            "case_id" => $case_id,
            "contact_tel" => $tel[$key],
            "type"=>$contact_index[$key],
            "user_id"=>$user_id
        );
        pdo_insert('zh_gjhdbm_contact',$contact_info);
    }
    message('操作成功！', $this->createWebUrl('activity'), 'recommend');
}
//编辑+添加
if(checksubmit('submit')){

    $address = trim($_GPC['address']);
    $address_descript = trim($_GPC['address_descript']);
    $case_type = trim($_GPC['case_type']);
    $contact = $_GPC['contact'];
    $tel = $_GPC['tel'];
    $plate_num = $_GPC['plate_num'];
    $type = $_GPC['type'];
    $cert_info = trim($_GPC['cert_info']);
    $picarr = $_GPC['acc_pic'];

	if(empty($address)){
		message('定位地址不能为空!','','error');
	}
    if(empty($case_type)){
        message('请选择事故类型','','error');
    }
    if(empty($contact[0])){
        message('联系人不能为空!','','error');
    }
    if(empty($tel[0])){
        message('联系电话不能为空!','','error');
    }
    if(empty($plate_num[0])){
        message('车牌号不能为空!','','error');
    }
    $pic_str = '';
    if($picarr){
	   $pic_str =  implode(";",$picarr);
    }
   $data = array(
       'address'=>$address,
       'case_type'=>$case_type,
       'cert_info'=>$cert_info,
       'acc_pic'=>$pic_str,
       'address_descript'=>$address_descript,
   );
    $contact_index = array("甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸");

   if(empty($case_id)){
         $data['add_time'] = time();
         $data['apply_id'] = -1;
         $data['deal_id'] = $_GPC['deal_id'];
         $res=pdo_insert('zh_gjhdbm_case',$data);
         $case_id = pdo_insertid();
         //添加联系人信息
        foreach ($contact as $key => $contact_name) {
             $contact_info = array(
                "case_id" => $case_id,
                "contact_tel" => $tel[$key],
                "type"=>$contact_index[$key]
             );
             pdo_insert('zh_gjhdbm_contact',$contact_info);
        }
   }else {
       $res = pdo_update('zh_gjhdbm_case', $data, array('case_id' => $case_id));
       $contact_ids = $_GPC['contact_ids'];
       //编辑联系人信息
       foreach ($contact as $key => $contact_name) {
           $contact_info = array(
               "case_id"=>$case_id,
               "contact_name" => $contact_name,
               "contact_tel" => $tel[$key],
               "plate_num" => $plate_num[$key],
               "type"=>$contact_index[$key]
           );
           if($contact_ids[$key]) {
              $contact = pdo_update('zh_gjhdbm_contact',$contact_info,array('contact_id'=>$contact_ids[$key]));
           }
           else{
              $contact = pdo_insert('zh_gjhdbm_contact',$contact_info);
           }
       }
    }
    message('操作成功！', $this->createWebUrl('activity'), 'recommend');
}
if($type == 0){
    include $this->template('web/addactivity1');
}else{
    include $this->template('web/addactivity');
}
