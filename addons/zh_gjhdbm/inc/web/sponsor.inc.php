<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$type=empty($_GPC['type']) ? 'all' :$_GPC['type'];
$state=$_GPC['state'];
$pageindex = max(1, intval($_GPC['page']));
$pagesize=10;
$where=' WHERE  a.uniacid=:uniacid  ';
$data[':uniacid']=$_W['uniacid'];
if($_GPC['keywords']){  
    $where.=" and a.name LIKE  concat('%', :name,'%') ";    
    $data[':name']=$_GPC['keywords'];
}
if($type !='all'){
   $where.= " and status=".$state;
}
  $sql="SELECT a.*,b.name as nick_name,b.money,c.state,c.id as rz_id FROM ".tablename('zh_gjhdbm_sponsor') . " a"  . " left join " . tablename("zh_gjhdbm_user") . " b on b.id=a.user_id". " left join " . tablename("zh_gjhdbm_attestation") . " c on c.user_id=a.user_id". $where." ORDER BY id DESC";
  $total=pdo_fetchcolumn("SELECT count(*) FROM ".tablename('zh_gjhdbm_sponsor') . " a"  . " left join " . tablename("zh_gjhdbm_user") . " b on b.id=a.user_id". " left join " . tablename("zh_gjhdbm_attestation") . " c on c.user_id=a.user_id". $where,$data);
$select_sql =$sql." LIMIT " .($pageindex - 1) * $pagesize.",".$pagesize;
$list=pdo_fetchall($select_sql,$data);
foreach($list  as $key=> $v){
	//$v['details']=base64_decode($v['details']);
	$list[$key]['details']=base64_decode($v['details']);
}
if($operation=='delete'){
     $id=$_GPC['id'];    
      $rst=pdo_get('zh_gjhdbm_sponsor',array('id'=>$id),'user_id');    
      $rst2=pdo_delete('zh_gjhdbm_activity',array('user_id'=>$rst['user_id']));
     if($rst){
        $res=pdo_delete('zh_gjhdbm_sponsor',array('id'=>$id));   
        message('删除成功',$this->createWebUrl('sponsor',array()),'success');
    }else{
         message('删除失败','','error');
    }
}
$pager = pagination($total, $pageindex, $pagesize);
include $this->template('web/sponsor');