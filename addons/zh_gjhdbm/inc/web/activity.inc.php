<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$type = empty($_GPC['type']) ? 'all' : $_GPC['type'];
$state = $_GPC['state'];
$pageindex = max(1, intval($_GPC['page']));
$pagesize = 10;
$where = 'c.is_delete = 0';

if ($_GPC['keywords']) {
    $where .= " and c.address LIKE  '%" . $_GPC['keywords'] . "%'";
}
if ($type != 'all') {
    $where .= " and status=" . $state;
}

$sql = "select c.*,u.user_name,u.link_tel,s.link_tel as tel,s.user_name as deal_name from " . tablename('zh_gjhdbm_case') . " as c left join " . tablename('zh_gjhdbm_user') . " as u on c.apply_id = u.id left join " . tablename('zh_gjhdbm_user') . " as s on c.deal_id = s.id where " . $where . " order by c.case_id desc";

$total = pdo_fetch("select count(*) as c from " . tablename('zh_gjhdbm_case') . " as c  left join " . tablename('zh_gjhdbm_user') . " as u on c.apply_id = u.id where " . $where . " order by c.case_id desc");
$select_sql = $sql . " LIMIT " . ($pageindex - 1) * $pagesize . "," . $pagesize;
$list = pdo_fetchall($select_sql);


$pager = pagination($total['c'], $pageindex, $pagesize);

//获取受理人
$deal_sql = "select * from " . tablename('zh_gjhdbm_user') . " where is_police = 1 and is_delete = 0";
$deal_data = pdo_fetchall($deal_sql);

if ($operation == 'check') {//审核通过
    $id = $_GPC['case_id'];
    $data['deal_id'] = $deal_id = $_GPC['deal_id'];
    $data['status'] = 4;

    if (empty($id)) {
        message('系统繁忙', 0, 'error');
    }
    if (empty($deal_id)) {
        message('请选择交警', 0, 'error');
    }
    $info = pdo_fetch("select * from " . tablename('zh_gjhdbm_case') . " where case_id = " . $id);
    $res = pdo_update('zh_gjhdbm_case', $data, array('case_id' => $id));
    if ($res) {
        //推送消息
//        send_message($id, $info['form_id']);
        $system = pdo_get('zh_gjhdbm_system', array('uniacid' => $_W['uniacid']));
        if($info['case_type'] == 1){
            $type = '一般';
        }else{
            $type = '紧急';
        }
        $policeInfo = pdo_fetch("select * from ".tablename('zh_gjhdbm_user')." where id = ".$deal_id);
        send_msg($policeInfo['link_tel'],array('time'=>date('Y-m-d H:i:s', $info['add_time']),'address'=>$info['address'],'type'=>$type),$system['tpl_id1']);
        message('审核成功', $this->createWebUrl('activity', array()), 'success');
    } else {
        message('审核失败', '', 'error');
    }
}
if ($operation == 'delete') {
    $case_id = $_GPC['case_id'];
    $res = pdo_update('zh_gjhdbm_case', array('is_delete' => 1), array('case_id' => $case_id));
    if ($res) {
        message('删除成功', $this->createWebUrl('activity', array()), 'success');
    } else {
        message('删除失败', '', 'error');
    }
}
include $this->template('web/activity');

function getaccess_token($uniacid) {
    $res = pdo_get('zh_gjhdbm_system', array('uniacid' => $uniacid));
    $appid = $res['appid'];
    $secret = $res['appsecret'];
    $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $appid . "&secret=" . $secret . "";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    $data = json_decode($data, true);
    return $data['access_token'];
}

//短信验证码
function send_msg($mobile, $data,$tpl_id) {
    global $_W;
    $res = pdo_get('zh_gjhdbm_system', array('uniacid' => $_W['uniacid']));
    include_once IA_ROOT . '/addons/zh_gjhdbm/ventor/dayu/SignatureHelper.php';
    $params = array ();
    $accessKeyId = $res['sms_key'];
    $accessKeySecret = $res['sms_secret'];
    $params["PhoneNumbers"] = $mobile;
    $params["SignName"] = $res['sign_name'];
    $params["TemplateCode"] = $tpl_id;
    $params['TemplateParam'] = $data;
    if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
        $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
    }
    $helper = new  \Aliyun\DySDKLite\SignatureHelper();
    $content = $helper->request(
        $accessKeyId,
        $accessKeySecret,
        "dysmsapi.aliyuncs.com",
        array_merge($params, array(
            "RegionId" => "cn-hangzhou",
            "Action" => "SendSms",
            "Version" => "2017-05-25",
        ))
    );
    return array('status' => 1);
}
