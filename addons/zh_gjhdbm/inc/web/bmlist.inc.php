<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$pageindex = max(1, intval($_GPC['page']));
$pagesize=10;
$activity_id=$_GPC['id'];
$data[':activity_id']=$activity_id;
$sql=" select xx.*,b.title,c.name,c.img  from (select distinct user_id, activity_id from ".tablename('zh_gjhdbm_bmlist')." where activity_id=:activity_id and status=1) xx  ". " left join " . tablename("zh_gjhdbm_activity") . " b on xx.activity_id=b.id left join " . tablename("zh_gjhdbm_user") . " c on xx.user_id=c.id";

$total=pdo_fetchcolumn("select count(*) from (select distinct user_id, activity_id from ".tablename('zh_gjhdbm_bmlist')." where activity_id=:activity_id) xx  ". " left join " . tablename("zh_gjhdbm_activity") . " b on xx.activity_id=b.id left join " . tablename("zh_gjhdbm_user") . " c on xx.user_id=c.id",$data);
$select_sql =$sql." LIMIT " .($pageindex - 1) * $pagesize.",".$pagesize;
$list=pdo_fetchall($select_sql,$data);
$pager = pagination($total, $pageindex, $pagesize);
//$list=pdo_getall('zhtc_nav',array('uniacid'=>$_W['uniacid']),array(),'','orderby ASC');
if($_GPC['op']=='delete'){
	$res=pdo_delete('zhtc_nav',array('id'=>$_GPC['id']));
	if($res){
		 message('删除成功！', $this->createWebUrl('nav'), 'success');
		}else{
			  message('删除失败！','','error');
		}
}
if($_GPC['status']){
	$data['status']=$_GPC['status'];
	$res=pdo_update('zhtc_nav',$data,array('id'=>$_GPC['id']));
	if($res){
		 message('编辑成功！', $this->createWebUrl('nav'), 'success');
		}else{
			  message('编辑失败！','','error');
		}
}

if(checksubmit('export_submit', true)) {
    $activity_id=$_GPC['activity_id'];
	$sql=" select xx.*,b.title,c.name,c.img,c.link_name,c.link_tel  from (select distinct user_id, activity_id from ".tablename('zh_gjhdbm_bmlist')." where activity_id=:activity_id and status=1) xx  ". " left join " . tablename("zh_gjhdbm_activity") . " b on xx.activity_id=b.id left join " . tablename("zh_gjhdbm_user") . " c on xx.user_id=c.id";
	$count=pdo_fetchall($sql,$data);
	$count=count($count);
	$pagesize = ceil($count/5000);
        //array_unshift( $names,  '活动名称'); 

	$header = array(
		'item'=>'序号',
		'title' => '活动标题',
		'name' => '报名人昵称', 
		'link_name' => '联系人', 
		'link_tel' => '联系电话',
		'mun'=>'购票数量',
		'money'=>'总金额',
		'time'=>'购票时间',

		);


	$keys = array_keys($header);
	$html = "\xEF\xBB\xBF";
	foreach ($header as $li) {
		$html .= $li . "\t ,";
	}
	$html .= "\n";
	for ($j = 1; $j <= $pagesize; $j++) {
		$sql=" select xx.*,b.title,c.name,c.img,c.link_name,c.link_tel  from (select distinct user_id, activity_id from ".tablename('zh_gjhdbm_bmlist')." where activity_id={$activity_id} and status=1) xx  ". " left join " . tablename("zh_gjhdbm_activity") . " b on xx.activity_id=b.id left join " . tablename("zh_gjhdbm_user") . " c on xx.user_id=c.id limit " . ($j - 1) * 5000 . ",5000 ";
		$list = pdo_fetchall($sql);
	}
	if (!empty($list)) {
		$size = ceil(count($list) / 500);
		for ($i = 0; $i < $size; $i++) {
			$buffer = array_slice($list, $i * 500, 500);
			$user = array();
			foreach ($buffer as $k =>$row) {
				$row['item']= $k+1;
				$total=pdo_get('zh_gjhdbm_bmlist', array('user_id'=>$row['user_id'],'activity_id'=>$activity_id,'status'=>1,'state !='=>5), array('count(id) as count','sum(money) as money','max(time) as max_time'));		
				$row['mun']=$total['count'];
				$row['money']=$total['money'];
				$row['time']= date('Y-m-d H:i:s',$total['max_time']);
				foreach ($keys as $key) {
					$data5[] = $row[$key];
				}
				$user[] = implode("\t ,", $data5) . "\t ,";
				unset($data5);
			}
			$html .= implode("\n", $user) . "\n";
		}
	}

	header("Content-type:text/csv");
	header("Content-Disposition:attachment; filename=购票信息.csv");
	echo $html;
	exit();
}
include $this->template('web/bmlist');