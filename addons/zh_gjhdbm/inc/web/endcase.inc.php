<?php

global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$case_id = $_GPC['case_id'];

if ($_W['ispost']) {
    $case_id = $_GPC['case_id'];
    if (!$_POST['cert_info']) {
        message('请上传认证书', '', 'error');
    }
    $res = pdo_update('zh_gjhdbm_case', array('status' => 2, 'cert_info' => $_POST['cert_info'], 'end_time' => time()), array('case_id' => $case_id));
    if ($res) {

        message('结案成功', $this->createWebUrl('activity', array()), 'success');
    } else {
        message('结案失败', '', 'error');
    }
}

include $this->template('web/endcase');
