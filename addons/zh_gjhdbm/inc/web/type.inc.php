<?php
global $_GPC, $_W;
// $action = 'ad';
// $title = $this->actions_titles[$action];
$GLOBALS['frames'] = $this->getMainMenu();
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';
$type=isset($_GPC['type'])?$_GPC['type']:'all';
$status=$_GPC['status'];
load()->func('tpl');
$pageindex = max(1, intval($_GPC['page']));
$pagesize=10;
$where=' WHERE  uniacid=:uniacid  ';

$sql="SELECT * FROM ".tablename('zh_gjhdbm_type') .$where." ORDER BY num asc";
$data[':uniacid']=$_W['uniacid'];
$total=pdo_fetchcolumn("SELECT * FROM ".tablename('zh_gjhdbm_type').$where." ORDER BY num asc",$data);

$select_sql =$sql." LIMIT " .($pageindex - 1) * $pagesize.",".$pagesize;
$list=pdo_fetchall($select_sql,$data);
$pager = pagination($total, $pageindex, $pagesize);
if($operation=='delete'){
	$res=pdo_delete('zh_gjhdbm_type',array('id'=>$_GPC['id']));
	if($res){
		message('删除成功',$this->createWebUrl('type',array()),'success');
	}else{
		message('删除失败','','error');
	}
}
if($_GPC['op']=='change'){
   $res=pdo_update('zh_gjhdbm_type',array('state'=>$_GPC['state']),array('id'=>$_GPC['id']));
    if($res){
        message('操作成功',$this->createWebUrl('type',array()),'success');
    }else{
        message('操作失败','','error');
    }
}
include $this->template('web/type');