<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$pageindex = max(1, intval($_GPC['page']));
$pagesize=20;
$where=" where uniacid=:uniacid";
$data[':uniacid']=$_W['uniacid'];
if($_GPC['keywords']){
   $where.=" and name LIKE  concat('%', :gid,'%') ";	
   $data[':gid']=$_GPC['keywords'];
}	
$sql="select * from".tablename('zh_gjhdbm_enroll').$where." ORDER BY sort asc";
$total=pdo_fetchcolumn("select count(*) from".tablename('zh_gjhdbm_enroll').$where,$data);
$select_sql =$sql." LIMIT " .($pageindex - 1) * $pagesize.",".$pagesize;
$list=pdo_fetchall($select_sql,$data);
$pager = pagination($total, $pageindex, $pagesize);
if($_GPC['op']=='delete'){
	$res=pdo_delete('zh_gjhdbm_enroll',array('id'=>$_GPC['id']));
	if($res){
		 message('删除成功！', $this->createWebUrl('enroll'), 'success');
		}else{
			  message('删除失败！','','error');
		}
}
if($_GPC['status']){
	$res=pdo_update('zh_gjhdbm_enroll',array('status'=>$_GPC['status']),array('id'=>$_GPC['id']));
	if($res){
		 message('编辑成功！', $this->createWebUrl('enroll'), 'success');
		}else{
			  message('编辑失败！','','error');
		}
}

include $this->template('web/enroll');