<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();

//获取数据
$id = $_GPC['id']?intval($_GPC['id']):0;
$list = pdo_fetch("select * from ".tablename('zh_gjhdbm_user')." where id = ".$id);

//编辑+添加
if(checksubmit('submit')){
    $id = trim($_GPC['id']);
    $user_name = trim($_GPC['user_name']);
    $link_tel = trim($_GPC['link_tel']);
    $police_num = trim($_GPC['police_num']);
    if(empty($user_name)){
        message('名字不能为空!','','error');
    }
    if(empty($link_tel)){
        message('电话不能为空','','error');
    }
    if(empty($police_num)){
        message('警号不能为空','','error');
    }
    $data = array(
        'user_name'=>$user_name,
        'link_tel'=>$link_tel,
        'police_num'=>$police_num,
    );
    if(!empty($list)){
        $res = pdo_update('zh_gjhdbm_user', $data, array('id' => $id));
    }
    if($res){
        message('操作成功！', $this->createWebUrl('deal'), 'recommend');
    }else{
        message('操作失败！','','error');
    }
}

include $this->template('web/adddeal');