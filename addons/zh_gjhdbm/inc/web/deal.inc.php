<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$operation = !empty($_GPC['op']) ? $_GPC['op'] : 'display';

$pageindex = max(1, intval($_GPC['page']));
$pagesize=10;
$sql = "select * from ".tablename('zh_gjhdbm_user')." where  is_police > 0 and is_delete = 0 order by id desc";
$select_sql =$sql." LIMIT " .($pageindex - 1) * $pagesize.",".$pagesize;
$list = pdo_fetchall($select_sql);

$total=pdo_fetchcolumn("select count(*)  from " . tablename("zh_gjhdbm_user")." where  is_police > 0 and is_delete = 0");
$pager = pagination($total, $pageindex, $pagesize);


if($operation=='delete'){
    $deal_id = $_GPC['id'];
    if(empty($deal_id)){
        message('系统繁忙','','error');
    }
    $res=pdo_update('zh_gjhdbm_user',array('is_delete'=>1),array('id'=>$deal_id));
    if($res){
        message('删除成功',$this->createWebUrl('deal',array()),'success');
    }else{
        message('删除失败','','error');
    }
}
if($operation=='check'){
    $deal_id = $_GPC['id'];
    $status = $_GPC['status'];
    if(empty($status)){
        message('未选择操作状态','','error');
    }
    if(empty($deal_id)){
        message('系统繁忙','','error');
    }
    $res=pdo_update('zh_gjhdbm_user',array('is_police'=>$status),array('id'=>$deal_id));
    if($res){
        message('操作成功',$this->createWebUrl('deal',array()),'success');
    }else{
        message('操作失败','','error');
    }
}
include $this->template('web/deal');