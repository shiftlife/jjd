<?php
global $_GPC, $_W;
$GLOBALS['frames'] = $this->getMainMenu();
$where= " where  is_police=0 and is_delete = 0";
if($_GPC['keywords']){
	 $where.=" and name LIKE  concat('%".$_GPC['keywords']."%') ";
}
$pageindex = max(1, intval($_GPC['page']));
$pagesize=10;
$sql="select *  from " . tablename("zh_gjhdbm_user").$where." order by id desc";
$select_sql =$sql." LIMIT " .($pageindex - 1) * $pagesize.",".$pagesize;
$list = pdo_fetchall($select_sql);	

$total=pdo_fetchcolumn("select count(*)  from " . tablename("zh_gjhdbm_user").$where);
$pager = pagination($total, $pageindex, $pagesize);
if($_GPC['op']=='delete'){
    $id = $_GPC['id'];
    if(empty($id)){
        message('系统繁忙！','','error');
    }
    $res = pdo_update("zh_gjhdbm_user",array('is_delete'=>1),array('id'=>$id));
    if($res){
       message('删除成功！', $this->createWebUrl('user2'), 'success');
    }else{
       message('删除失败！','','error');
    }
}
if($_GPC['op']=='police'){
    $id = $_GPC['id'];
    if(empty($id)){
        message('系统繁忙！','','error');
    }
    $tel_info = pdo_fetch("select * from ".tablename('zh_gjhdbm_tel')." where user_id = ".$id);
    if($tel_info){
        $info['link_tel'] = $tel_info['tel'];
        $info['is_police'] = 1;

    }
    $res = pdo_update("zh_gjhdbm_user",$info,array('id'=>$id));
    if($res){
        message('修改成功！', $this->createWebUrl('user2'), 'success');
    }else{
        message('修改失败！','','error');
    }
}
include $this->template('web/user2');
