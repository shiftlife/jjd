<?php
/**
 * [WeEngine System] Copyright (c) 2014 WE7.CC
 * WeEngine is NOT a free software, it under the license terms, visited http://www.we7.cc/ for more details.
 */
define('IN_MOBILE', true);
require '../../../../framework/bootstrap.inc.php';
global $_W, $_GPC;
$input = file_get_contents('php://input');
$isxml = true;
if (!empty($input) && empty($_GET['out_trade_no'])) {
	$obj = isimplexml_load_string($input, 'SimpleXMLElement', LIBXML_NOCDATA);
	$res = $data = json_decode(json_encode($obj), true);
	if (empty($data)) {
		$result = array(
			'return_code' => 'FAIL',
			'return_msg' => ''
			);
		echo array2xml($result);
		exit;
	}
	if ($data['result_code'] != 'SUCCESS' || $data['return_code'] != 'SUCCESS') {
		$result = array(
			'return_code' => 'FAIL',
			'return_msg' => empty($data['return_msg']) ? $data['err_code_des'] : $data['return_msg']
			);
		echo array2xml($result);
		exit;
	}
	$get = $data;
} else {
	$isxml = false;
	$get = $_GET;
}
load()->web('common');
load()->model('mc');
load()->func('communication');
$_W['uniacid'] = $_W['weid'] = intval($get['attach']);

$_W['uniaccount'] = $_W['account'] = uni_fetch($_W['uniacid']);
$_W['acid'] = $_W['uniaccount']['acid'];
$paySetting = uni_setting($_W['uniacid'], array('payment'));
if($res['return_code'] == 'SUCCESS' && $res['result_code'] == 'SUCCESS' ){
	$logno = trim($res['out_trade_no']);
	if (empty($logno)) {
		exit;
	}



	$str=$_W['siteroot'];
	$n = 0;
	for($i = 1;$i <= 3;$i++) {
		$n = strpos($str, '/', $n);
		$i != 3 && $n++;
	}
	$url=substr($str,0,$n);
//处理订单逻辑
	$order=pdo_get('zh_gjhdbm_bmlist',array('sh_ordernum'=>$logno,'status'=>0));
	if($order['status']==0){
		pdo_update('zh_gjhdbm_bmlist',array('status'=>1),array('sh_ordernum'=>$logno));
		$bmrst=pdo_getall('zh_gjhdbm_bmlist',array('order_num'=>$order['order_num'],'money !='=>"0.00"));
		$count=count($bmrst);
		foreach ($bmrst as $key => $value) {
			pdo_update('zh_gjhdbm_ticket',array('yg_num +='=>$count),array('id'=>$value['ticket_id']));
		}

	}
	

	$result = array(
		'return_code' => 'SUCCESS',
		'return_msg' => 'OK'
		);
	echo array2xml($result);
	exit;
	
}else{
		//订单已经处理过了
	$result = array(
		'return_code' => 'SUCCESS',
		'return_msg' => 'OK'
		);
	echo array2xml($result);
	exit;
}
